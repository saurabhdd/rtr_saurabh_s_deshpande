#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//namespaces
using namespace std;

//global varialbles 
bool bFullScreen_SSD = false;
Display *gpDisplay_SSD = NULL;
XVisualInfo *gpXVisualInfo_SSD = NULL;
Colormap gColormap_SSD;
Window gWindow_SSD;
int giWindowWidth_SSD = 800;
int giWindowHeight_SSD = 600;

//entry point function
int main(void)
{
    //function prototypes
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void Uninitialize(void);
    
    //varialbles declaration
    int winwidth_SSD = giWindowWidth_SSD;
    int winheight_SSD = giWindowHeight_SSD;
    
    //code 
    CreateWindow();
    
    //message loop
    XEvent event_SSD;
    KeySym keysym_SSD;
    
    while(1)
    {
        XNextEvent(gpDisplay_SSD, &event_SSD);
        switch(event_SSD.type)
        {
            case MapNotify:
                break;
                
            case KeyPress:
                keysym_SSD = XkbKeycodeToKeysym(gpDisplay_SSD, event_SSD.xkey.keycode, 0, 0);
                switch(keysym_SSD)
                {
                    case XK_Escape:
                        Uninitialize();
                        exit(0);
                        
                    case XK_F:
                    case XK_f:
                        if(bFullScreen_SSD == false)
                        {
                            ToggleFullScreen();
                            bFullScreen_SSD = true;
                        }
                        else
                        {
                            ToggleFullScreen();
                            bFullScreen_SSD = false;
                        }
                        break;
                        
                    default:
                        break;
                }
                
                break;
                
                    case ButtonPress:
                        switch(event_SSD.xbutton.button)
                        {
                            case 1:
                                break;
                                
                            case 2:
                                break;
                                
                            case 3:
                                break;
                                
                            default:
                                break;
                        }
                        break;
                        
                            case MotionNotify:
                                break;
                                
                            case ConfigureNotify:
                                winwidth_SSD = event_SSD.xconfigure.width;
                                winheight_SSD = event_SSD.xconfigure.height;
                                break;
                                
                            case Expose:
                                break;
                                
                            case DestroyNotify:
                                break;
                                
                            case 33:
                                Uninitialize();
                                exit(0);
                                
                            default:
                                break;
        }
    }
    
    Uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototypes
    void Uninitialize(void);
    
    //varialbles declaration
    XSetWindowAttributes winAttribs_SSD;
    int defaultScreen_SSD;
    int defaultDepth_SSD;
    int styleMask_SSD;
    
    //code
    gpDisplay_SSD = XOpenDisplay(NULL);
    if(gpDisplay_SSD == NULL)
    {
        printf("Error: Unable to Open X Display\n Exitting now..\n");
        Uninitialize();
        exit(1);
    }
    
    defaultScreen_SSD = XDefaultScreen(gpDisplay_SSD);
    
    defaultDepth_SSD = DefaultDepth(gpDisplay_SSD, defaultScreen_SSD);
    
    gpXVisualInfo_SSD = (XVisualInfo*) malloc(sizeof(XVisualInfo));
    
    if(gpXVisualInfo_SSD == NULL)
    {
        printf("Error: Unable to allocate memory for visual info \n Exitting now \n");
        Uninitialize();
        exit(1);
    }
    
    XMatchVisualInfo(gpDisplay_SSD, defaultScreen_SSD, defaultDepth_SSD, TrueColor, gpXVisualInfo_SSD);
    if(gpXVisualInfo_SSD == NULL)
    {
        printf("Error: Unable to get a visual\n Exitting now \n");
        Uninitialize();
        exit(1);
    }
    
    winAttribs_SSD.border_pixel = 0;
    winAttribs_SSD.background_pixmap = 0;
    winAttribs_SSD.colormap = XCreateColormap(gpDisplay_SSD, RootWindow(gpDisplay_SSD, gpXVisualInfo_SSD->screen), gpXVisualInfo_SSD->visual, AllocNone);
    gColormap_SSD = winAttribs_SSD.colormap;
    
    winAttribs_SSD.background_pixel = BlackPixel(gpDisplay_SSD, defaultScreen_SSD);
    
    winAttribs_SSD.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    styleMask_SSD = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    gWindow_SSD = XCreateWindow(gpDisplay_SSD, RootWindow(gpDisplay_SSD, gpXVisualInfo_SSD->screen),
    0,
    0,
    giWindowWidth_SSD, 
    giWindowHeight_SSD, 
    0,
    gpXVisualInfo_SSD->depth,
    InputOutput,
    gpXVisualInfo_SSD->visual,
    styleMask_SSD,
    &winAttribs_SSD);
    
    if(!gWindow_SSD)
    {
        printf("Error: Failed to create window\n Exitting now\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(gpDisplay_SSD, gWindow_SSD, "first xWindow");
    
    Atom WindowManagerDelete = XInternAtom(gpDisplay_SSD, "WM_DELETE_WINDOW", True);
    
    XSetWMProtocols(gpDisplay_SSD, gWindow_SSD, &WindowManagerDelete, 1);
    
    XMapWindow(gpDisplay_SSD, gWindow_SSD);
    
}


void ToggleFullScreen(void)
{
    //varialbles declaration
    Atom wm_state_SSD;
    Atom fullScreen_SSD;
    XEvent xev_SSD = {0};
    
    //code
    wm_state_SSD = XInternAtom(gpDisplay_SSD, "_NET_WM_STATE", False);
    memset(&xev_SSD, 0, sizeof(xev_SSD));
    
    xev_SSD.type = ClientMessage;
    xev_SSD.xclient.window = gWindow_SSD;
    xev_SSD.xclient.message_type = wm_state_SSD;
    xev_SSD.xclient.format = 32;
    xev_SSD.xclient.data.l[0] = bFullScreen_SSD ? 0 : 1;
    fullScreen_SSD = XInternAtom(gpDisplay_SSD, "_NET_WM_STATE_FULLSCREEN", False);
    xev_SSD.xclient.data.l[1] = fullScreen_SSD;
    
    XSendEvent(gpDisplay_SSD, RootWindow(gpDisplay_SSD, gpXVisualInfo_SSD->screen), False, 
               StructureNotifyMask,
               &xev_SSD);
    
}

void Uninitialize(void)
{
    if(gWindow_SSD)
    {
        XDestroyWindow(gpDisplay_SSD, gWindow_SSD);
    }
    
    if(gColormap_SSD)
    {
        XFreeColormap(gpDisplay_SSD, gColormap_SSD);
    }
    
    if(gpXVisualInfo_SSD)
    {
        free(gpXVisualInfo_SSD);
        gpXVisualInfo_SSD = NULL;
    }
    
    if(gpDisplay_SSD)
    {
        XCloseDisplay(gpDisplay_SSD);
        gpDisplay_SSD = NULL;
    }
}

    
    


    
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
            
            
            
            
            
            
            
            
            
            
            


