package com.astromedicomp.Android14;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //egl = embedded egl (cause opengl-es calles internally to embedded opengl)

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer{

	private final Context context;
	private GestureDetector gestureDetector;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] vao_smiley = new int[1];
	private int[] vbo_smiley_vertices = new int[1];
	private int[] vbo_smiley_texture = new int[1];
	private int smiley_texture;

	private int mvpUniform;
	private int textureSamplerUniform;
	private float perspectiveProjectionMatrix[] = new float[16];


	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;
		this.setEGLContextClientVersion(3); //opengl ndk is server and opengl sdk is client so we are setting client version to 3	
		this.setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	//override method of GLSurfaceView.Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		//String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE);
		System.out.println("SSD: " + glesVersion);
		//System.out.println("SSD: " + glslVersion);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventAction = event.getAction();
		if (!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);

		return true;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float veclocityX, float velocityY)
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	private void initialize(GL10 gl)
	{
		//vertex shader source
		final String vertexShaderSourceCode = String.format
		(
		  "#version 320 es" +
		  "\n" +
		  "in vec4 vPosition;" +
		  "in vec2 texCords;" +
		  "out vec2 out_texCords;" +
		  "uniform mat4 u_mvp_matrix;" +
		  "void main(void)" +
		  "{" +
		  "gl_Position = u_mvp_matrix * vPosition;" +
		  "out_texCords = texCords;" +
		  "}"
		 );

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		//compile source code
		GLES32.glCompileShader(vertexShaderObject);

		//compile staus error chec
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("SSD: vertex shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}


		//fragment shader
		//create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		  "#version 320 es" +
		  "\n" +
		  "precision highp float;" +
		  "in vec2 out_texCords;" +
		  "uniform highp sampler2D u_texture_sampler;" +
		  "out vec4 fragColor;" +
		  "void main(void)" +
		  "{" +
		  "fragColor = texture(u_texture_sampler, out_texCords);" +
		  "}"
		 );

		//provide source code
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compile fragment shader
		GLES32.glCompileShader(fragmentShaderObject);

		//compile status
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

	   if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
	   {
		   GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

		   if (iInfoLogLength[0] > 0)
		   {
			   szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
			   System.out.println("SSD: fragment shader compilation log = " + szInfoLog);
			   uninitialize();
			   System.exit(0);
		   }
	   }


	   //shader program object
	   shaderProgramObject = GLES32.glCreateProgram();

	   //attach vertex shader
	   GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

	   //attach fragment shader
	   GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

	   //pre linking post attaching attribute binding
	   GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_VERTEX, "vPosition");
	   GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_TEXTURE, "texCords");

	   //linking progam
	   GLES32.glLinkProgram(shaderProgramObject);

	   iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompileStatus, 0);

	   if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
	   {
		   GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

		   if (iInfoLogLength[0] > 0)
		   {
			   szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
			   System.out.println("SSD: shader shader link log = " + szInfoLog);
			   uninitialize();
			   System.exit(0);
		   }
	   }


	   //get MVP uniform location
	   mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
	   textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");

	   //triangle vertices
	   final float[] smiley_vertices = new float[]
	   {
		   1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	   };

	  final float[] smiley1 = new float[]
	   {
		   1.0f, 1.0f,
		   0.0f, 1.0f,
		   0.0f, 0.0f, 
		   1.0f, 0.0f
	   };

	   GLES32.glGenVertexArrays(1, vao_smiley, 0);
	   GLES32.glBindVertexArray(vao_smiley[0]);


	   //vbo vertices smiley
	   GLES32.glGenBuffers(1, vbo_smiley_vertices, 0);
	   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_smiley_vertices[0]);

	   ByteBuffer byteBuffer = ByteBuffer.allocateDirect(smiley_vertices.length * 4);
	   byteBuffer.order(ByteOrder.nativeOrder());

	   FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
	   verticesBuffer.put(smiley_vertices);
	   verticesBuffer.position(0);

	   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, smiley_vertices.length * 4, verticesBuffer, 	GLES32.GL_STATIC_DRAW);
	   GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
	   GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);

	   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);



	   //vbo texture smiley
	   GLES32.glGenBuffers(1, vbo_smiley_texture, 0);
	   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_smiley_texture[0]);
	   
	   ByteBuffer byteBufferTexture = ByteBuffer.allocateDirect(smiley1.length * 4);
	   byteBufferTexture.order(ByteOrder.nativeOrder());
	   FloatBuffer texture_buffer = byteBufferTexture.asFloatBuffer();
	   texture_buffer.put(smiley1);
	   texture_buffer.position(0);
	   
	   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, smiley1.length * 4, texture_buffer, GLES32.GL_STATIC_DRAW);
	   GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_TEXTURE, 2, GLES32.GL_FLOAT, false, 0, 0);
	   GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_TEXTURE);
	   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	   
	   

	   GLES32.glBindVertexArray(0);



	   //enable depth testing
	   GLES32.glEnable(GLES32.GL_DEPTH_TEST);
	   GLES32.glDepthFunc(GLES32.GL_LEQUAL);

	   //GLES32.glEnable(GLES32.GL_CULL_FACE);

	   //load texture
	   smiley_texture = loadGLTexture(R.raw.smiley_texture);

	   //set background color
	   GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	   Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

   }

   private void resize(int width, int height)
   {
	   GLES32.glViewport(0, 0, width, height);

	   Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 1.0f, 100.0f);
   }

   public void display()
   {
	   GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	 

	   GLES32.glUseProgram(shaderProgramObject);

	   float modelViewMatrix[] = new float[16];
	   float modelViewProjectionMatrix[] = new float[16];

	   //set identity
	   Matrix.setIdentityM(modelViewMatrix, 0);
	   Matrix.setIdentityM(modelViewProjectionMatrix, 0);
	   Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.0f);


	   //multiply matrix
	   Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

	   //passing final matrix to shader
	   GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
	   
	   //texture binding
	   GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
	   GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, smiley_texture);
	   GLES32.glUniform1i(textureSamplerUniform, 0);
	   
	   //bind vao
	   GLES32.glBindVertexArray(vao_smiley[0]);

	   //drawing
	   GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
	   

	   //unbind vao
	   GLES32.glBindVertexArray(0);


	   GLES32.glUseProgram(0);

	   requestRender();
   }


   private int loadGLTexture(int imageFileResourceId)
{
	BitmapFactory.Options options = new BitmapFactory.Options();
	options.inScaled = false;
	Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceId, options);
	int texture[] = new int[1];

	GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
	GLES32.glGenTextures(1, texture, 0);
	GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
	GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	return texture[0];

	}

private void uninitialize()
	{
	//code
	//destroy vao
	if(vao_smiley[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_smiley, 0);
		vao_smiley[0] = 0;
	}
	
	if(vbo_smiley_texture[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_smiley_texture, 0);
		vbo_smiley_texture[0] = 0;
	}
	
	if(vbo_smiley_vertices[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_smiley_vertices, 0);
		vbo_smiley_vertices[0] = 0;
	}
	
	

	if (shaderProgramObject != 0)
	{
		if (vertexShaderObject != 0)
		{
			GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
			GLES32.glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;
		}

		if (fragmentShaderObject != 0)
		{
			GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
			GLES32.glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;
		}

		GLES32.glDeleteProgram(shaderProgramObject);
	}


}

}
