/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp.Android12;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.astromedicomp.Android12";
  public static final String BUILD_TYPE = "release";
}
