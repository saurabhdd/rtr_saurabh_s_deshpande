C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\java\com\astromedicomp\Android19\MainActivity.java:19: Warning: Field requires API level 19 (current min is 18): android.view.View#SYSTEM_UI_FLAG_IMMERSIVE [InlinedApi]
   getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
                                                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Explanation for issues of type "InlinedApi":
   This check scans through all the Android API field references in the
   application and flags certain constants, such as static final integers and
   Strings, which were introduced in later versions. These will actually be
   copied into the class files rather than being referenced, which means that
   the value is available even when running on older devices. In some cases
   that's fine, and in other cases it can result in a runtime crash or
   incorrect behavior. It depends on the context, so consider the code
   carefully and decide whether it's safe and can be suppressed or whether the
   code needs to be guarded.

   If you really want to use this API and don't need to support older devices
   just set the minSdkVersion in your build.gradle or AndroidManifest.xml
   files.

   If your code is deliberately accessing newer APIs, and you have ensured
   (e.g. with conditional execution) that this code will only ever be called
   on a supported platform, then you can annotate your class or method with
   the @TargetApi annotation specifying the local minimum SDK to apply, such
   as @TargetApi(11), such that this check considers 11 rather than your
   manifest file's minimum SDK as the required API level.

C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\layout\activity_main.xml:2: Warning: The resource R.layout.activity_main appears to be unused [UnusedResources]
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
^
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-anydpi-v26\ic_launcher.xml:2: Warning: The resource R.mipmap.ic_launcher appears to be unused [UnusedResources]
<adaptive-icon xmlns:android="http://schemas.android.com/apk/res/android">
^
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\drawable\ic_launcher_background.xml:2: Warning: The resource R.drawable.ic_launcher_background appears to be unused [UnusedResources]
<vector xmlns:android="http://schemas.android.com/apk/res/android"
^
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\drawable-v24\ic_launcher_foreground.xml:1: Warning: The resource R.drawable.ic_launcher_foreground appears to be unused [UnusedResources]
<vector xmlns:android="http://schemas.android.com/apk/res/android"
^
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-anydpi-v26\ic_launcher_round.xml:2: Warning: The resource R.mipmap.ic_launcher_round appears to be unused [UnusedResources]
<adaptive-icon xmlns:android="http://schemas.android.com/apk/res/android">
^

   Explanation for issues of type "UnusedResources":
   Unused resources make applications larger and slow down builds.

   The unused resource check can ignore tests. If you want to include
   resources that are only referenced from tests, consider packaging them in a
   test source set instead.

   You can include test sources in the unused resource check by setting the
   system property lint.unused-resources.include-tests=true, and to exclude
   them (usually for performance reasons), use
   lint.unused-resources.exclude-tests=true.

C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-hdpi\icon.png: Warning: Launcher icon used as round icon did not have a circular shape [IconLauncherShape]
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-mdpi\icon.png: Warning: Launcher icon used as round icon did not have a circular shape [IconLauncherShape]
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-xhdpi\icon.png: Warning: Launcher icon used as round icon did not have a circular shape [IconLauncherShape]
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-xxhdpi\icon.png: Warning: Launcher icon used as round icon did not have a circular shape [IconLauncherShape]
C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-xxxhdpi\icon.png: Warning: Launcher icon used as round icon did not have a circular shape [IconLauncherShape]

   Explanation for issues of type "IconLauncherShape":
   According to the Android Design Guide
   (https://material.io/design/iconography/) your launcher icons should "use a
   distinct silhouette", a "three-dimensional, front view, with a slight
   perspective as if viewed from above, so that users perceive some depth."

   The unique silhouette implies that your launcher icon should not be a
   filled square.

C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-xxxhdpi\icon.png: Warning: The image icon.png varies significantly in its density-independent (dip) size across the various density versions: mipmap-hdpi\icon.png: 48x48 dp (72x72 px), mipmap-mdpi\icon.png: 72x72 dp (72x72 px), mipmap-xhdpi\icon.png: 36x36 dp (72x72 px), mipmap-xxhdpi\icon.png: 24x24 dp (72x72 px), mipmap-xxxhdpi\icon.png: 18x18 dp (72x72 px) [IconDipSize]

   Explanation for issues of type "IconDipSize":
   Checks the all icons which are provided in multiple densities, all compute
   to roughly the same density-independent pixel (dip) size. This catches
   errors where images are either placed in the wrong folder, or icons are
   changed to new sizes but some folders are forgotten.

C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\res\mipmap-xxxhdpi\icon.png: Warning: The icon.png icon has identical contents in the following configuration folders: mipmap-hdpi, mipmap-mdpi, mipmap-xhdpi, mipmap-xxhdpi, mipmap-xxxhdpi [IconDuplicatesConfig]

   Explanation for issues of type "IconDuplicatesConfig":
   If an icon is provided under different configuration parameters such as
   drawable-hdpi or -v11, they should typically be different. This detector
   catches cases where the same icon is provided in different configuration
   folder which is usually not intentional.

C:\Backup\Backup\OpenGL\RTR-Compute\03-Android\01-Codes\MyAndroid\19-3LightsOnSphere\app\src\main\java\com\astromedicomp\Android19\GLESView.java:112: Warning: Custom view GLESView overrides onTouchEvent but not performClick [ClickableViewAccessibility]
 public boolean onTouchEvent(MotionEvent event)
                ~~~~~~~~~~~~

   Explanation for issues of type "ClickableViewAccessibility":
   If a View that overrides onTouchEvent or uses an OnTouchListener does not
   also implement performClick and call it when clicks are detected, the View
   may not handle accessibility actions properly. Logic handling the click
   actions should ideally be placed in View#performClick as some accessibility
   services invoke performClick when a click action should occur.

0 errors, 14 warnings
