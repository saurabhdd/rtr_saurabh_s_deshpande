/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.astromedicomp";
  public static final String BUILD_TYPE = "debug";
}
