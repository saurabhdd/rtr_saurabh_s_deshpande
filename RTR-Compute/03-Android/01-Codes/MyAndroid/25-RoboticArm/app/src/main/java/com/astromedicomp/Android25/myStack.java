package com.astromedicomp.Android25;

import java.util.*;

public class myStack{

	public int top;
	public ArrayList<Float> data;

    public myStack()
	{
		 top = -1;
	    data = new ArrayList<>();
	}
   
	
	public void PushMatrix(float[] matrix)
	{
		
		data.add(++this.top, matrix[0]);
		data.add(++this.top, matrix[1]);
		data.add(++this.top, matrix[2]);
		data.add(++this.top, matrix[3]);
		
		data.add(++this.top, matrix[4]);
		data.add(++this.top, matrix[5]);
		data.add(++this.top, matrix[6]);
		data.add(++this.top, matrix[7]);
	
		data.add(++this.top, matrix[8]);
		data.add(++this.top, matrix[9]);
		data.add(++this.top, matrix[10]);
		data.add(++this.top, matrix[11]);
		
		data.add(++this.top, matrix[12]);
		data.add(++this.top, matrix[13]);
		data.add(++this.top, matrix[14]);
		data.add(++this.top, matrix[15]);
	}
	
	public float[] PopMatrix()
	{
		float temp[] = new float[16];
		temp[15] = data.remove(this.top--);
		temp[14] = data.remove(this.top--);
		temp[13] = data.remove(this.top--);
		temp[12] = data.remove(this.top--);
		
		temp[11] = data.remove(this.top--);
		temp[10] = data.remove(this.top--);
		temp[9] = data.remove(this.top--);
		temp[8] = data.remove(this.top--);
		
		temp[7] = data.remove(this.top--);
		temp[6] = data.remove(this.top--);
		temp[5] = data.remove(this.top--);
		temp[4] = data.remove(this.top--);
		
		temp[3] = data.remove(this.top--);
		temp[2] = data.remove(this.top--);
		temp[1] = data.remove(this.top--);
		temp[0] = data.remove(this.top--);
	
		return temp;
	}

}