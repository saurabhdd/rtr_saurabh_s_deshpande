/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp.Android25;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.astromedicomp.Android25";
  public static final String BUILD_TYPE = "release";
}
