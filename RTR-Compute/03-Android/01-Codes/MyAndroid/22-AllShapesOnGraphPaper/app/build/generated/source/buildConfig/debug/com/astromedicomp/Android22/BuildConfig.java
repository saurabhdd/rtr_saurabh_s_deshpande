/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.astromedicomp.Android22;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.astromedicomp.Android22";
  public static final String BUILD_TYPE = "debug";
}
