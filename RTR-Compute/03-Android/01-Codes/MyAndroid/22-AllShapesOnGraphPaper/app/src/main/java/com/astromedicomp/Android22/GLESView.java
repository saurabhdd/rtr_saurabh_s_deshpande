package com.astromedicomp.Android22;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //egl = embedded egl (cause opengl-es calles internally to embedded opengl)

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer{
	
	private final Context context;
	private GestureDetector gestureDetector;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int vbo_position_middle[] = new int[1];
	private int vbo_color_middle[] = new int[1];
	private int vbo_position_x[] = new int[1];
	private int vbo_position_y[] = new int[1];
	private int vao_y[] = new int[1];
	private int vao_x[] = new int[1];
	private int vao_middle[] = new int[1];
	private int vao_circle[] = new int[1];
    private int vao_rectangle[] = new int[1];
    private int vao_triangle[] = new int[1];
    private int vbo_position_circle[] = new int[1];
    private int vbo_position_rectangle[] = new int[1]; 
    private int vbo_position_triangle[] = new int[1];
    private int vbo_color_circle[] = new int[1];
    private int vbo_color_rectangle[] = new int[1];
    private int vbo_color_triangle[] = new int[1];
	
	private float GL_PI = 3.14159f;
	
	private int mvpUniform;
	private float perspectiveProjectionMatrix[] = new float[16];
	
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		this.setEGLContextClientVersion(3); //opengl ndk is server and opengl sdk is client so we are setting client version to 3	
		this.setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//override method of GLSurfaceView.Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		//String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE);
		System.out.println("SSD: " + glesVersion);
		//System.out.println("SSD: " + glslVersion);
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float veclocityX, float velocityY)
	{
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return true;
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	private void initialize(GL10 gl)
	{
		//vertex shader source
		final String vertexShaderSourceCode = String.format
		(
		  "#version 320 es"+
		  "\n"+
		  "in vec4 vPosition;"+
		  "in vec4 vColor;"+
		  "out vec4 color;"+
		  "uniform mat4 u_mvp_matrix;"+
		  "void main(void)"+
		  "{"+
		  "gl_Position = u_mvp_matrix * vPosition;"+
		  "color = vColor;"+
		  "}"
		 );
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile source code
		GLES32.glCompileShader(vertexShaderObject);
		
		//compile staus error chec
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("SSD: vertex shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//fragment shader
		//create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		//fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		  "#version 320 es"+
		  "\n"+
		  "precision highp float;"+
		  "in vec4 color;"+
		  "out vec4 fragColor;"+
		  "void main(void)"+
		  "{"+
		  "fragColor = color;"+
		  "}"
		 );
		 
		 //provide source code
		 GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		 
		 //compile fragment shader
		 GLES32.glCompileShader(fragmentShaderObject);
		 
		 //compile status
		 iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("SSD: fragment shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//shader program object
		shaderProgramObject = GLES32.glCreateProgram();
		
		//attach vertex shader
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		
		//attach fragment shader
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		//pre linking post attaching attribute binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_COLOR, "vColor");
		
		//linking progam
		GLES32.glLinkProgram(shaderProgramObject);
		
		iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("SSD: shader shader link log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//get MVP uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		
	   float graphXVertices[] = new float[240];
	   float i = 0.0f;
	   int counter = 0;
	 for (i = i + (1.0f / 21.0f); i <= 1.0f; i = i + (1.0f / 20.0f))
	 {
		graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0f;
		 else
			 graphXVertices[counter++] = 1.0f;
		 graphXVertices[counter++] = 0.0f;

		 //fprintf_s(gbFile_SSD, "\n");
		 //fprintf_s(gbFile_SSD, "{%f, %f, %f}, ", graphXVertices[counter - 3], graphXVertices[counter - 2], graphXVertices[counter - 1]);

		 graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0f;
		 else
			 graphXVertices[counter++] = 1.0f;
		 graphXVertices[counter++] = 0.0f;

		// fprintf_s(gbFile_SSD, "{%f, %f, %f}, ", graphXVertices[counter - 3], graphXVertices[counter - 2], graphXVertices[counter - 1]);

	 }
		
		
		i = 0.0f;

	 for (i = i - (1.0f / 21.0f); i >= -1.0f; i = i - (1.0f / 20.0f))
	 {
		 graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0f;
		 else
			 graphXVertices[counter++] = 1.0f;
		 graphXVertices[counter++] = 0.0f;


		// fprintf_s(gbFile_SSD, "\n");
		 
		 //fprintf_s(gbFile_SSD, "{%f, %f, %f}, ", graphXVertices[counter - 3], graphXVertices[counter - 2], graphXVertices[counter - 1]);

		 graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0f;
		 else
			 graphXVertices[counter++] = 1.0f;
		 graphXVertices[counter++] = 0.0f;

		// fprintf_s(gbFile_SSD, "{%f, %f, %f}, ", graphXVertices[counter - 3], graphXVertices[counter - 2], graphXVertices[counter - 1]);
		 
	 }
	 
	 
	 
	 //y vertices*********************************************
	 i = 0;
	 counter = 0;
	 float graphYVertices[] = new float[240];

	 for (i = i + (1.0f / 21.0f); i <= 1.0f; i = i + (1.0f / 20.0f))
	 {
		 
		 if (counter % 2 != 0)
			 graphYVertices[counter++] = -1.0f;
		 else
			 graphYVertices[counter++] = 1.0f;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0f;




		 if (counter % 2 != 0)
			 graphYVertices[counter++] = -1.0f;
		 else
			 graphYVertices[counter++] = 1.0f;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0f;

	 }
	 
	 
	 
	 i = 0;

	 for (i = i - (1.0f / 21.0f); i >= -1.0f; i = i - (1.0f / 20.0f))
	 {
		 if (counter % 2 != 0)
			 graphYVertices[counter++] = -1.0f;
		 else
			 graphYVertices[counter++] = 1.0f;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0f;




		 if (counter % 2 != 0)
			 graphYVertices[counter++] = -1.0f;
		 else
			 graphYVertices[counter++] = 1.0f;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0f;

	 }
		
		
		final float middleLines[] =
	 {
		 0.0f, 1.0f, 0.0f,
		 0.0f, -1.0f, 0.0f,
		 1.0f, 0.0f, 0.0f,
		 -1.0f, 0.0f, 0.0f
	 };

	 final float middleColors[] =
	 {
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f, 
		 0.0f, 1.0f, 0.0f
	 };
		
		
	   float circleVertices[] = new float[300];
	  
	   final float radius = 0.50f;
	   counter = 0;
	   i = 0.0f;
	   
	 for ( i = 0.0f; i <= 2.0f * GL_PI; i = i + (2.0f * GL_PI / 100))
	   {
		 circleVertices[counter++] = radius * (float)Math.cos(i);
		 circleVertices[counter++] = radius * (float)Math.sin(i);
		 circleVertices[counter++] = 0.0f;
	   }

	 //circle colors
	 float circleColors[] = new float[300];
	 counter = 0;
	 for (int i_c = 0; i_c < 100; i_c++)
	 {
		 circleColors[counter++] = 0.0f;
		 circleColors[counter++] = 1.0f;
		 circleColors[counter++] = 0.0f;
	 }
		
		
		
		//rectangle vertices
	 final float RECTANGLE_SIDE = radius / 1.414f;
	 final float rectangleVertices[] =
	 {
		 (float)RECTANGLE_SIDE, (float)RECTANGLE_SIDE, 0.0f,
		 -(float)RECTANGLE_SIDE, (float)RECTANGLE_SIDE, 0.0f,
		 -(float)RECTANGLE_SIDE, -(float)RECTANGLE_SIDE, 0.0f,
		 (float)RECTANGLE_SIDE, -(float)RECTANGLE_SIDE, 0.0f,
	 };

	 //rectangle colors
	 final float rectangle_colors[] =
	 {
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
	 };
	 
	 
	 
	 //triangle vertices
	 final float triangleVertices[] =
	 {
		 -(float)RECTANGLE_SIDE, -(float)RECTANGLE_SIDE, 0.0f,
		 (float)RECTANGLE_SIDE, -(float)RECTANGLE_SIDE, 0.0f,
		 0.0f, (float)RECTANGLE_SIDE, 0.0f
	 };

	 //triangle colors
	 final float triangleColors[] =
	 {
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
		 0.0f, 1.0f, 0.0f,
	 };

		
		
		GLES32.glGenVertexArrays(1, vao_middle, 0);
		GLES32.glBindVertexArray(vao_middle[0]);
		
		GLES32.glGenBuffers(1, vbo_position_middle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_middle[0]);
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(middleLines.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(middleLines);
		verticesBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, middleLines.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glGenBuffers(1, vbo_color_middle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_middle[0]);
		ByteBuffer  byteBufferColor = ByteBuffer.allocateDirect(middleColors.length * 4);
		byteBufferColor.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBufferColor.asFloatBuffer();
		colorBuffer.put(middleColors);
		colorBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, middleColors.length * 4, colorBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		
		
		//vao for outer lines*********************
		GLES32.glGenVertexArrays(1, vao_x, 0);
		GLES32.glBindVertexArray(vao_x[0]);
		
		GLES32.glGenBuffers(1, vbo_position_x, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_x[0]);
		ByteBuffer byteBuffer_x = ByteBuffer.allocateDirect(graphXVertices.length * 4);
		byteBuffer_x.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer_x = byteBuffer_x.asFloatBuffer();
		verticesBuffer_x.put(graphXVertices);
		verticesBuffer_x.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphXVertices.length * 4, verticesBuffer_x, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glVertexAttrib3f(GLESMacros.SSD_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);
		
		GLES32.glBindVertexArray(0);
		
		
		
		
		GLES32.glGenVertexArrays(1, vao_y, 0);
		GLES32.glBindVertexArray(vao_y[0]);
		
		GLES32.glGenBuffers(1, vbo_position_y, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_y[0]);
		ByteBuffer byteBuffer_y = ByteBuffer.allocateDirect(graphYVertices.length * 4);
		byteBuffer_y.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer_y = byteBuffer_y.asFloatBuffer();
		verticesBuffer_y.put(graphYVertices);
		verticesBuffer_y.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphYVertices.length * 4, verticesBuffer_y, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		
		
		//vao for triangle
		GLES32.glGenVertexArrays(1, vao_triangle, 0);
		GLES32.glBindVertexArray(vao_triangle[0]);
		
		GLES32.glGenBuffers(1, vbo_position_triangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_triangle[0]);
		ByteBuffer byteBufferTrianglePosition = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		byteBufferTrianglePosition.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBufferTriangle = byteBufferTrianglePosition.asFloatBuffer();
		verticesBufferTriangle.put(triangleVertices);
		verticesBufferTriangle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, verticesBufferTriangle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glGenBuffers(1, vbo_color_triangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_triangle[0]);
		ByteBuffer  byteBufferTriangleColor = ByteBuffer.allocateDirect(triangleColors.length * 4);
		byteBufferTriangleColor.order(ByteOrder.nativeOrder());
		FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();
		colorBufferTriangle.put(triangleColors);
		colorBufferTriangle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleColors.length * 4, colorBufferTriangle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		
		
		//rectangle vao binding
		GLES32.glGenVertexArrays(1, vao_rectangle, 0);
		GLES32.glBindVertexArray(vao_rectangle[0]);
		
		GLES32.glGenBuffers(1, vbo_position_rectangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_rectangle[0]);
		ByteBuffer byteBufferRectanglePosition = ByteBuffer.allocateDirect(rectangleVertices.length * 4);
		byteBufferRectanglePosition.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBufferRectangle = byteBufferRectanglePosition.asFloatBuffer();
		verticesBufferRectangle.put(rectangleVertices);
		verticesBufferRectangle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, rectangleVertices.length * 4, verticesBufferRectangle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glGenBuffers(1, vbo_color_rectangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_rectangle[0]);
		ByteBuffer  byteBufferRectangleColor = ByteBuffer.allocateDirect(rectangle_colors.length * 4);
		byteBufferRectangleColor.order(ByteOrder.nativeOrder());
		FloatBuffer colorBufferRectangle = byteBufferRectangleColor.asFloatBuffer();
		colorBufferRectangle.put(rectangle_colors);
		colorBufferRectangle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, rectangle_colors.length * 4, colorBufferRectangle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glBindVertexArray(0);
		
		
		//vao for circle
		GLES32.glGenVertexArrays(1, vao_circle, 0);
		GLES32.glBindVertexArray(vao_circle[0]);
		
		GLES32.glGenBuffers(1, vbo_position_circle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_circle[0]);
		ByteBuffer byteBufferCirclePosition = ByteBuffer.allocateDirect(circleVertices.length * 4);
		byteBufferCirclePosition.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBufferCircle = byteBufferCirclePosition.asFloatBuffer();
		verticesBufferCircle.put(circleVertices);
		verticesBufferCircle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleVertices.length * 4, verticesBufferCircle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glGenBuffers(1, vbo_color_circle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_circle[0]);
		ByteBuffer  byteBufferCircleColor = ByteBuffer.allocateDirect(circleColors.length * 4);
		byteBufferCircleColor.order(ByteOrder.nativeOrder());
		FloatBuffer colorBufferCircle = byteBufferCircleColor.asFloatBuffer();
		colorBufferCircle.put(circleColors);
		colorBufferCircle.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleColors.length * 4, colorBufferCircle, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glBindVertexArray(0);
		
		
		
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		//set background color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float) height, 1.0f, 100.0f);
	}
	
	public void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		
		//set identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.0f);
		
		//multiply matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		//passing final matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_x[0]);
	    GLES32.glDrawArrays(GLES32.GL_LINES, 0, 80);
	    GLES32.glBindVertexArray(0);

	    GLES32.glBindVertexArray(vao_y[0]);
	    GLES32.glDrawArrays(GLES32.GL_LINES, 0, 80);
	    GLES32.glBindVertexArray(0);

	    GLES32.glBindVertexArray(vao_middle[0]);
	    GLES32.glDrawArrays(GLES32.GL_LINES, 0, 4);
	    GLES32.glBindVertexArray(0);
		
		GLES32.glBindVertexArray(vao_circle[0]);
	    GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 100);
	    GLES32.glBindVertexArray(0);

	    GLES32.glBindVertexArray(vao_rectangle[0]);
	    GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 4);
	    GLES32.glBindVertexArray(0);

	    GLES32.glBindVertexArray(vao_triangle[0]);   
	    GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 3);
	    GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		requestRender();
	}
	
	private void uninitialize()
	{
		//code
		//destroy vao
		if(vao_middle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_middle, 0);
			vao_middle[0] = 0;
		}
		
		if(vao_x[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_x, 0);
			vao_x[0] = 0;
		}
		
		if(vao_y[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_y, 0);
			vao_y[0] = 0;
		}
		
		if(vao_triangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_triangle, 0);
			vao_triangle[0] = 0;
		}
		
		if(vao_circle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_circle, 0);
			vao_circle[0] = 0;
		}
		
		if(vao_rectangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_rectangle, 0);
			vao_rectangle[0] = 0;
		}
		
		//destroy vbo
		if(vbo_position_middle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_middle, 0);
			vbo_position_middle[0] = 0;
		}
		
		if(vbo_color_middle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_color_middle, 0);
			vbo_color_middle[0] = 0;
		}
		
		if(vbo_position_x[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_x, 0);
			vbo_position_x[0] = 0;
		}
		
		
		if(vbo_position_y[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_y, 0);
			vbo_position_y[0] = 0;
		}
		
		if(vbo_position_triangle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_triangle, 0);
			vbo_position_triangle[0] = 0;
		}
		
		if(vbo_color_triangle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_color_triangle, 0);
			vbo_color_triangle[0] = 0;
		}
		
		
		if(vbo_position_circle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_circle, 0);
			vbo_position_circle[0] = 0;
		}
		
		if(vbo_color_circle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_color_circle, 0);
			vbo_color_circle[0] = 0;
		}
		
		if(vbo_position_rectangle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_rectangle, 0);
			vbo_position_rectangle[0] = 0;
		}
		
		if(vbo_color_rectangle[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_color_rectangle, 0);
			vbo_color_rectangle[0] = 0;
		}
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		
	}
	
}