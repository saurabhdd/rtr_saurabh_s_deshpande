package com.astromedicomp.Android24;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //egl = embedded egl (cause opengl-es calles internally to embedded opengl)

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer{
	
	private final Context context;
	private GestureDetector gestureDetector;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	
	
	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;
	private int laUniform;
	private int ldUniform;
	private int lsUniform;
	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int shininessUniform;
	private int lKeyPressedUniform;
	private int lightDirectionUniform;
	

    float xDimension1 = 1.6f;
    float xDimension2 = 5.150f;
    float yDimension1 = -3.65f / 1.20f;
    float yDimension2 = -2.20f / 1.20f;
    float yDimension3 = -0.80f / 1.20f;
    float yDimension4 = 0.60f / 1.20f;
    float yDimension5 = 2.0f / 1.20f;
    float yDimension6 = 3.50f / 1.20f;
	
	float lightPosition[] = new float[] { 100.0f, 100.0f, 100.0f, 1.0f };;
	float ambientLight[] = new float[] { 0.0f, 0.0f, 0.0f };
	float diffuseLight[] = new float[] { 1.0f, 1.0f, 1.0f };;
	float specularLight[] = new float[] { 0.7f, 0.7f, 0.7f };
	
	private static boolean gbLighting = false;
	private static boolean gbAnimate = false;
	//private float yRotate = 0.0f;
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private int numVertices;
	private int numElements;
	
	
	float xRotate;
    float yRotate;
    float zRotate;
    float xRotate_r;
    float yRotate_r;
    float zRotate_r;

    int keyPressed = 0;
	
	float GL_PI = 3.14159f;

    float radius = 400.0f;
	
	    ByteBuffer byteBufferAmbient;
		FloatBuffer lightAmbientBuffer;
		ByteBuffer byteBufferDiffuse;
		FloatBuffer lightDiffuseBuffer;
	    ByteBuffer byteBufferSpecular;
		FloatBuffer lightSpecularBuffer;
	
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		this.setEGLContextClientVersion(3); //opengl ndk is server and opengl sdk is client so we are setting client version to 3	
		this.setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//override method of GLSurfaceView.Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		//String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE);
		System.out.println("SSD: " + glesVersion);
		//System.out.println("SSD: " + glslVersion);
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		if(gbLighting == false)
			gbLighting = true;
		else
			gbLighting = false;
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		keyPressed++;
		if(keyPressed > 3)
			keyPressed = 0;
		
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float veclocityX, float velocityY)
	{
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return true;
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	private void initialize(GL10 gl)
	{
		//vertex shader source
		final String vertexShaderSourceCode = String.format
		(
		 "#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec3 u_la[2];" +
		"uniform mediump vec3 u_ld[2];" +
		"uniform mediump vec3 u_ls[2];" +
		"uniform mediump vec3 u_kd;" +
		"uniform mediump vec3 u_ka;" +
		"uniform mediump vec3 u_ks;" +
		"uniform mediump vec4 u_light_position[2];" +
		"uniform mediump float u_shininess;" +
		"out vec3 fong_ads_light;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"vec3 light_direction[2];" +
		"vec3 reflection_vector[2];" +
		"vec3 ambient_light[2];" +
		"vec3 diffuse_light[2];" +
		"vec3 specular_light[2];" +
		"if(l_key_pressed == 1)" +
		"{" +
		"fong_ads_light = vec3(0.0, 0.0, 0.0);" +
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" +
		"for(i = 0; i < 2; i++)" +
		"{" +
		"light_direction[i] = normalize(vec3(u_light_position[i] - eye_cordinate));" +
		"reflection_vector[i] = reflect(-light_direction[i], trasformed_normal);" +
		"ambient_light[i] = u_la[i] * u_ka;" +
		"diffuse_light[i] = u_ld[i] * u_kd * max(dot(light_direction[i], trasformed_normal), 0.0);" +
		"specular_light[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i], view_vector), 0.0), u_shininess);" +
		"fong_ads_light =  fong_ads_light + ambient_light[i] + diffuse_light[i] + specular_light[i];" +
		"}" +
		"}" +
		"else" +
		"{" +
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix *  u_model_matrix * vPosition;" +
		"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile source code
		GLES32.glCompileShader(vertexShaderObject);
		
		//compile staus error chec
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("SSD: vertex shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//fragment shader
		//create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		//fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		 "#version 320 es" +
		"\n" +
		"precision highp float;"+
		"in vec3 fong_ads_light;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"fragColor = vec4(fong_ads_light, 1.0);" +
		"}"
		);
		 
		 //provide source code
		 GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		 
		 //compile fragment shader
		 GLES32.glCompileShader(fragmentShaderObject);
		 
		 //compile status
		 iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("SSD: fragment shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//shader program object
		shaderProgramObject = GLES32.glCreateProgram();
		
		//attach vertex shader
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		
		//attach fragment shader
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		//pre linking post attaching attribute binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
		
		//linking progam
		GLES32.glLinkProgram(shaderProgramObject);
		
		iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("SSD: shader shader link log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//get MVP uniform location
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		lightDirectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		shininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_shininess");
		lKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "l_key_pressed");
		
		
		
		 byteBufferAmbient = ByteBuffer.allocateDirect(ambientLight.length * 4);
		byteBufferAmbient.order(ByteOrder.nativeOrder());
		 lightAmbientBuffer = byteBufferAmbient.asFloatBuffer();
		lightAmbientBuffer.put(ambientLight);
		lightAmbientBuffer.position(0);
		
		 byteBufferDiffuse = ByteBuffer.allocateDirect(diffuseLight.length * 4);
		byteBufferDiffuse.order(ByteOrder.nativeOrder());
		 lightDiffuseBuffer = byteBufferDiffuse.asFloatBuffer();
		lightDiffuseBuffer.put(diffuseLight);
		lightDiffuseBuffer.position(0);
		
		 byteBufferSpecular = ByteBuffer.allocateDirect(specularLight.length * 4);
		byteBufferSpecular.order(ByteOrder.nativeOrder());
		 lightSpecularBuffer = byteBufferSpecular.asFloatBuffer();
		lightSpecularBuffer.put(specularLight);
		lightSpecularBuffer.position(0);
		
		
		
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        
		numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();
		
		
		
		
		
		// vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		//set background color
		GLES32.glClearColor(0.250f, 0.250f, 0.250f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float) height, 1.0f, 100.0f);
	}
	
	public void display()
	{
		    float[] ambientMaterial_1 = {0.0215f, 0.1745f, 0.0215f};
	float[] diffuseMaterial_1 = {0.18275f, 0.17f, 0.22525f};
	float[] specularMaterial_1 = {0.332741f, 0.328634f, 0.346435f};

	float[] ambientMaterial_2 = {0.135f, 0.2225f, 0.1575f};
	float[] diffuseMaterial_2 = {0.54f, 0.89f, 0.63f};
	float[] specularMaterial_2 = {0.316228f, 0.316228f, 0.316228f};

	float[] ambientMaterial_3 = {0.05375f, 0.05f, 0.06625f};
	float[] diffuseMaterial_3 = {0.18275f, 0.17f, 0.22525f};
	float[] specularMaterial_3 = {0.332741f, 0.328634f, 0.346435f};

	float[] ambientMaterial_4 = {0.25f, 0.20725f, 0.20725f};
	float[] diffuseMaterial_4 = {1.0f, 0.829f, 0.829f};
	float[] specularMaterial_4 = {0.296648f, 0.296648f, 0.296648f};

	float[] ambientMaterial_5 = {0.1745f, 0.01175f, 0.01175f};
	float[] diffuseMaterial_5 = {0.61424f, 0.04136f, 0.04136f};
	float[] specularMaterial_5 = {0.727811f, 0.626959f, 0.626959f};

	float[] ambientMaterial_6 = {0.1f, 0.18725f, 0.1745f};
	float[] diffuseMaterial_6 = {0.396f, 0.74151f, 0.69102f};
	float[] specularMaterial_6 = {0.297254f, 0.30829f, 0.306678f};

	float[] ambientMaterial_7 = {0.329412f, 0.223529f, 0.027451f};
	float[] diffuseMaterial_7 = {0.780392f, 0.568627f, 0.113725f};
	float[] specularMaterial_7 = {0.992157f, 0.941176f, 0.807843f};

	float[] ambientMaterial_8 = {0.2125f, 0.1275f, 0.054f};
	float[] diffuseMaterial_8 = {0.714f, 0.4284f, 0.18144f};
	float[] specularMaterial_8 = {0.393548f, 0.271906f, 0.166721f};

	float[] ambientMaterial_9 = {0.25f, 0.25f, 0.25f};
	float[] diffuseMaterial_9 = {0.4f, 0.4f, 0.4f};
	float[] specularMaterial_9 = {0.774597f, 0.774597f, 0.774597f};

	float[] ambientMaterial_10 = {0.19125f, 0.0735f, 0.0225f};
	float[] diffuseMaterial_10 = {0.7038f, 0.27048f, 0.0828f};
	float[] specularMaterial_10 = {0.256777f, 0.137622f, 0.086014f};

	float[] ambientMaterial_11 = {0.24725f, 0.1995f, 0.0745f};
	float[] diffuseMaterial_11 = {0.75164f, 0.60648f, 0.22648f};
	float[] specularMaterial_11 = {0.628281f, 0.555802f, 0.366065f};

	float[] ambientMaterial_12 = {0.19225f, 0.19225f, 0.19225f};
	float[] diffuseMaterial_12 = {0.50754f, 0.50754f, 0.50754f};
	float[] specularMaterial_12 = {0.508273f, 0.508273f, 0.508273f};

	float[] ambientMaterial_13 = {0.0f, 0.0f, 0.0f};
	float[] diffuseMaterial_13 = {0.01f, 0.01f, 0.01f};
	float[] specularMaterial_13 = {0.50f, 0.50f, 0.50f};

	float[] ambientMaterial_14 = {0.0f, 0.1f, 0.06f};
	float[] diffuseMaterial_14 = {0.0f, 0.50980392f, 0.50980392f};
	float[] specularMaterial_14 = {0.50196078f, 0.50196078f, 0.50196078f};

	float[] ambientMaterial_15 = {0.0f, 0.0f, 0.0f};
	float[] diffuseMaterial_15 = {0.1f, 0.35f, 0.1f};
	float[] specularMaterial_15 = {0.45f, 0.55f, 0.45f};

	float[] ambientMaterial_16 = {0.0f, 0.0f, 0.0f};
	float[] diffuseMaterial_16 = {0.5f, 0.0f, 0.0f};
	float[] specularMaterial_16 = {0.7f, 0.6f, 0.6f};

	float[] ambientMaterial_17 = {0.0f, 0.0f, 0.0f};
	float[] diffuseMaterial_17 = {0.55f, 0.55f, 0.55f};
	float[] specularMaterial_17 = {0.70f, 0.70f, 0.70f};

	float[] ambientMaterial_18 = {0.0f, 0.0f, 0.0f};
	float[] diffuseMaterial_18 = {0.5f, 0.5f, 0.0f};
	float[] specularMaterial_18 = {0.60f, 0.60f, 0.50f};

	float[] ambientMaterial_19 = {0.02f, 0.02f, 0.02f};
	float[] diffuseMaterial_19 = {0.01f, 0.01f, 0.01f};
	float[] specularMaterial_19 = {0.4f, 0.4f, 0.4f};

	float[] ambientMaterial_20 = {0.0f, 0.05f, 0.05f};
	float[] diffuseMaterial_20 = {0.4f, 0.5f, 0.5f};
	float[] specularMaterial_20 = {0.04f, 0.7f, 0.7f};

	float[] ambientMaterial_21 = {0.0f, 0.05f, 0.0f};
	float[] diffuseMaterial_21 = {0.4f, 0.5f, 0.4f};
	float[] specularMaterial_21 = {0.04f, 0.7f, 0.04f};

	float[] ambientMaterial_22 = {0.05f, 0.0f, 0.0f};
	float[] diffuseMaterial_22 = {0.5f, 0.4f, 0.4f};
	float[] specularMaterial_22 = {0.7f, 0.04f, 0.04f};

	float[] ambientMaterial_23 = {0.05f, 0.05f, 0.05f};
	float[] diffuseMaterial_23 = {0.5f, 0.5f, 0.5f};
	float[] specularMaterial_23 = {0.7f, 0.7f, 0.7f};

	float[] ambientMaterial_24 = {0.05f, 0.05f, 0.0f};
	float[] diffuseMaterial_24 = {0.5f, 0.5f, 0.4f};
	float[] specularMaterial_24 = {0.7f, 0.7f, 0.04f};
	
	
	    float MShininess1_SSD = 0.3f * 128f;
	float MShininess2_SSD = 0.3f * 128f;
	float MShininess3_SSD = 0.3f * 128f;
	float MShininess4_SSD = 0.3f * 128f;
	float MShininess5_SSD = 0.6f * 128f;
	float MShininess6_SSD = 0.1f * 128f;
	float MShininess7_SSD = 0.21794872f * 128f;
	float MShininess8_SSD = 0.2f * 128f;
	float MShininess9_SSD = 0.6f * 128f;
	float MShininess10_SSD = 0.1f * 128f;
	float MShininess11_SSD = 0.4f * 128f;
	float MShininess12_SSD = 0.4f * 128f;
	float MShininess13_SSD = 0.25f * 128f;
	float MShininess14_SSD = 0.25f * 128f;
	float MShininess15_SSD = 0.25f * 128f;
	float MShininess16_SSD = 0.25f * 128f;
	float MShininess17_SSD = 0.25f * 128f;
	float MShininess18_SSD = 0.25f * 128f;
	float MShininess19_SSD = 0.078125f * 128f;
	float MShininess20_SSD = 0.078125f * 128f;
	float MShininess21_SSD = 0.078125f * 128f;
	float MShininess22_SSD = 0.078125f * 128f;
	float MShininess23_SSD = 0.078125f * 128f;
	float MShininess24_SSD = 0.078125f * 128f;
		
		
		if (keyPressed == 1)
	{
		lightPosition[0] = 0.0f;
		lightPosition[1] = radius * (float)Math.sin(xRotate_r);
		lightPosition[2] = radius * (float)Math.cos(xRotate_r);
	}
	else if (keyPressed == 2)
	{
		lightPosition[0] = radius * (float)Math.cos(xRotate_r);
		lightPosition[1] = 0.0f;
		lightPosition[2] = radius * (float)Math.sin(xRotate_r);
	}
	else if (keyPressed == 3)
	{
		lightPosition[0] = radius * (float)Math.sin(xRotate_r);
		lightPosition[1] = radius * (float)Math.cos(xRotate_r);
		lightPosition[2] = 0.0f;
	}
		
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject); 
		
		ByteBuffer byteBufferDirection = ByteBuffer.allocateDirect(lightPosition.length * 4);
		byteBufferDirection.order(ByteOrder.nativeOrder());
		FloatBuffer lightDirectionBuffer = byteBufferDirection.asFloatBuffer();
		lightDirectionBuffer.put(lightPosition);
		lightDirectionBuffer.position(0);
		
		
		//material
		/*ByteBuffer byteMaterialAmbient = ByteBuffer.allocateDirect(materialAmbient.length * 4);
		byteMaterialAmbient.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialABuffer = byteMaterialAmbient.asFloatBuffer();
		lightMaterialABuffer.put(materialAmbient);
		lightMaterialABuffer.position(0);
		
		ByteBuffer byteMaterialDiffuse = ByteBuffer.allocateDirect(materialDiffuse.length * 4);
		byteMaterialDiffuse.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialDBuffer = byteMaterialDiffuse.asFloatBuffer();
		lightMaterialDBuffer.put(materialDiffuse);
		lightMaterialDBuffer.position(0);
		
		ByteBuffer byteMaterialSpecular = ByteBuffer.allocateDirect(materialSpecular.length * 4);
		byteMaterialSpecular.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialSBuffer = byteMaterialSpecular.asFloatBuffer();
		lightMaterialSBuffer.put(materialSpecular);
		lightMaterialSBuffer.position(0);*/
		
		
		
		
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float translateMatrix[] = new float[16];
		
		
		//set identity
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension6, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_1), toFloatBuffer(diffuseMaterial_1), toFloatBuffer(specularMaterial_1), lightDirectionBuffer, MShininess1_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension6, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_2), toFloatBuffer(diffuseMaterial_2), toFloatBuffer(specularMaterial_2), lightDirectionBuffer, MShininess2_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension6, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_3), toFloatBuffer(diffuseMaterial_3), toFloatBuffer(specularMaterial_3), lightDirectionBuffer, MShininess3_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension6, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_4), toFloatBuffer(diffuseMaterial_4), toFloatBuffer(specularMaterial_4), lightDirectionBuffer, MShininess4_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension5, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_5), toFloatBuffer(diffuseMaterial_5), toFloatBuffer(specularMaterial_5), lightDirectionBuffer, MShininess5_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension5, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_6), toFloatBuffer(diffuseMaterial_6), toFloatBuffer(specularMaterial_6), lightDirectionBuffer, MShininess6_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension5, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_7), toFloatBuffer(diffuseMaterial_7), toFloatBuffer(specularMaterial_7), lightDirectionBuffer, MShininess7_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension5, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_8), toFloatBuffer(diffuseMaterial_8), toFloatBuffer(specularMaterial_8), lightDirectionBuffer, MShininess8_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension4, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_9), toFloatBuffer(diffuseMaterial_9), toFloatBuffer(specularMaterial_9), lightDirectionBuffer, MShininess9_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension4, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_10), toFloatBuffer(diffuseMaterial_10), toFloatBuffer(specularMaterial_10), lightDirectionBuffer, MShininess10_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension4, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_11), toFloatBuffer(diffuseMaterial_11), toFloatBuffer(specularMaterial_11), lightDirectionBuffer, MShininess11_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension4, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_12), toFloatBuffer(diffuseMaterial_12), toFloatBuffer(specularMaterial_12), lightDirectionBuffer, MShininess12_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension3, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_13), toFloatBuffer(diffuseMaterial_13), toFloatBuffer(specularMaterial_13), lightDirectionBuffer, MShininess13_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension3, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_14), toFloatBuffer(diffuseMaterial_14), toFloatBuffer(specularMaterial_14), lightDirectionBuffer, MShininess14_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension3, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_15), toFloatBuffer(diffuseMaterial_15), toFloatBuffer(specularMaterial_15), lightDirectionBuffer, MShininess15_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension3, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_16), toFloatBuffer(diffuseMaterial_16), toFloatBuffer(specularMaterial_16), lightDirectionBuffer, MShininess16_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension2, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_17), toFloatBuffer(diffuseMaterial_17), toFloatBuffer(specularMaterial_17), lightDirectionBuffer, MShininess17_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension2, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_18), toFloatBuffer(diffuseMaterial_18), toFloatBuffer(specularMaterial_18), lightDirectionBuffer, MShininess18_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension2, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_19), toFloatBuffer(diffuseMaterial_19), toFloatBuffer(specularMaterial_19), lightDirectionBuffer, MShininess19_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension2, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_20), toFloatBuffer(diffuseMaterial_20), toFloatBuffer(specularMaterial_20), lightDirectionBuffer, MShininess20_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension2, yDimension1, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_21), toFloatBuffer(diffuseMaterial_21), toFloatBuffer(specularMaterial_21), lightDirectionBuffer, MShininess21_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, -xDimension1, yDimension1, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_22), toFloatBuffer(diffuseMaterial_22), toFloatBuffer(specularMaterial_22), lightDirectionBuffer, MShininess22_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension1, yDimension1, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_23), toFloatBuffer(diffuseMaterial_23), toFloatBuffer(specularMaterial_23), lightDirectionBuffer, MShininess23_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);

		
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, xDimension2, yDimension1, -10.0f);
        modelMatrix = translateMatrix;
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		BindUniforms(toFloatBuffer(ambientMaterial_24), toFloatBuffer(diffuseMaterial_24), toFloatBuffer(specularMaterial_24), lightDirectionBuffer, MShininess24_SSD);
        GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        GLES32.glBindVertexArray(0);
		
		
		
		
		GLES32.glUseProgram(0);
		
		

		
		
		if (xRotate >= 360.0f)
		xRotate = 0.0f;
	else
		xRotate += 1.41f;

	if (yRotate >= 360.0f)
		yRotate = 0.0f;
	else
		yRotate += 1.41f;

	if (zRotate >= 360.0f)
		zRotate = 0.0f;
	else
		zRotate += 1.41f;

	xRotate_r = (GL_PI / 180.0f) * xRotate;
	yRotate_r = (GL_PI / 180.0f) * yRotate;
	zRotate_r = (GL_PI / 180.0f) * zRotate;
		requestRender();
	}
	
	
	//this function will convert simple float array for each material property to FloatBuffer, since BindUniform only accepts FloatBuffer data
	public FloatBuffer toFloatBuffer(float vertexData[])
	{
		ByteBuffer byteBufferVar = ByteBuffer.allocateDirect(vertexData.length * 4);
		byteBufferVar.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferVar = byteBufferVar.asFloatBuffer();
		floatBufferVar.put(vertexData);
		floatBufferVar.position(0);
		
		return floatBufferVar;
	}
	
	
	//this function binds each circles material and light and position properties to shader uniforms
	public void BindUniforms(FloatBuffer materialAmbient, FloatBuffer materialDiffuse, FloatBuffer materialSpecular, FloatBuffer lightPositionBuffer, float Shininess)
	{
		if(gbLighting == true)
		{
			GLES32.glUniform1i(lKeyPressedUniform, 1);
			GLES32.glUniform4fv(lightDirectionUniform, 1, lightPositionBuffer);
			GLES32.glUniform3fv(ldUniform, 1, lightDiffuseBuffer);
			GLES32.glUniform3fv(laUniform, 1, lightAmbientBuffer);
			GLES32.glUniform3fv(lsUniform, 1, lightSpecularBuffer);
			GLES32.glUniform3fv(kaUniform, 1, materialAmbient);
			GLES32.glUniform3fv(kdUniform, 1, materialDiffuse);
			GLES32.glUniform3fv(ksUniform, 1, materialSpecular);
			GLES32.glUniform1f(shininessUniform, Shininess);
			
		}else
		{
			GLES32.glUniform1i(lKeyPressedUniform, 0);
		}
	}
	
	
	
	
	private void uninitialize()
	{
		//code
		//destroy vao
		
		 if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
			
			GLES32.glDeleteProgram(shaderProgramObject);
		}
		
		
	}
	
}

