package com.astromedicomp.Android15;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //egl = embedded egl (cause opengl-es calles internally to embedded opengl)

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer{
	
	private final Context context;
	private GestureDetector gestureDetector;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] vao_cube = new int[1];
	private int[] vbo_vertex_cube = new int[1];
	private int[] vbo_normal_cube = new int[1];
	
	
	private int modelViewMatrixUniform;
	private int projectionMatrixUniform;
	private int ldUniform;
	private int kdUniform;
	private int lKeyPressedUniform;
	private int lightDirectionUniform;
	
	private static boolean gbLighting = false;
	private static boolean gbAnimate = false;
	private float xRotate = 0.0f;
	private float perspectiveProjectionMatrix[] = new float[16];
	
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		this.setEGLContextClientVersion(3); //opengl ndk is server and opengl sdk is client so we are setting client version to 3	
		this.setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//override method of GLSurfaceView.Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		//String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE);
		System.out.println("SSD: " + glesVersion);
		//System.out.println("SSD: " + glslVersion);
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		if(gbLighting == false)
			gbLighting = true;
		else
			gbLighting = false;
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		gbAnimate = true;
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float veclocityX, float velocityY)
	{
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return true;
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	private void initialize(GL10 gl)
	{
		//vertex shader source
		final String vertexShaderSourceCode = String.format
		(
		 "#version 320 es"+
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform highp int u_lkeyPressed;" +
		"uniform mediump vec3 u_ld;" +
		"uniform mediump vec3 u_kd;" +
		"uniform mediump vec4 u_light_position;" +
		"out vec3 diffuse_light;" +
		"void main(void)" +
		"{" +
		"if(u_lkeyPressed == 1)" +
		"{" +
		"vec4 eye_cordinate = u_model_view_matrix * vPosition;" +
		"mat3 normal_matrix = mat3(transpose(inverse(u_model_view_matrix)));" +
		"vec3 tnorm = normalize(normal_matrix * vNormal);" +
		"vec3 s = normalize(vec3(u_light_position -eye_cordinate));" +
		"diffuse_light = u_ld * u_kd * max(dot(s, tnorm), 0.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" +
		"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile source code
		GLES32.glCompileShader(vertexShaderObject);
		
		//compile staus error chec
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("SSD: vertex shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//fragment shader
		//create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		//fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		 "#version 320 es"+
		"\n" +
		"precision highp float;"+
		"in vec3 diffuse_light;" +
		"uniform highp int u_lkeyPressed;" +
		"out vec4 fragColor;" +
		"vec4 color;" +
		"void main(void)" +
		"{" +
		"if(u_lkeyPressed == 1)" +
		"{" +
		"color = vec4(diffuse_light, 1.0);" +
		"}" +
		"else" +
		"{" +
		"color = vec4(1.0, 1.0, 1.0, 1.0);" +
		"}" +
		"fragColor = color;" +
		"}"
		);
		 
		 //provide source code
		 GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		 
		 //compile fragment shader
		 GLES32.glCompileShader(fragmentShaderObject);
		 
		 //compile status
		 iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("SSD: fragment shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//shader program object
		shaderProgramObject = GLES32.glCreateProgram();
		
		//attach vertex shader
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		
		//attach fragment shader
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		//pre linking post attaching attribute binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
		
		//linking progam
		GLES32.glLinkProgram(shaderProgramObject);
		
		iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("SSD: shader shader link log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//get MVP uniform location
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		lKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lkeyPressed");
		lightDirectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		modelViewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_view_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		
		//triangle vertices
		
		final float[] cube_vertices = new float[]
		{
			-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f
		};
		
		final float[] cube_normals = new float[]
		{
			// Front Face
	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,
	         // Right Face

	        1.0f, 1.0f, -1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, -1.0f, 1.0f,
	        1.0f, -1.0f, -1.0f,


	        // Back Face
	        1.0f, -1.0f, -1.0f,
	       -1.0f, -1.0f, -1.0f,
	       -1.0f, 1.0f, -1.0f,
        	1.0f, 1.0f, -1.0f,

	
	        // Left Face
	       -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
	       -1.0f, 0.0f, 0.0f,
	       -1.0f, 0.0f, 0.0f,
		   
	       // Top Face
	        1.0f, 1.0f, -1.0f,
	       -1.0f, 1.0f, -1.0f,
	       -1.0f, 1.0f, 0.0f,
	        1.0f, 1.0f, 0.0f,

	       // Bottom Face
	       1.0f, -1.0f, 1.0f,
	      -1.0f, -1.0f, 1.0f,
	      -1.0f, -1.0f, -1.0f,
	       1.0f, -1.0f, -1.0f
		};
		
		
		
		
		//vao for cube
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		//vbo cube vertices
		GLES32.glGenBuffers(1, vbo_vertex_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_vertex_cube[0]);
		
		ByteBuffer  byteBufferRectVertex = ByteBuffer.allocateDirect(cube_vertices.length * 4);
		byteBufferRectVertex.order(ByteOrder.nativeOrder());
		
		FloatBuffer vertexRectBuffer = byteBufferRectVertex.asFloatBuffer();
		vertexRectBuffer.put(cube_vertices);
		vertexRectBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube_vertices.length * 4, vertexRectBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		//vbo for cube color
		GLES32.glGenBuffers(1, vbo_normal_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal_cube[0]);
		
		ByteBuffer byteBufferNormal = ByteBuffer.allocateDirect(cube_normals.length * 4);
		byteBufferNormal.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBufferNormal.asFloatBuffer();
		normalBuffer.put(cube_normals);
		normalBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube_normals.length * 4, normalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		//set background color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float) height, 1.0f, 100.0f);
	}
	
	public void display()
	{
		//variable declaration
		float lightDirection[] = new float[] {0.0f, 0.0f, 2.0f, 1.0f};
		
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject); 
		
		ByteBuffer byteBufferDirection = ByteBuffer.allocateDirect(lightDirection.length * 4);
		byteBufferDirection.order(ByteOrder.nativeOrder());
		FloatBuffer lightDirectionBuffer = byteBufferDirection.asFloatBuffer();
		lightDirectionBuffer.put(lightDirection);
		lightDirectionBuffer.position(0);
		
		if(gbLighting == true)
		{
			GLES32.glUniform1i(lKeyPressedUniform, 1);
			GLES32.glUniform4fv(lightDirectionUniform, 1, lightDirectionBuffer);
			GLES32.glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
			GLES32.glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);
			
		}else
		{
			GLES32.glUniform1i(lKeyPressedUniform, 0);
		}
		
		float modelViewMatrix[] = new float[16];
		float translateMatrix[] = new float[16];
		float rotateMatrix[] = new float[16];
		
		//set identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -4.0f);
		Matrix.setRotateM(rotateMatrix, 0, xRotate, 1.0f, 0.0f, 0.0f);
		
		//matrix multiply
		Matrix.multiplyMM(modelViewMatrix, 0, translateMatrix, 0, rotateMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		if(gbAnimate == true)
		{
			if(xRotate < 360.0f)
			xRotate += 0.2f;
		else
			xRotate = 0.0f;
		}
		
		requestRender();
	}
	
	private void uninitialize()
	{
		//code
		//destroy vao
		
		if(vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
		
		//destroy vbo
		
		if(vbo_vertex_cube[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_vertex_cube, 0);
			vbo_vertex_cube[0] = 0;
		}
		
		if(vbo_normal_cube[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_normal_cube, 0);
			vbo_normal_cube[0] = 0;
		}
		
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
			
			GLES32.glDeleteProgram(shaderProgramObject);
		}
		
		
	}
	
}

