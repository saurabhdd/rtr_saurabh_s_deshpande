package com.astromedicomp.Hello;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;

import android.graphics.Color;

import android.view.Gravity;

public class MainActivity extends AppCompatActivity {

		AppCompatTextView myTexView = new AppCompatTextView(this);
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
        myTexView.setText("Hello World!!!");
	    myTexView.setTextSize(32);
	    myTexView.setTextColor(Color.rgb(0, 255, 0));
	   // myTexView.setTextGravity(Gravity.CENTER);
	    myTexView.setBackgroundColor(Color.rgb(0, 0, 0));
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));
       setContentView(myTexView);
    }
	
}