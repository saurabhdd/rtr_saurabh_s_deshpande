package com.astromedicomp.Android16;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.Matrix;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig; //egl = embedded egl (cause opengl-es calles internally to embedded opengl)

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer{
	
	private final Context context;
	private GestureDetector gestureDetector;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] vao_pyramid = new int[1];
	private int[] vbo_vertex_pyramid = new int[1];
	private int[] vbo_normal_pyramid = new int[1];
	
	
	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;
	private int laUniform;
	private int ldUniform;
	private int lsUniform;
	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int shininessUniform;
	private int lKeyPressedUniform;
	private int lightDirectionUniform;
	
	private float shininess = 128.0f;
	
	float materialAmbient[] = new float[] { 0.0f, 0.0f, 0.0f };
    float materialDiffuse[] = new float[] { 1.0f, 1.0f, 1.0f };
    float materialSpecular[] = new float[] { 1.0f, 1.0f, 1.0f };
	
	private static boolean gbLighting = false;
	private static boolean gbAnimate = false;
	private float yRotate = 0.0f;
	private float perspectiveProjectionMatrix[] = new float[16];
	
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		this.setEGLContextClientVersion(3); //opengl ndk is server and opengl sdk is client so we are setting client version to 3	
		this.setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	//override method of GLSurfaceView.Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL-ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		//String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE);
		System.out.println("SSD: " + glesVersion);
		//System.out.println("SSD: " + glslVersion);
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		if(gbLighting == false)
			gbLighting = true;
		else
			gbLighting = false;
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if(gbAnimate == true)
			gbAnimate = false;
		else
			gbAnimate = true;
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public boolean onFling(MotionEvent e1, MotionEvent e2, float veclocityX, float velocityY)
	{
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return true;
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	private void initialize(GL10 gl)
	{
		//vertex shader source
		final String vertexShaderSourceCode = String.format
		(
		 "#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec3 u_la[2];" +
		"uniform mediump vec3 u_ld[2];" +
		"uniform mediump vec3 u_ls[2];" +
		"uniform mediump vec3 u_kd;" +
		"uniform mediump vec3 u_ka;" +
		"uniform mediump vec3 u_ks;" +
		"uniform mediump vec4 u_light_position[2];" +
		"uniform mediump float u_shininess;" +
		"out vec3 fong_ads_light;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"vec3 light_direction[2];" +
		"vec3 reflection_vector[2];" +
		"vec3 ambient_light[2];" +
		"vec3 diffuse_light[2];" +
		"vec3 specular_light[2];" +
		"if(l_key_pressed == 1)" +
		"{" +
		"fong_ads_light = vec3(0.0, 0.0, 0.0);" +
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" +
		"for(i = 0; i < 2; i++)" +
		"{" +
		"light_direction[i] = normalize(vec3(u_light_position[i] - eye_cordinate));" +
		"reflection_vector[i] = reflect(-light_direction[i], trasformed_normal);" +
		"ambient_light[i] = u_la[i] * u_ka;" +
		"diffuse_light[i] = u_ld[i] * u_kd * max(dot(light_direction[i], trasformed_normal), 0.0);" +
		"specular_light[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i], view_vector), 0.0), u_shininess);" +
		"fong_ads_light =  fong_ads_light + ambient_light[i] + diffuse_light[i] + specular_light[i];" +
		"}" +
		"}" +
		"else" +
		"{" +
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix *  u_model_matrix * vPosition;" +
		"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		//compile source code
		GLES32.glCompileShader(vertexShaderObject);
		
		//compile staus error chec
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("SSD: vertex shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//fragment shader
		//create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		//fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		 "#version 320 es" +
		"\n" +
		"precision highp float;"+
		"in vec3 fong_ads_light;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"fragColor = vec4(fong_ads_light, 1.0);" +
		"}"
		);
		 
		 //provide source code
		 GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		 
		 //compile fragment shader
		 GLES32.glCompileShader(fragmentShaderObject);
		 
		 //compile status
		 iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("SSD: fragment shader compilation log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//shader program object
		shaderProgramObject = GLES32.glCreateProgram();
		
		//attach vertex shader
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		
		//attach fragment shader
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		//pre linking post attaching attribute binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
		
		//linking progam
		GLES32.glLinkProgram(shaderProgramObject);
		
		iShaderCompileStatus[0] = 0;
		 iInfoLogLength[0] = 0;
		 szInfoLog = null;
		 
		 GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("SSD: shader shader link log = " + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		
		//get MVP uniform location
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		lightDirectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		shininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_shininess");
		lKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "l_key_pressed");
		
		
		//triangle vertices
		
		final float[] pyramid_vertices = new float[]
		{
			// front
	0.0f, 1.0f, 0.0f,
	   -1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,


	// right
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,


	// back
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, -1.0f,
	   -1.0f, -1.0f, -1.0f,


	   // left
	   0.0f, 1.0f, 0.0f,
		  -1.0f, -1.0f, -1.0f,
		  -1.0f, -1.0f, 1.0f
		};
		
		final float[] pyramid_normals = new float[]
		{
			0.0f, 0.447214f, 0.894427f, //top
	0.0f, 0.447214f, 0.894427f, //left
	0.0f, 0.447214f, 0.894427f, //right

	//Right Face	
	0.894427f, 0.447214f, 0.0f, //top	
	0.894427f, 0.447214f, 0.0f, //left
	0.894427f, 0.447214f, 0.0f, //right

	//Back Face
	0.0f, 0.447214f, -0.894427f, //top
	0.0f, 0.447214f, -0.894427f, //left
	0.0f, 0.447214f, -0.894427f, //right

	//Left Face
	-0.894427f, 0.447214f, 0.0f, //top
	-0.894427f, 0.447214f, 0.0f, //left
	-0.894427f, 0.447214f, 0.0f, //right
		};
		
		
		
		
		//vao for cube
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		//vbo cube vertices
		GLES32.glGenBuffers(1, vbo_vertex_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_vertex_pyramid[0]);
		
		ByteBuffer  byteBufferRectVertex = ByteBuffer.allocateDirect(pyramid_vertices.length * 4);
		byteBufferRectVertex.order(ByteOrder.nativeOrder());
		
		FloatBuffer vertexRectBuffer = byteBufferRectVertex.asFloatBuffer();
		vertexRectBuffer.put(pyramid_vertices);
		vertexRectBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramid_vertices.length * 4, vertexRectBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_VERTEX);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		//vbo for cube color
		GLES32.glGenBuffers(1, vbo_normal_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal_pyramid[0]);
		
		ByteBuffer byteBufferNormal = ByteBuffer.allocateDirect(pyramid_normals.length * 4);
		byteBufferNormal.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBufferNormal.asFloatBuffer();
		normalBuffer.put(pyramid_normals);
		normalBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramid_normals.length * 4, normalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.SSD_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.SSD_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		//set background color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float) height, 1.0f, 100.0f);
	}
	
	public void display()
	{
		//variable declaration
		float lightDirection[] = new float[8];
		float ambientLight[] = new float[6];
		float diffuseLight[] = new float[6];
		float specularLight[] = new float[6];
		
		
	ambientLight[0] = 0.0f;
	ambientLight[1] = 0.0f;
	ambientLight[2] = 0.0f;
	diffuseLight[0] = 1.0f;
	diffuseLight[1] = 0.0f;
	diffuseLight[2] = 0.0f;
	specularLight[0] = 1.0f;
	specularLight[1] = 0.0f;
	specularLight[2] = 0.0f;
	
	ambientLight[3] = 0.0f;
	ambientLight[4] = 0.0f;
	ambientLight[5] = 0.0f;
	diffuseLight[3] = 0.0f;
	diffuseLight[4] = 0.0f;
	diffuseLight[5] = 1.0f;
	specularLight[3] = 0.0f;
	specularLight[4] = 0.0f;
	specularLight[5] = 1.0f;
	
	ambientLight[6] = 0.0f;
	ambientLight[7] = 0.0f;
	ambientLight[8] = 0.0f;
	diffuseLight[6] = 0.0f;
	diffuseLight[7] = 1.0f;
	diffuseLight[8] = 0.0f;
	specularLight[6] = 0.0f;
	specularLight[7] = 1.0f;
	specularLight[8] = 0.0f;
		
		lightDirection[0] = -2.0f;
		lightDirection[1] = 0.0f;
		lightDirection[2] = 0.0f;
		lightDirection[3] = 1.0f;
		lightDirection[4] = 2.0f;
		lightDirection[5] = 0.0f;
		lightDirection[6] = 0.0f;
		lightDirection[7] = 1.0f;
		
		
		
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject); 
		
		ByteBuffer byteBufferDirection = ByteBuffer.allocateDirect(lightDirection.length * 4);
		byteBufferDirection.order(ByteOrder.nativeOrder());
		FloatBuffer lightDirectionBuffer = byteBufferDirection.asFloatBuffer();
		lightDirectionBuffer.put(lightDirection);
		lightDirectionBuffer.position(0);
		
		ByteBuffer byteBufferAmbient = ByteBuffer.allocateDirect(ambientLight.length * 4);
		byteBufferAmbient.order(ByteOrder.nativeOrder());
		FloatBuffer lightAmbientBuffer = byteBufferAmbient.asFloatBuffer();
		lightAmbientBuffer.put(ambientLight);
		lightAmbientBuffer.position(0);
		
		ByteBuffer byteBufferDiffuse = ByteBuffer.allocateDirect(diffuseLight.length * 4);
		byteBufferDiffuse.order(ByteOrder.nativeOrder());
		FloatBuffer lightDiffuseBuffer = byteBufferDiffuse.asFloatBuffer();
		lightDiffuseBuffer.put(diffuseLight);
		lightDiffuseBuffer.position(0);
		
		ByteBuffer byteBufferSpecular = ByteBuffer.allocateDirect(specularLight.length * 4);
		byteBufferSpecular.order(ByteOrder.nativeOrder());
		FloatBuffer lightSpecularBuffer = byteBufferSpecular.asFloatBuffer();
		lightSpecularBuffer.put(specularLight);
		lightSpecularBuffer.position(0);
		
		//material
		ByteBuffer byteMaterialAmbient = ByteBuffer.allocateDirect(materialAmbient.length * 4);
		byteMaterialAmbient.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialABuffer = byteMaterialAmbient.asFloatBuffer();
		lightMaterialABuffer.put(materialAmbient);
		lightMaterialABuffer.position(0);
		
		ByteBuffer byteMaterialDiffuse = ByteBuffer.allocateDirect(materialDiffuse.length * 4);
		byteMaterialDiffuse.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialDBuffer = byteMaterialDiffuse.asFloatBuffer();
		lightMaterialDBuffer.put(materialDiffuse);
		lightMaterialDBuffer.position(0);
		
		ByteBuffer byteMaterialSpecular = ByteBuffer.allocateDirect(materialSpecular.length * 4);
		byteMaterialSpecular.order(ByteOrder.nativeOrder());
		FloatBuffer lightMaterialSBuffer = byteMaterialSpecular.asFloatBuffer();
		lightMaterialSBuffer.put(materialSpecular);
		lightMaterialSBuffer.position(0);
		
		
		if(gbLighting == true)
		{
			GLES32.glUniform1i(lKeyPressedUniform, 1);
			GLES32.glUniform4fv(lightDirectionUniform, 2, lightDirectionBuffer);
			GLES32.glUniform3fv(ldUniform, 2, lightDiffuseBuffer);
			GLES32.glUniform3fv(laUniform, 2, lightAmbientBuffer);
			GLES32.glUniform3fv(lsUniform, 2, lightSpecularBuffer);
			GLES32.glUniform3fv(kaUniform, 1, lightMaterialABuffer);
			GLES32.glUniform3fv(kdUniform, 1, lightMaterialDBuffer);
			GLES32.glUniform3fv(ksUniform, 1, lightMaterialSBuffer);
			GLES32.glUniform1f(shininessUniform, shininess);
			
		}else
		{
			GLES32.glUniform1i(lKeyPressedUniform, 0);
		}
		
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float translateMatrix[] = new float[16];
		float rotateMatrix[] = new float[16];
		
		//set identity
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -4.0f);
		Matrix.setRotateM(rotateMatrix, 0, yRotate, 0.0f, 1.0f, 0.0f);
		
		//matrix multiply
		Matrix.multiplyMM(modelMatrix, 0, translateMatrix, 0, rotateMatrix, 0);
		
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		if(gbAnimate == true)
		{
			if(yRotate < 360.0f)
			yRotate += 0.5f;
		else
			yRotate = 0.0f;
		}
		
		requestRender();
	}
	
	private void uninitialize()
	{
		//code
		//destroy vao
		
		if(vao_pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}
		
		//destroy vbo
		
		if(vbo_vertex_pyramid[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_vertex_pyramid, 0);
			vbo_vertex_pyramid[0] = 0;
		}
		
		if(vbo_normal_pyramid[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_normal_pyramid, 0);
			vbo_normal_pyramid[0] = 0;
		}
		
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
			
			GLES32.glDeleteProgram(shaderProgramObject);
		}
		
		
	}
	
}

