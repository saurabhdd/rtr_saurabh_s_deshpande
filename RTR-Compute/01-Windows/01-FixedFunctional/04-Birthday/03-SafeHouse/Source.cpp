
#include<windows.h>
#include<stdio.h>
#include<math.h>
#include<gl/gl.h>
#include<gl/GLU.h>
#include"Math3d.h"
#include"Header.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "GLU32.lib")

#define GL_PI 3.14159
#define WINWIDTH 800
#define WINHEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gbFile = NULL;
HWND ghwnd;
bool gbActiveWindow = false;
bool gbFullScreen = false;

HGLRC hglrc;
HDC ghdc;
DWORD dwStyle;
WINDOWPLACEMENT wPrev;
int WINDOWMAXWIDTH, WINDOWMAXHEIGHT, X, Y;
GLfloat xAngle = 0.0f;
GLfloat COORDINATE[3];
static GLfloat lightXPos = 0.0f;
static GLfloat lightZPos = 0.0f;
static GLfloat lightYPos = 0.0f;
GLfloat yRot = 0.0f;
GLuint texture_rock;
GLuint texture_door;
GLuint texture_brick;
GLuint texture_grass;
GLuint texture_sky;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd;
	bool bDone = false;
	TCHAR szAppName[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass;
	MSG msg;

	//Creation of the log file
	if (fopen_s(&gbFile, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev = { sizeof(WINDOWPLACEMENT) };

	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	WINDOWMAXWIDTH = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT = GetSystemMetrics(SM_CYMAXIMIZED);

	X = (WINDOWMAXWIDTH / 2) - (WINWIDTH / 2);
	Y = (WINDOWMAXHEIGHT / 2) - (WINHEIGHT / 2);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("practice"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X, Y,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	//void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 65:
			lightXPos -= 0.050f;
			break;

		case 66:
			lightXPos += 0.050f;
			break;

		case 67:
			lightYPos -= 0.050f;
			break;

		case 68:
			lightYPos += 0.050f;
			break;

		case VK_LEFT:
			yRot -= 0.10f;
			break;

		case VK_RIGHT:
			yRot += 0.100f;
			break;

		case VK_UP:
			lightZPos -= 0.010f;
			break;

		case VK_DOWN:
			lightZPos += 0.010f;
			break;


		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

}



void Initialize()
{
	//function declaration
	void Resize(int, int);
	bool LoadTexture(GLuint *, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf_s(gbFile, "choosepixel()  failed\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf_s(gbFile, "SetPixel()  failed\n");
		DestroyWindow(ghwnd);
	}

	hglrc = wglCreateContext(ghdc);
	if (hglrc == NULL)
	{
		fprintf_s(gbFile, "wglCreateContext()  failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, hglrc) == FALSE)
	{
		fprintf_s(gbFile, "wglMakeCurrent()  failed\n");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	//glShadeModel(GL_FLAT);
	//glFrontFace(GL_CW);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);
	LoadTexture(&texture_rock, MAKEINTRESOURCE(MY_WOOD));
	LoadTexture(&texture_door, MAKEINTRESOURCE(MY_DOOR));
	LoadTexture(&texture_brick, MAKEINTRESOURCE(MY_BRICK));
	LoadTexture(&texture_grass, MAKEINTRESOURCE(GRASS));
	LoadTexture(&texture_sky, MAKEINTRESOURCE(SKY));


	Resize(WINWIDTH, WINHEIGHT);

}

void Resize(int width, int height)
{
	if (height <= 0) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{

	//variable declaration
	GLfloat lightPos[] = { lightXPos, lightYPos, lightZPos, 1.0f };
	GLfloat specLight[] = { 0.40f, 0.40f, 0.40f, 0.4f };
	GLfloat diffuseLight[] = { 0.5f, 0.5f, 0.5f, 0.5f };
	GLfloat specRef[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat ambientLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat baseWidth = 0.9f;
	M3DVector3f vNormal;
	M3DVector3f vPoints[3];
	M3DVector3f shadowPoints[3] = { {-2.0f * 0.7f, -1.30f * 0.7f, -2.0f * 0.7f},
								  {-2.0f * 0.7f, -1.30f * 0.7f, 2.0f * 0.7f},
								  {2.0f * 0.7f, -1.30f * 0.7f, 2.0f * 0.7f} };

	M3DVector3f shadowPointsHome[3] = { {-2.0f * 1.2f, -1.00f * 1.2f, -2.0f * 1.2f},
								  {-2.0f * 1.2f, -1.0f * 1.2f, 2.0f * 1.2f},
								  {2.0f * 1.2f, -1.0f * 1.2f, 2.0f * 1.2f} };
	M3DVector4f vPlaneEquation;
	M3DMatrix44f vShadowMatrix;
	M3DVector4f vPlaneEquationHome;
	M3DMatrix44f vShadowMatrixHome;

	//function declaration
	void DrawGround(void);
	void DrawTower(bool);
	void DrawHome(bool);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHT0);
	glFrontFace(GL_CCW);


	//glLightModelfv(GL_AMBIENT, ambientLight);

	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);

	//glMaterialfv(GL_FRONT, GL_SPECULAR, specRef);
	//glMateriali(GL_FRONT, GL_SHININESS, 128);

	//shadow calculations
	m3dGetPlaneEquation(vPlaneEquation, shadowPoints[0], shadowPoints[1], shadowPoints[2]);

	m3dMakePlanarShadowMatrix(vShadowMatrix, vPlaneEquation, lightPos);

	m3dGetPlaneEquation(vPlaneEquationHome, shadowPointsHome[0], shadowPointsHome[1], shadowPointsHome[2]);

	m3dMakePlanarShadowMatrix(vShadowMatrixHome, vPlaneEquationHome, lightPos);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	//glBindTexture(GL_TEXTURE_2D, texture_rock);
	DrawGround();

	//creating shadow
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glPushMatrix();

	glMultMatrixf((GLfloat *)vShadowMatrix);

	DrawTower(true);

	glPopMatrix();

	//creating shadow for home
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glPushMatrix();

	glMultMatrixf((GLfloat *)vShadowMatrixHome);

	DrawHome(true);

	glPopMatrix();

	//creating the model for tower
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	glPushMatrix();

	DrawTower(false);

	glPopMatrix();

	//creating the model for home
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	glPushMatrix();

	glTranslatef(0.0f, -0.3f, 0.0f);
	DrawHome(false);

	glPopMatrix();


	SwapBuffers(ghdc);

}

void m3dGetPlaneEquation(M3DVector4f planeEq, const M3DVector3f p1, const M3DVector3f p2, const M3DVector3f p3)
{
	// Get two vectors... do the cross product
	M3DVector3f v1, v2;

	// V1 = p3 - p1
	v1[0] = p3[0] - p1[0];
	v1[1] = p3[1] - p1[1];
	v1[2] = p3[2] - p1[2];

	// V2 = P2 - p1
	v2[0] = p2[0] - p1[0];
	v2[1] = p2[1] - p1[1];
	v2[2] = p2[2] - p1[2];

	// Unit normal to plane - Not sure which is the best way here
	m3dCrossProduct(planeEq, v1, v2);
	m3dNormalizeVector(planeEq);
	// Back substitute to get D
	planeEq[3] = -(planeEq[0] * p3[0] + planeEq[1] * p3[1] + planeEq[2] * p3[2]);
}

void m3dMakePlanarShadowMatrix(M3DMatrix44f proj, const M3DVector4f planeEq, const M3DVector3f vLightPos)
{
	// These just make the code below easier to read. They will be 
	// removed by the optimizer.	
	float a = planeEq[0];
	float b = planeEq[1];
	float c = planeEq[2];
	float d = planeEq[3];

	float dx = -vLightPos[0];
	float dy = -vLightPos[1];
	float dz = -vLightPos[2];

	// Now build the projection matrix
	proj[0] = b * dy + c * dz;
	proj[1] = -a * dy;
	proj[2] = -a * dz;
	proj[3] = 0.0;

	proj[4] = -b * dx;
	proj[5] = a * dx + c * dz;
	proj[6] = -b * dz;
	proj[7] = 0.0;

	proj[8] = -c * dx;
	proj[9] = -c * dy;
	proj[10] = a * dx + b * dy;
	proj[11] = 0.0;

	proj[12] = -d * dx;
	proj[13] = -d * dy;
	proj[14] = -d * dz;
	proj[15] = a * dx + b * dy + c * dz;
	// Shadow matrix ready
}

void DrawGround(void)
{
	//glTranslatef(0.0f, 0.0f, -3.0f);
	glScalef(0.7f, 0.7f, 0.7f);
	glColor3f(1.0f, 1.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, texture_grass);

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-10.5f, -1.3f, -10.5f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-10.5f, -1.3f, 10.5f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(10.5f, -1.3f, 10.5f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(10.5f, -1.3f, -10.5f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, texture_sky);

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-12.50f, -1.3f, -12.5f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(12.50f, -1.3f, -12.5f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(12.5f, 7.0f, -12.5f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-12.5f, 7.0f, -12.5f);
	glEnd();

}

void DrawTower(bool drawShadow)
{
	//function declaration
	void SquareRod(GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, bool);

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();

	//glTranslatef(0.0f, 0.0f, -3.0f);
	glTranslatef(-1.0f, 0.0f, -1.0f);
	glScalef(0.7f, 0.7f, 0.7f);

	if (!drawShadow)
		glBindTexture(GL_TEXTURE_2D, texture_rock);
	else
		glColor3f(0.0f, 0.0f, 0.0f);

	glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	glBegin(GL_QUADS);
	glVertex3f(-0.7f, 0.50f, 0.6f);
	glVertex3f(-0.7f, 0.50f, -0.6f);
	glVertex3f(0.7f, 0.5f, -0.6f);
	glVertex3f(0.7f, 0.50f, 0.6f);

	glVertex3f(-0.7f, 0.5f, 0.6f);
	glVertex3f(-0.7f, 0.45f, 0.6f);
	glVertex3f(0.7f, 0.45f, 0.6f);
	glVertex3f(0.7f, 0.5f, 0.6f);


	glVertex3f(0.7f, 0.5f, 0.6f);
	glVertex3f(0.7f, 0.45f, 0.6f);
	glVertex3f(0.7f, 0.45f, -0.6f);
	glVertex3f(0.7f, 0.5f, -0.6f);


	glVertex3f(0.7f, 0.5f, -0.6f);
	glVertex3f(0.7f, 0.45f, -0.6f);
	glVertex3f(-0.7f, 0.45f, -0.6f);
	glVertex3f(-0.7f, 0.5f, -0.6f);

	glVertex3f(-0.7f, 0.5f, -0.6f);
	glVertex3f(-0.7f, 0.45f, -0.6f);
	glVertex3f(-0.7f, 0.45f, 0.6f);
	glVertex3f(-0.7f, 0.5f, 0.6f);

	glEnd();

	GLfloat leftLTop[3] = { -0.7f, 0.54f, 0.58f };
	GLfloat leftLBottom[3] = { -0.7f, 0.5f, 0.58f };
	GLfloat leftRBottom[3] = { -0.7f, 0.5f, 0.6f };
	GLfloat leftRTop[3] = { -0.7f, 0.54f, 0.6f };
	GLfloat rightLTop[3] = { 0.7f, 0.54f, 0.6f };
	GLfloat rightLBottom[3] = { 0.7f, 0.5f, 0.6f };
	GLfloat rightRBottom[3] = { 0.7f, 0.5f, 0.58f };
	GLfloat rightRTop[3] = { 0.7f, 0.54f, 0.58f };

	//railings 
	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[1] = 0.62f;
	leftLBottom[1] = 0.58f;
	leftRBottom[1] = 0.58f;
	leftRTop[1] = 0.62f;
	rightLTop[1] = 0.62f;
	rightLBottom[1] = 0.58f;
	rightRBottom[1] = 0.58f;
	rightRTop[1] = 0.62f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[1] = 0.7f;
	leftLBottom[1] = 0.66f;
	leftRBottom[1] = 0.66f;
	leftRTop[1] = 0.7f;
	rightLTop[1] = 0.7f;
	rightLBottom[1] = 0.66f;
	rightRBottom[1] = 0.66f;
	rightRTop[1] = 0.7f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	GLfloat leftLTopR[3] = { 0.7f, 0.54f, 0.58f };
	GLfloat leftLBottomR[3] = { 0.7f, 0.5f, 0.58f };
	GLfloat leftRBottomR[3] = { 0.7f, 0.5f, 0.6f };
	GLfloat leftRTopR[3] = { 0.7f, 0.54f, 0.6f };
	GLfloat rightLTopR[3] = { 0.7f, 0.54f, -0.6f };
	GLfloat rightLBottomR[3] = { 0.7f, 0.5f, -0.6f };
	GLfloat rightRBottomR[3] = { 0.7f, 0.5f, -0.58f };
	GLfloat rightRTopR[3] = { 0.7f, 0.54f, -0.58f };

	SquareRod(leftLTopR, leftLBottomR, leftRBottomR, leftRTopR, rightLTopR, rightLBottomR, rightRBottomR, rightRTopR, drawShadow);

	leftLTopR[1] = 0.62f;
	leftLBottomR[1] = 0.58f;
	leftRBottomR[1] = 0.58f;
	leftRTopR[1] = 0.62f;
	rightLTopR[1] = 0.62f;
	rightLBottomR[1] = 0.58f;
	rightRBottomR[1] = 0.58f;
	rightRTopR[1] = 0.62f;

	SquareRod(leftLTopR, leftLBottomR, leftRBottomR, leftRTopR, rightLTopR, rightLBottomR, rightRBottomR, rightRTopR, drawShadow);

	leftLTopR[1] = 0.7f;
	leftLBottomR[1] = 0.66f;
	leftRBottomR[1] = 0.66f;
	leftRTopR[1] = 0.7f;
	rightLTopR[1] = 0.7f;
	rightLBottomR[1] = 0.66f;
	rightRBottomR[1] = 0.66f;
	rightRTopR[1] = 0.7f;

	SquareRod(leftLTopR, leftLBottomR, leftRBottomR, leftRTopR, rightLTopR, rightLBottomR, rightRBottomR, rightRTopR, drawShadow);

	GLfloat leftLTopB[3] = { -0.7f, 0.54f, -0.58f };
	GLfloat leftLBottomB[3] = { -0.7f, 0.5f, -0.58f };
	GLfloat leftRBottomB[3] = { -0.7f, 0.5f, -0.6f };
	GLfloat leftRTopB[3] = { -0.7f, 0.54f, -0.6f };
	GLfloat rightLTopB[3] = { -0.7f, 0.54f, 0.6f };
	GLfloat rightLBottomB[3] = { -0.7f, 0.5f, 0.6f };
	GLfloat rightRBottomB[3] = { -0.7f, 0.5f, 0.58f };
	GLfloat rightRTopB[3] = { -0.7f, 0.54f, 0.58f };


	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[1] = 0.62f;
	leftLBottomB[1] = 0.58f;
	leftRBottomB[1] = 0.58f;
	leftRTopB[1] = 0.62f;
	rightLTopB[1] = 0.62f;
	rightLBottomB[1] = 0.58f;
	rightRBottomB[1] = 0.58f;
	rightRTopB[1] = 0.62f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[1] = 0.7f;
	leftLBottomB[1] = 0.66f;
	leftRBottomB[1] = 0.66f;
	leftRTopB[1] = 0.7f;
	rightLTopB[1] = 0.7f;
	rightLBottomB[1] = 0.66f;
	rightRBottomB[1] = 0.66f;
	rightRTopB[1] = 0.7f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[0] = -0.68f;
	leftLTopB[1] = 0.7f;
	leftLTopB[2] = -0.58f;
	leftLBottomB[0] = -0.68f;
	leftLBottomB[1] = 0.7f;
	leftLBottomB[2] = -0.6f;
	leftRBottomB[0] = -0.7f;
	leftRBottomB[1] = 0.7f;
	leftRBottomB[2] = -0.6f;
	leftRTopB[0] = -0.7f;
	leftRTopB[1] = 0.7f;
	leftRTopB[2] = -0.58f;

	rightLTopB[0] = -0.7f;
	rightLTopB[1] = 0.5f;
	rightLTopB[2] = -0.58f;
	rightLBottomB[0] = -0.7f;
	rightLBottomB[1] = 0.5f;
	rightLBottomB[2] = -0.6f;
	rightRBottomB[0] = -0.68f;
	rightRBottomB[1] = 0.5f;
	rightRBottomB[2] = -0.6f;
	rightRTopB[0] = -0.68f;
	rightRTopB[1] = 0.5f;
	rightRTopB[2] = -0.6f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);


	leftLTopB[0] = 0.7f;
	leftLTopB[1] = 0.7f;
	leftLTopB[2] = -0.58f;
	leftLBottomB[0] = 0.7f;
	leftLBottomB[1] = 0.7f;
	leftLBottomB[2] = -0.6f;
	leftRBottomB[0] = 0.68f;
	leftRBottomB[1] = 0.7f;
	leftRBottomB[2] = -0.6f;
	leftRTopB[0] = 0.68f;
	leftRTopB[1] = 0.7f;
	leftRTopB[2] = -0.58f;

	rightLTopB[0] = 0.68f;
	rightLTopB[1] = 0.5f;
	rightLTopB[2] = -0.58f;
	rightLBottomB[0] = 0.68f;
	rightLBottomB[1] = 0.5f;
	rightLBottomB[2] = -0.6f;
	rightRBottomB[0] = 0.7f;
	rightRBottomB[1] = 0.5f;
	rightRBottomB[2] = -0.6f;
	rightRTopB[0] = 0.7f;
	rightRTopB[1] = 0.5f;
	rightRTopB[2] = -0.6f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//Central rest room for watch tower

	leftLTopB[0] = 0.5f;
	leftLTopB[1] = 0.95f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.5f;
	leftLBottomB[1] = 0.95f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.5f;
	leftRBottomB[1] = 0.95f;
	leftRBottomB[2] = -0.45f;
	leftRTopB[0] = -0.5f;
	leftRTopB[1] = 0.95f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.5f;
	rightLTopB[1] = 0.5f;
	rightLTopB[2] = 0.45f;
	rightLBottomB[0] = -0.5f;
	rightLBottomB[1] = 0.5f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.5f;
	rightRBottomB[1] = 0.5f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.5f;
	rightRTopB[1] = 0.5f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//roof
	leftLTopB[0] = 0.7f;
	leftLTopB[1] = 0.980f;
	leftLTopB[2] = 0.6f;
	leftLBottomB[0] = 0.7f;
	leftLBottomB[1] = 0.980f;
	leftLBottomB[2] = -0.6f;
	leftRBottomB[0] = -0.7f;
	leftRBottomB[1] = 0.980f;
	leftRBottomB[2] = -0.6f;
	leftRTopB[0] = -0.7f;
	leftRTopB[1] = 0.980f;
	leftRTopB[2] = 0.6f;

	rightLTopB[0] = -0.7f;
	rightLTopB[1] = 0.95f;
	rightLTopB[2] = 0.6f;
	rightLBottomB[0] = -0.7f;
	rightLBottomB[1] = 0.95f;
	rightLBottomB[2] = -0.6f;
	rightRBottomB[0] = 0.7f;
	rightRBottomB[1] = 0.95f;
	rightRBottomB[2] = -0.6f;
	rightRTopB[0] = 0.7f;
	rightRTopB[1] = 0.95f;
	rightRTopB[2] = 0.6f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	glBegin(GL_QUADS);

	glVertex3f(0.7f, 0.980f, 0.6f);
	glVertex3f(0.7f, 0.980f, -0.6f);
	glVertex3f(-0.7f, 0.980f, -0.6f);
	glVertex3f(-0.7f, 0.980f, 0.6f);

	glVertex3f(0.7f, 0.950f, 0.6f);
	glVertex3f(0.7f, 0.950f, -0.6f);
	glVertex3f(-0.7f, 0.950f, -0.6f);
	glVertex3f(-0.7f, 0.950f, 0.6f);

	glEnd();

	//Lower support
	leftLTopB[0] = 0.5f;
	leftLTopB[1] = 0.5f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.5f;
	leftLBottomB[1] = 0.5f;
	leftLBottomB[2] = 0.41f;
	leftRBottomB[0] = 0.45f;
	leftRBottomB[1] = 0.5f;
	leftRBottomB[2] = 0.41f;
	leftRTopB[0] = 0.45f;
	leftRTopB[1] = 0.5f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.65f;
	rightLTopB[1] = -1.30f;
	rightLTopB[2] = 0.45f;
	rightLBottomB[0] = 0.65f;
	rightLBottomB[1] = -1.30f;
	rightLBottomB[2] = 0.41f;
	rightRBottomB[0] = 0.7f;
	rightRBottomB[1] = -1.30f;
	rightRBottomB[2] = 0.41f;
	rightRTopB[0] = 0.7f;
	rightRTopB[1] = -1.30f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[0] = 0.5f;
	leftLTopB[1] = 0.5f;
	leftLTopB[2] = -0.41f;
	leftLBottomB[0] = 0.5f;
	leftLBottomB[1] = 0.5f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = 0.45f;
	leftRBottomB[1] = 0.5f;
	leftRBottomB[2] = -0.45f;
	leftRTopB[0] = 0.45f;
	leftRTopB[1] = 0.5f;
	leftRTopB[2] = -0.41f;

	rightLTopB[0] = 0.65f;
	rightLTopB[1] = -1.30f;
	rightLTopB[2] = -0.41f;
	rightLBottomB[0] = 0.65f;
	rightLBottomB[1] = -1.30f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.7f;
	rightRBottomB[1] = -1.30f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.7f;
	rightRTopB[1] = -1.30f;
	rightRTopB[2] = -0.41f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[0] = -0.45f;
	leftLTopB[1] = 0.5f;
	leftLTopB[2] = -0.41f;
	leftLBottomB[0] = -0.45f;
	leftLBottomB[1] = 0.5f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.5f;
	leftRBottomB[1] = 0.5f;
	leftRBottomB[2] = -0.45f;
	leftRTopB[0] = -0.5f;
	leftRTopB[1] = 0.5f;
	leftRTopB[2] = -0.41f;

	rightLTopB[0] = -0.7f;
	rightLTopB[1] = -1.30f;
	rightLTopB[2] = -0.41f;
	rightLBottomB[0] = -0.7f;
	rightLBottomB[1] = -1.30f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.65f;
	rightRBottomB[1] = -1.30f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.65f;
	rightRTopB[1] = -1.30f;
	rightRTopB[2] = -0.41f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	leftLTopB[0] = -0.45f;
	leftLTopB[1] = 0.5f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.45f;
	leftLBottomB[1] = 0.5f;
	leftLBottomB[2] = 0.41f;
	leftRBottomB[0] = -0.5f;
	leftRBottomB[1] = 0.5f;
	leftRBottomB[2] = 0.41f;
	leftRTopB[0] = -0.5f;
	leftRTopB[1] = 0.5f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.7f;
	rightLTopB[1] = -1.30f;
	rightLTopB[2] = 0.45f;
	rightLBottomB[0] = -0.7f;
	rightLBottomB[1] = -1.30f;
	rightLBottomB[2] = 0.41f;
	rightRBottomB[0] = -0.65f;
	rightRBottomB[1] = -1.30f;
	rightRBottomB[2] = 0.41f;
	rightRTopB[0] = -0.65f;
	rightRTopB[1] = -1.30f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//truss for lower support
	//A1
	leftLTopB[0] = -0.55f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.55f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//A2
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//C1
	leftLTopB[0] = -0.55f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.55f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//C2
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B1
	leftLTopB[0] = 0.57f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.57f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.6f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = 0.6f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.57f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.57f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B2
	leftLTopB[0] = 0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.63f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = 0.63f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.63f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = 0.63f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//D1
	leftLTopB[0] = -0.57f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.57f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.6f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = -0.6f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.57f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.57f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//D2
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.63f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = -0.63f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.63f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = -0.63f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//cross truss bars

	//A0-A1
	leftLTopB[0] = -0.5f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.5f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.5f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = -0.5f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//A1-A0
	leftLTopB[0] = 0.5f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.5f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.5f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = 0.5f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = -0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = -0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = -0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = -0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B0-B1
	leftLTopB[0] = 0.52f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.52f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.55f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = 0.55f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.58f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = 0.58f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B1-B0
	leftLTopB[0] = 0.52f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = 0.52f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = 0.55f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = -0.45f;
	leftRTopB[0] = 0.55f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = -0.45f;

	rightLTopB[0] = 0.58f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.45f;
	rightLBottomB[0] = 0.58f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.45f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//c0-c1
	leftLTopB[0] = -0.5f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.5f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.5f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = -0.5f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//C1-C0
	leftLTopB[0] = 0.5f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = 0.5f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = 0.5f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = 0.5f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = -0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = -0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = -0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//D0-D1
	leftLTopB[0] = -0.52f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.52f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.58f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = -0.58f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//D1-D0
	leftLTopB[0] = -0.52f;
	leftLTopB[1] = 0.48f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.52f;
	leftLBottomB[1] = 0.43f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = 0.43f;
	leftRBottomB[2] = -0.45f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = 0.48f;
	leftRTopB[2] = -0.45f;

	rightLTopB[0] = -0.58f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.45f;
	rightLBottomB[0] = -0.58f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.45f;
	rightRBottomB[0] = -0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = -0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//A1-A2
	leftLTopB[0] = -0.55f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.55f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//A2-A1
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.47f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = 0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = 0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = 0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = 0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B1-B2
	leftLTopB[0] = 0.57f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.57f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.6f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = 0.6f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.63f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = 0.63f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//B2-B1
	leftLTopB[0] = 0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = 0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = 0.63f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = 0.63f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = 0.57f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.57f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//C1-C2
	leftLTopB[0] = -0.55f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.55f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.55f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = -0.55f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = 0.6f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = 0.6f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = 0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//C2-C1
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = -0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = -0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = -0.47f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = -0.47f;

	rightLTopB[0] = 0.55f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.47f;
	rightLBottomB[0] = 0.55f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.47f;
	rightRBottomB[0] = 0.55f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = 0.55f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

	//D1-D2
	leftLTopB[0] = -0.57f;
	leftLTopB[1] = -0.1f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.57f;
	leftLBottomB[1] = -0.15f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.6f;
	leftRBottomB[1] = -0.15f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = -0.6f;
	leftRTopB[1] = -0.1f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.63f;
	rightLTopB[1] = -0.60f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = -0.63f;
	rightLBottomB[1] = -0.650f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.6f;
	rightRBottomB[1] = -0.650f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.6f;
	rightRTopB[1] = -0.60f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);


	//D2-D1
	leftLTopB[0] = -0.6f;
	leftLTopB[1] = -0.6f;
	leftLTopB[2] = 0.45f;
	leftLBottomB[0] = -0.6f;
	leftLBottomB[1] = -0.65f;
	leftLBottomB[2] = 0.45f;
	leftRBottomB[0] = -0.63f;
	leftRBottomB[1] = -0.65f;
	leftRBottomB[2] = 0.45f;
	leftRTopB[0] = -0.63f;
	leftRTopB[1] = -0.6f;
	leftRTopB[2] = 0.45f;

	rightLTopB[0] = -0.6f;
	rightLTopB[1] = -0.10f;
	rightLTopB[2] = -0.45f;
	rightLBottomB[0] = -0.6f;
	rightLBottomB[1] = -0.150f;
	rightLBottomB[2] = -0.45f;
	rightRBottomB[0] = -0.57f;
	rightRBottomB[1] = -0.150f;
	rightRBottomB[2] = -0.45f;
	rightRTopB[0] = -0.57f;
	rightRTopB[1] = -0.10f;
	rightRTopB[2] = -0.45f;

	SquareRod(leftLTopB, leftLBottomB, leftRBottomB, leftRTopB, rightLTopB, rightLBottomB, rightRBottomB, rightRTopB, drawShadow);

}

void DrawHome(bool drawShadow)
{

	//function declaration
	void SquareRod(GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, GLfloat*, bool);

	glRotatef(yRot, 0.0f, 1.0f, 0.0f);
	glScalef(1.2f, 1.2f, 1.2f);
	glTranslatef(1.0f, 0.0f, -4.0f);
	

	glBindTexture(GL_TEXTURE_2D, texture_brick);

	GLfloat leftLTop[3] = { 1.0f, 0.40f, 0.1f };
	GLfloat leftLBottom[3] = { 1.0f, 0.40f, -2.1f };
	GLfloat leftRBottom[3] = { -1.0f, 0.40f, -2.1f };
	GLfloat leftRTop[3] = { -1.0f, 0.40f, 0.1f };

	GLfloat rightLTop[3] = { -1.0f, -1.0f, 0.1f };
	GLfloat rightLBottom[3] = { -1.0f, -1.0f, -2.1f };
	GLfloat rightRBottom[3] = { 1.0f, -1.0f, -2.1f };
	GLfloat rightRTop[3] = { 1.0f, -1.0f, 0.1f };

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);


	leftLTop[0] = 0.5f;
	leftLTop[1] = 1.5f;
	leftLTop[2] = -0.3f;
	leftLBottom[0] = 0.50f;
	leftLBottom[1] = 1.5f;
	leftLBottom[2] = -2.1f;
	leftRBottom[0] = -1.0f;
	leftRBottom[1] = 1.5f;
	leftRBottom[2] = -2.1f;
	leftRTop[0] = -1.0f;
	leftRTop[1] = 1.5f;
	leftRTop[2] = -0.3f;

	rightLTop[0] = -1.0f;
	rightLTop[1] = 0.4f;
	rightLTop[2] = -0.3f;
	rightLBottom[0] = -1.0f;
	rightLBottom[1] = 0.4f;
	rightLBottom[2] = -2.1f;
	rightRBottom[0] = 0.5f;
	rightRBottom[1] = 0.4f;
	rightRBottom[2] = -2.1f;
	rightRTop[0] = 0.5f;
	rightRTop[1] = 0.4f;
	rightRTop[2] = -0.3f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = -0.25f;
	leftLTop[1] = 2.0f;
	leftLTop[2] = -2.3f;
	leftLBottom[0] = -0.250f;
	leftLBottom[1] = 2.0f;
	leftLBottom[2] = -2.3f;
	leftRBottom[0] = -1.20f;
	leftRBottom[1] = 1.3f;
	leftRBottom[2] = -2.3f;
	leftRTop[0] = -1.20f;
	leftRTop[1] = 1.4f;
	leftRTop[2] = -2.3f;

	rightLTop[0] = -1.20f;
	rightLTop[1] = 1.4f;
	rightLTop[2] = -0.2f;
	rightLBottom[0] = -1.20f;
	rightLBottom[1] = 1.3f;
	rightLBottom[2] = -0.2f;
	rightRBottom[0] = -0.25f;
	rightRBottom[1] = 2.0f;
	rightRBottom[2] = -0.2f;
	rightRTop[0] = -0.20f;
	rightRTop[1] = 2.0f;
	rightRTop[2] = -0.2f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = 0.7f;
	leftLTop[1] = 1.40f;
	leftLTop[2] = -2.3f;
	leftLBottom[0] = 0.70f;
	leftLBottom[1] = 1.350f;
	leftLBottom[2] = -2.3f;
	leftRBottom[0] = -0.250f;
	leftRBottom[1] = 2.0f;
	leftRBottom[2] = -2.3f;
	leftRTop[0] = -0.250f;
	leftRTop[1] = 2.0f;
	leftRTop[2] = -2.3f;

	rightLTop[0] = -0.250f;
	rightLTop[1] = 2.0f;
	rightLTop[2] = -0.2f;
	rightLBottom[0] = -0.250f;
	rightLBottom[1] = 2.0f;
	rightLBottom[2] = -0.2f;
	rightRBottom[0] = 0.70f;
	rightRBottom[1] = 1.350f;
	rightRBottom[2] = -0.2f;
	rightRTop[0] = 0.7f;
	rightRTop[1] = 1.40f;
	rightRTop[2] = -0.2f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	//upper triangle
	glBegin(GL_TRIANGLES);
	glVertex3f(-1.0f, 1.5f, -0.3f);
	glVertex3f(0.5f, 1.5f, -0.3f);
	glVertex3f(-0.25f, 2.0f, -0.3f);

	glVertex3f(-1.0f, 1.5f, -2.1f);
	glVertex3f(0.5f, 1.5f, -2.1f);
	glVertex3f(-0.25f, 2.0f, -2.1f);

	glEnd();

	//railing
	leftLTop[0] = -1.0f;
	leftLTop[1] = 0.50f;
	leftLTop[2] = 0.09f;
	leftLBottom[0] = -1.0f;
	leftLBottom[1] = 0.4750f;
	leftLBottom[2] = 0.09f;
	leftRBottom[0] = -1.0f;
	leftRBottom[1] = 0.4750f;
	leftRBottom[2] = 0.1f;
	leftRTop[0] = -1.0f;
	leftRTop[1] = 0.50f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.50f;
	rightLTop[2] = 0.1f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.4750f;
	rightLBottom[2] = 0.1f;
	rightRBottom[0] = 1.0f;
	rightRBottom[1] = 0.4750f;
	rightRBottom[2] = 0.09f;
	rightRTop[0] = 1.0f;
	rightRTop[1] = 0.5f;
	rightRTop[2] = 0.09f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = -1.0f;
	leftLTop[1] = 0.60f;
	leftLTop[2] = 0.09f;
	leftLBottom[0] = -1.0f;
	leftLBottom[1] = 0.5750f;
	leftLBottom[2] = 0.09f;
	leftRBottom[0] = -1.0f;
	leftRBottom[1] = 0.5750f;
	leftRBottom[2] = 0.1f;
	leftRTop[0] = -1.0f;
	leftRTop[1] = 0.60f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.60f;
	rightLTop[2] = 0.1f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.5750f;
	rightLBottom[2] = 0.1f;
	rightRBottom[0] = 1.0f;
	rightRBottom[1] = 0.5750f;
	rightRBottom[2] = 0.09f;
	rightRTop[0] = 1.0f;
	rightRTop[1] = 0.6f;
	rightRTop[2] = 0.09f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = 0.990f;
	leftLTop[1] = 0.50f;
	leftLTop[2] = 0.1f;
	leftLBottom[0] = 0.990f;
	leftLBottom[1] = 0.4750f;
	leftLBottom[2] = 0.1f;
	leftRBottom[0] = 1.0f;
	leftRBottom[1] = 0.4750f;
	leftRBottom[2] = 0.1f;
	leftRTop[0] = 1.0f;
	leftRTop[1] = 0.50f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.50f;
	rightLTop[2] = -2.1f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.4750f;
	rightLBottom[2] = -2.1f;
	rightRBottom[0] = 0.990f;
	rightRBottom[1] = 0.4750f;
	rightRBottom[2] = -2.1f;
	rightRTop[0] = 0.990f;
	rightRTop[1] = 0.5f;
	rightRTop[2] = -2.1f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = 0.990f;
	leftLTop[1] = 0.60f;
	leftLTop[2] = 0.1f;
	leftLBottom[0] = 0.990f;
	leftLBottom[1] = 0.5750f;
	leftLBottom[2] = 0.1f;
	leftRBottom[0] = 1.0f;
	leftRBottom[1] = 0.5750f;
	leftRBottom[2] = 0.1f;
	leftRTop[0] = 1.0f;
	leftRTop[1] = 0.60f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.60f;
	rightLTop[2] = -2.1f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.5750f;
	rightLBottom[2] = -2.1f;
	rightRBottom[0] = 0.990f;
	rightRBottom[1] = 0.5750f;
	rightRBottom[2] = -2.1f;
	rightRTop[0] = 0.990f;
	rightRTop[1] = 0.6f;
	rightRTop[2] = -2.1f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = 0.90f;
	leftLTop[1] = 0.650f;
	leftLTop[2] = 0.1f;
	leftLBottom[0] = 0.90f;
	leftLBottom[1] = 0.65f;
	leftLBottom[2] = 0.08f;
	leftRBottom[0] = 1.0f;
	leftRBottom[1] = 0.650f;
	leftRBottom[2] = 0.08f;
	leftRTop[0] = 1.0f;
	leftRTop[1] = 0.650f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.40f;
	rightLTop[2] = 0.1f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.70f;
	rightLBottom[2] = 0.08f;
	rightRBottom[0] = 0.90f;
	rightRBottom[1] = 0.70f;
	rightRBottom[2] = 0.08f;
	rightRTop[0] = 0.90f;
	rightRTop[1] = 0.4f;
	rightRTop[2] = 0.1f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = -0.90f;
	leftLTop[1] = 0.650f;
	leftLTop[2] = 0.1f;
	leftLBottom[0] = -0.90f;
	leftLBottom[1] = 0.65f;
	leftLBottom[2] = 0.08f;
	leftRBottom[0] = -1.0f;
	leftRBottom[1] = 0.650f;
	leftRBottom[2] = 0.08f;
	leftRTop[0] = -1.0f;
	leftRTop[1] = 0.650f;
	leftRTop[2] = 0.1f;

	rightLTop[0] = -1.0f;
	rightLTop[1] = 0.40f;
	rightLTop[2] = 0.1f;
	rightLBottom[0] = -1.0f;
	rightLBottom[1] = 0.70f;
	rightLBottom[2] = 0.08f;
	rightRBottom[0] = -0.90f;
	rightRBottom[1] = 0.70f;
	rightRBottom[2] = 0.08f;
	rightRTop[0] = -0.90f;
	rightRTop[1] = 0.4f;
	rightRTop[2] = 0.1f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	leftLTop[0] = 0.90f;
	leftLTop[1] = 0.650f;
	leftLTop[2] = -2.08f;
	leftLBottom[0] = 0.90f;
	leftLBottom[1] = 0.65f;
	leftLBottom[2] = -2.1f;
	leftRBottom[0] = 1.0f;
	leftRBottom[1] = 0.650f;
	leftRBottom[2] = -2.1f;
	leftRTop[0] = 1.0f;
	leftRTop[1] = 0.650f;
	leftRTop[2] = -2.08f;

	rightLTop[0] = 1.0f;
	rightLTop[1] = 0.40f;
	rightLTop[2] = -2.08f;
	rightLBottom[0] = 1.0f;
	rightLBottom[1] = 0.70f;
	rightLBottom[2] = -2.1f;
	rightRBottom[0] = 0.90f;
	rightRBottom[1] = 0.70f;
	rightRBottom[2] = -2.1f;
	rightRTop[0] = 0.90f;
	rightRTop[1] = 0.4f;
	rightRTop[2] = -2.08f;

	SquareRod(leftLTop, leftLBottom, leftRBottom, leftRTop, rightLTop, rightLBottom, rightRBottom, rightRTop, drawShadow);

	//Door

	glBindTexture(GL_TEXTURE_2D, texture_door);
	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.3f, 0.1f, 0.12f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.12f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.12f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.3f, 0.1f, 0.12f);



	//window
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.01f, 0.071f, -0.25f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.01f, -0.70f, -0.25f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.01f, -0.70f, -1.5f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.01f, 0.071f, -1.5f);


	glEnd();


}


void SquareRod(GLfloat* leftLTop, GLfloat* leftLBottom, GLfloat* leftRBottom, GLfloat* leftRTop, GLfloat* rightLTop, GLfloat* rightLBottoom, GLfloat* rightRBottom, GLfloat* rightRTop, bool shadowFlag)
{
	if (shadowFlag)
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glBegin(GL_QUADS);


		glVertex3fv(leftRTop);
		glVertex3fv(leftRBottom);
		glVertex3fv(rightLBottoom);
		glVertex3fv(rightLTop);


		//2

		glVertex3fv(leftRTop);
		glVertex3fv(rightLTop);
		glVertex3fv(rightRTop);
		glVertex3fv(leftLTop);


		//3
		glVertex3fv(rightRTop);
		glVertex3fv(rightRBottom);
		glVertex3fv(leftLBottom);
		glVertex3fv(leftLTop);

		//4
		glVertex3fv(rightLBottoom);
		glVertex3fv(leftRBottom);
		glVertex3fv(leftLBottom);
		glVertex3fv(rightRBottom);
		glEnd();
	}
	else
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glBegin(GL_QUADS);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(leftRTop);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(leftRBottom);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(rightLBottoom);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(rightLTop);


		//2

		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(leftRTop);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(rightLTop);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(rightRTop);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(leftLTop);


		//3
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(rightRTop);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(rightRBottom);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(leftLBottom);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(leftLTop);

		//4
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(rightLBottoom);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(leftRBottom);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(leftLBottom);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(rightRBottom);
		glEnd();
	}

}

bool LoadTexture(GLuint *texture, TCHAR imageResource[])
{
	HBITMAP hBitmap;
	BITMAP bmp;
	bool bStatus = false;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResource, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glBindTexture(GL_TEXTURE_2D, 0);
		DeleteObject(hBitmap);
		bStatus = true;

	}
	return bStatus;
}

void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

	if (wglGetCurrentContext() == hglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc)
	{
		wglDeleteContext(hglrc);
		hglrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf_s(gbFile, "log file closed successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}

	if (texture_rock)
	{
		glDeleteTextures(1, &texture_rock);
	}

	if (texture_door)
	{
		glDeleteTextures(1, &texture_door);
	}

	if (texture_grass)
	{
		glDeleteTextures(1, &texture_grass);
	}

	if (texture_sky)
	{
		glDeleteTextures(1, &texture_sky);
	}

	if (texture_brick)
	{
		glDeleteTextures(1, &texture_brick);
	}
}












