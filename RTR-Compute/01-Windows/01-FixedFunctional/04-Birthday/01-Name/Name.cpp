#include<windows.h>
#include<gl/gl.h>
#include<stdio.h>
#include<math.h>
#include<gl/GLU.h>

#define winwidth 800
#define winhight 600
#define GL_PI 3.14159
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD dwStyle;
WINDOWPLACEMENT wpPrev;
bool gbFullscreen = false;
HWND ghwnd;
HDC ghdc = NULL;
HGLRC ghrc;
bool gbActiveWindow = false;
FILE *gbFile = NULL;
int xPos;
int yPos;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndClass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;


	void Initialize(void);
	//void Display(void);

	wpPrev = { sizeof(WINDOWPLACEMENT) };

	if ((fopen_s(&gbFile, "log.txt", "w")))
	{
		MessageBox(NULL, TEXT("File not opened"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gbFile, "log file successfullly created, program started successful\n");
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		winwidth,
		winhight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;


	Initialize();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//here should call update function for openGL rendering

				//here should call display function for openGL rendering
				//Display();
			}
		}
	}
	return (int)msg.wParam;

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		Display();
		break;

	case WM_MOUSEMOVE:
		xPos = LOWORD(lParam);
		yPos = HIWORD(lParam);
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };
	//code

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullscreen = true;
	}

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullscreen = false;
	}

}


void ChangeDisplay(void)
{
	DEVMODE dMode;
	DWORD width, height;
	TCHAR str[255];
	MONITORINFO mi = { sizeof(MONITORINFO) };
	RECT rc;

	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dMode);

	width = (DWORD)GetSystemMetrics(SM_CXFULLSCREEN);
	height = (DWORD)GetSystemMetrics(SM_CYFULLSCREEN);

	GetClientRect(ghwnd, &rc);
	wsprintf(str, "%ld", width);
	//MessageBox(ghwnd, str, TEXT("message"), MB_OK);

	wsprintf(str, "%ld", height);
	//MessageBox(ghwnd, str, TEXT("message"), MB_OK);

	if (gbFullscreen == false)
	{
		dMode.dmSize = sizeof(DEVMODE);
		dMode.dmPelsHeight = (rc.bottom - rc.top);
		dMode.dmPelsWidth = (rc.right - rc.left);
		dMode.dmBitsPerPel = 32;
		dMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFLAGS | DM_DISPLAYFREQUENCY | DM_POSITION;
		GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi);
		SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

		ChangeDisplaySettings(&dMode, CDS_FULLSCREEN);
		//ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		ChangeDisplaySettings(NULL, 0);
		//	ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize()
{
	//function declaration
	void Resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf_s(gbFile, "choosepixel()  failed\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf_s(gbFile, "SetPixel()  failed\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf_s(gbFile, "wglCreateContext()  failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf_s(gbFile, "wglMakeCurrent()  failed\n");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glShadeModel(GL_FLAT);
	glFrontFace(GL_CW);

	Resize(winwidth, winhight);

}

void Resize(int width, int height)
{
	//variable declaration
	GLfloat aspectRatio;

	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	aspectRatio = (GLfloat)width / (GLfloat)height;

	gluPerspective(45.0f, aspectRatio, 1.1f, 100.0f);

	

	/*if (width <= height)
		glOrtho(-100.0f, 100.0f, -100.0f / aspectRatio, 100.0f / aspectRatio, -100.f, 100.0f);

	else
		glOrtho(-100.0f * aspectRatio, 100.0f * aspectRatio, -100.0f, 100.0f, -100.0f, 100.0f);*/

	
}

void Display(void)
{
	//function declaration



   //variable declaration
	GLfloat x, y, z, angle;
	GLfloat curSize[2];

	glClear(GL_COLOR_BUFFER_BIT);

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -5.0f);
	x = -30.0f;
	y = -10.0f;
	glGetFloatv(GL_LINE_WIDTH, curSize);

	x = curSize[1];

	fprintf_s(gbFile, "%f\n", x);
	glLineWidth(4.0f);

	
	//S
	glBegin(GL_LINE_STRIP);

	
	glVertex2f(-105.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-90.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-90.0f / 100.0f, -5.0f / 100.0f);
	glVertex2f(-105.0f / 100.0f, -5.0f / 100.0f);
	glVertex2f(-105.0f / 100.0f, 10.0f / 100.0f);
	glVertex2f(-90.0f / 100.0f, 10.0f / 100.0f);

	glEnd();


	//A
	glBegin(GL_LINE_STRIP);

	glVertex2f(-85.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-85.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-70.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-70.0f/100.0f, -20.0f/100.0f);

	glEnd();


	
	glBegin(GL_LINES);
	glVertex2f(-85.0f/100.0f, -5.0f/100.0f);
	glVertex2f(-70.0f/100.0f, -5.0f/100.0f);

	glEnd();


	//U
	glBegin(GL_LINE_STRIP);

	glVertex2f(-65.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-65.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-50.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-50.0f/100.0f, 10.0f/100.0f);

	glEnd();


	//R
	glBegin(GL_LINE_STRIP);

	glVertex2f(-45.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-45.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-30.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-30.0f/100.0f, -2.0f/100.0f);
	glVertex2f(-45.0f/100.0f, -2.0f/100.0f);
	glVertex2f(-30.0f/100.0f, -20.0f/100.0f);

	glEnd();


	//A
	glBegin(GL_LINE_STRIP);

	glVertex2f(-25.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-25.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-10.0f/100.0f, 10.0f/100.0f);
	glVertex2f(-10.0f/100.0f, -20.0f/100.0f);

	glEnd();

	glBegin(GL_LINES);

	glVertex2f(-25.0f/100.0f, -5.0f/100.0f);
	glVertex2f(-10.0f/100.0f, -5.0f/100.0f);

	glEnd();

	
	//B
	glBegin(GL_LINE_STRIP);

	glVertex2f(-5.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-5.0f/100.0f, 10.0f/100.0f);
	glVertex2f(3.0f/100.0f, 10.0f/100.0f);
	glVertex2f(10.0f/100.0f, 5.0f/100.0f);
	glVertex2f(10.0f/100.0f, -5.0f/100.0f);
	glVertex2f(-5.0f/100.0f, -5.0f/100.0f);
	glVertex2f(10.0f/100.0f, -5.0f/100.0f);
	glVertex2f(10.0f/100.0f, -15.0f/100.0f);
	glVertex2f(3.0f/100.0f, -20.0f/100.0f);
	glVertex2f(-5.0f/100.0f, -20.0f/100.0f);

	glEnd();

	//H
	glBegin(GL_LINES);

	glVertex2f(15.0f/100.0f, -20.0f/100.0f);
	glVertex2f(15.0f/100.0f, 10.0f/100.0f);
	glVertex2f(15.0f/100.0f, -5.0f/100.0f);
	glVertex2f(30.0f/100.0f, -5.0f/100.0f);
	glVertex2f(30.0f/100.0f, 10.0f/100.0f);
	glVertex2f(30.0f/100.0f, -20.0f/100.0f);

	glEnd();

	//glPointSize(4.0f);

	/*glBegin(GL_POINTS);

	glVertex2f(40.0f/100.0f, -20.0f/100.0f);

	glEnd();*/


	//D
	glBegin(GL_LINE_STRIP);

	glVertex2f(-65.0f/100.0f, -60.0f/100.0f);
	glVertex2f(-65.0f/100.0f, -30.0f/100.0f);
	glVertex2f(-57.0f/100.0f, -30.0f/100.0f);
	glVertex2f(-49.0f/100.0f, -35.0f/100.0f);
	glVertex2f(-49.0f/100.0f, -55.0f/100.0f);
	glVertex2f(-57.0f/100.0f, -60.0f/100.0f);
	glVertex2f(-65.0f/100.0f, -60.0f/100.0f);

	glEnd();

	//E

	glBegin(GL_LINE_STRIP);

	glVertex2f(-45.0f/100.0f, -60.0f/100.0f);
	glVertex2f(-45.0f/100.0f, -30.0f/100.0f);
	glVertex2f(-30.0f/100.0f, -30.0f/100.0f);

	glEnd();

	glBegin(GL_LINES);

	glVertex2f(-45.0f / 100.0f, -45.0f/100.0f);
	glVertex2f(-30.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(-45.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(-30.0f / 100.0f, -60.0f / 100.0f);

	glEnd();

	//S
	glBegin(GL_LINE_STRIP);


	glVertex2f(-25.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(-10.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(-10.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(-25.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(-25.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(-10.0f / 100.0f, -30.0f / 100.0f);

	glEnd();

	//H
	glBegin(GL_LINES);

	glVertex2f(-5.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(-5.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(-5.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(10.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(10.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(10.0f / 100.0f, -60.0f / 100.0f);

	glEnd();

	//P
	glBegin(GL_LINE_STRIP);

	glVertex2f(15.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(15.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(23.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(30.0f / 100.0f, -34.0f / 100.0f);
	glVertex2f(30.0f / 100.0f, -41.0f / 100.0f);
	glVertex2f(23.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(15.0f / 100.0f, -45.0f / 100.0f);

	glEnd();

	//A
	glBegin(GL_LINE_STRIP);

	glVertex2f(35.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(35.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(50.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(50.0f / 100.0f, -60.0f / 100.0f);

	glEnd();



	glBegin(GL_LINES);
	glVertex2f(35.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(50.0f / 100.0f, -45.0f / 100.0f);

	glEnd();

	//N
	glBegin(GL_LINE_STRIP);

	glVertex2f(55.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(55.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(70.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(70.0f / 100.0f, -30.0f / 100.0f);

	glEnd();

	//D
	glBegin(GL_LINE_STRIP);

	glVertex2f(75.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(75.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(83.0f / 100.0f, -30.0f / 100.0f);
	glVertex2f(91.0f / 100.0f, -35.0f / 100.0f);
	glVertex2f(91.0f / 100.0f, -55.0f / 100.0f);
	glVertex2f(83.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(75.0f / 100.0f, -60.0f / 100.0f);

	glEnd();

	//E

	glBegin(GL_LINE_STRIP);

	glVertex2f(95.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(95.0f/100.0f, -30.0f/100.0f);
	glVertex2f(110.0f/100.0f, -30.0f/100.0f);

	glEnd();

	glBegin(GL_LINES);

	glVertex2f(95.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(110.0f / 100.0f, -45.0f / 100.0f);
	glVertex2f(95.0f / 100.0f, -60.0f / 100.0f);
	glVertex2f(110.0f / 100.0f, -60.0f / 100.0f);

	glEnd();



	glFlush();
}

void Uninitialize(void)
{
	//file IO code

	if (gbFile)
	{
		fprintf_s(gbFile, "log file closed successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}

}







