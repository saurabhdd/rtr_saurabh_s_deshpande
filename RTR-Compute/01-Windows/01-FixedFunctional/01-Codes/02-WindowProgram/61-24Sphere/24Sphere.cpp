#include<windows.h>
#include"Header.h"
#include<stdlib.h>
#include<stdio.h>
#include<GL/gl.h>
#include<gl/glu.h>
#include<math.h>

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")

#define _USE_MATH_DEFINES
#define WINWIDTH 800
#define WINHEIGHT 600
#define PI 3.14


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gbFile_SSD = NULL;

GLfloat angel_SSD = 0.0f;
DWORD dwstyle_SSD;

WINDOWPLACEMENT wpPrev_SSD = { sizeof(WINDOWPLACEMENT) };

bool gbFullscreen_SSD = false;
bool bLight_SSD = false;
bool gbActiveWindow_SSD = false;
HWND ghwnd_SSD = NULL;
HDC ghdc_SSD = NULL;
HGLRC ghrc_SSD = NULL;



GLfloat lAmbientzero_SSD[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lDiffusezero_SSD[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lPositionzero_SSD[] = { 0.0f,3.0f,3.0f,0.0f };
GLfloat Light_Model_Ambiant_SSD[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat Light_Model_Local_Viewer[] = { 0.0f };
GLfloat xRotation_SSD = 0.0f;
GLfloat yRotation_SSD = 0.0f;
GLfloat zRotation_SSD = 0.0f;
GLint keypressed_SSD = 0;
GLUquadric* quadric_SSD[24];


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	void initialize_SSD(void);
	void display_SSD(void);


	WNDCLASSEX wndclass_SSD;
	HWND hwnd_SSD;
	MSG msg_SSD;
	TCHAR szAppName_SSD[] = TEXT("MyApp");

	if (fopen_s(&gbFile_SSD, "logApp.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("file not created"), TEXT("error"), MB_OK);
		return(0);
	}
	else
	{
		fprintf(gbFile_SSD, "log file successfully created\n");
	}

	bool BDone_SSD = false;
	int length_SSD;
	int height_SSD;

	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass_SSD);

	length_SSD = GetSystemMetrics(SM_CXSCREEN) / 2;
	height_SSD = GetSystemMetrics(SM_CYSCREEN) / 2;

	int x_SSD = length_SSD - WINWIDTH / 2;
	int y_SSD = height_SSD - WINHEIGHT / 2;

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("WINDOW"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, x_SSD, y_SSD, WINWIDTH, WINHEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd_SSD = hwnd_SSD;
	initialize_SSD();

	ShowWindow(hwnd_SSD, iCmdShow);
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (BDone_SSD == false)
	{

		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				BDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);

			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				display_SSD();

			}
		}


	}

	return((int)msg_SSD.wParam);

}
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen_SSD();
	void resize_SSD(int, int);
	void uninitialized_SSD(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		resize_SSD(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen_SSD();
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'x':
		case 'X':
			keypressed_SSD = 1;

			xRotation_SSD = 0.0f;


			break;
		case 'y':
		case 'Y':
			keypressed_SSD = 2;

			yRotation_SSD = 0.0f;

			break;

		case 'z':
		case 'Z':
			keypressed_SSD = 3;

			zRotation_SSD = 0.0f;

			break;

		case 'L':
		case 'l':

			if (bLight_SSD == true)
			{
				bLight_SSD = false;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight_SSD = true;
				glDisable(GL_LIGHTING);
			}
			break;

		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CREATE:

		break;
	case WM_DESTROY:
		uninitialized_SSD();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullScreen_SSD(void)
{

	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };

	if (gbFullscreen_SSD == false)
	{
		dwstyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);

		if (dwstyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wpPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(false);
		gbFullscreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
		gbFullscreen_SSD = false;
	}

}

void initialize_SSD(void)
{

	void resize_SSD(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex_SSD;
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf(gbFile_SSD, "choosepixelformat fail\n");
		DestroyWindow(ghwnd_SSD);
	}


	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd) == FALSE)
	{
		fprintf(gbFile_SSD, "Setformatfunction fail\n");
		DestroyWindow(ghwnd_SSD);
	}


	ghrc_SSD = wglCreateContext(ghdc_SSD);
	if (ghrc_SSD == NULL)
	{
		fprintf(gbFile_SSD, "wglcreatecontext fail\n");
		DestroyWindow(ghwnd_SSD);
	}


	if (wglMakeCurrent(ghdc_SSD, ghrc_SSD) == FALSE)
	{
		fprintf(gbFile_SSD, "wglmakecurrent function fail fail\n");
		DestroyWindow(ghwnd_SSD);
	}


	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, Light_Model_Ambiant_SSD);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, Light_Model_Local_Viewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lAmbientzero_SSD);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lDiffusezero_SSD);
	glLightfv(GL_LIGHT0, GL_POSITION, lPositionzero_SSD);

	glEnable(GL_LIGHT0);

	for (int i_SSD = 0; i_SSD <= 24; i_SSD++)
	{
		quadric_SSD[0] = gluNewQuadric();

	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	resize_SSD(WINWIDTH, WINHEIGHT);

}
void resize_SSD(int width_SSD, int height_SSD)
{


	if (height_SSD == 0)
		height_SSD = 1;
	glViewport(0, 0, (GLsizei)width_SSD, GLsizei(height_SSD));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width_SSD <= height_SSD)
	{
		glOrtho(0.0f,
			15.5f,
			0.0f,
			15.5f * ((GLfloat)height_SSD / (GLfloat)width_SSD),
			-10.0f,
			10.0f);
	}
	else
	{
		glOrtho(0.0f,
			15.5f * ((GLfloat)width_SSD / (GLfloat)height_SSD),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}
	//gluPerspective(45.0f, ((GLfloat)width_SSD / (GLfloat)height_SSD), 0.1f, 100.0f);
}
void display_SSD(void)
{
	void draw24sphere(void);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (keypressed_SSD == 1)
	{
		glRotatef(xRotation_SSD, 1.0f, 0.0f, 0.0f);
		lPositionzero_SSD[1] = xRotation_SSD;

	}
	else if (keypressed_SSD == 2)
	{
		glRotatef(yRotation_SSD, 0.0f, 1.0f, 0.0f);
		lPositionzero_SSD[2] = yRotation_SSD;
	}
	else if (keypressed_SSD == 3)
	{
		glRotatef(zRotation_SSD, 0.0f, 0.0f, 1.0f);
		lPositionzero_SSD[0] = zRotation_SSD;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, lPositionzero_SSD);

	draw24sphere();

	xRotation_SSD += 1.0f;
	yRotation_SSD += 1.0f;
	zRotation_SSD += 1.0f;

	SwapBuffers(ghdc_SSD);
}


void draw24sphere(void)
{
	/////////////////////////////////////////////////////
	GLfloat MAmbiantzero_SSD[4] = { 0.0215f,0.1745,0.0215,1.0f };
	GLfloat MDiffusezero_SSD[4] = { 0.18275,0.17, 0.22525 ,1.0f };
	GLfloat MSpecularzero_SSD[4] = { 0.332741, 0.328634, 0.346435, 1.0f };

	float MShininesszero_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantzero_SSD);


	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusezero_SSD);


	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularzero_SSD);

	MShininesszero_SSD = 0.3 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesszero_SSD);


	/////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 14.0f, 0.0f);
	glRotatef(0.0f, lPositionzero_SSD[1], 0.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);


	GLfloat MAmbiantone_SSD[4] = { 0.135,0.2225,0.1575,1.0f };
	GLfloat MDiffuseone_SSD[4] = { 0.54,0.89,0.63,1.0f };
	GLfloat MSpecularone_SSD[4] = { 0.316228, 0.316228, 0.316228,1.0f };

	float MShininessone_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantone_SSD);


	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseone_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularone_SSD);

	MShininessone_SSD = 0.3 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessone_SSD);


	///////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.9f, 14.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);

	//obsidian *****
	GLfloat MAmbianttwo_SSD[4] = { 0.05375,0.05,0.06625,1.0f };
	GLfloat MDiffusetwo_SSD[4] = { 0.18275,0.17,0.22525,1.0f };
	GLfloat MSpeculartwo_SSD[4] = { 0.332741,0.328634,0.346435,1.0f };
	float MShininesstwo_SSD;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwo_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwo_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwo_SSD);

	MShininesstwo_SSD = 0.3 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwo_SSD);



	//////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.8f, 14.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	//pearl *****
	GLfloat MAmbiantthree_SSD[4] = { 0.25,0.20725,0.20725 ,1.0f };
	GLfloat MDiffusethree_SSD[4] = { 1.0,0.829,0.829,1.0f };
	GLfloat MSpecularthree_SSD[4] = { 0.296648,0.296648,0.296648,1.0f };
	float MShininessthree_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantthree_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusethree_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularthree_SSD);

	MShininessthree_SSD = 0.3 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessthree_SSD);


	//////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 14.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);

	GLfloat MAmbiantfour_SSD[4] = { 0.1745,0.01175,0.01175  ,1.0f };
	GLfloat MDiffusefour_SSD[4] = { 0.61424,0.04136,0.04136 ,1.0f };
	GLfloat MSpecularfour_SSD[4] = { 0.727811,0.626959,0.626959 ,1.0f };
	float MShininessfour_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantfour_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusefour_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularfour_SSD);

	MShininessfour_SSD = 0.6 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessfour_SSD);


	/////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 11.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantfive_SSD[4] = { 0.1f,0.18725f,0.1745f,1.0f };
	GLfloat MDiffusefive_SSD[4] = { 0.396,0.74151,0.69102,1.0f };
	GLfloat MSpecularfive_SSD[4] = { 0.297254,0.30829,0.306678,1.0f };
	float MShininessfive_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantfive_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusefive_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularfive_SSD);

	MShininessfive_SSD = 0.1 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessfive_SSD);


	/////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.9f, 11.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	// brass *****
	GLfloat MAmbiantsix_SSD[4] = { 0.329412,0.223529,0.027451,1.0f };
	GLfloat MDiffusesix_SSD[4] = { 0.780392, 0.568627,0.113725,1.0f };
	GLfloat MSpecularsix_SSD[4] = { 0.992157,0.941176,0.807843 ,1.0f };
	float MShininesssix_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantsix_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusesix_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularsix_SSD);

	MShininesssix_SSD = 0.21794872 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesssix_SSD);


	///////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.8f, 11.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantseven_SSD[4] = { 0.2125,0.1275,0.054,1.0f };
	GLfloat MDiffuseseven_SSD[4] = { 0.714,0.4284, 0.18144,1.0f };
	GLfloat MSpecularseven_SSD[4] = { 0.393548,0.271906,0.166721,1.0f };
	float MShininessseven_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantseven_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseseven_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularseven_SSD);

	MShininessseven_SSD = 0.2 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessseven_SSD);


	//////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 11.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianteight_SSD[4] = { 0.25,0.25,0.25  ,1.0f };
	GLfloat MDiffuseeight_SSD[4] = { 0.4,0.4,0.4 ,1.0f };
	GLfloat MSpeculareight_SSD[4] = { 0.774597,0.774597,0.774597 ,1.0f };
	float MShininesseight_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianteight_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseeight_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculareight_SSD);

	MShininesseight_SSD = 0.6 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesseight_SSD);


	///////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 9.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantnine_SSD[4] = { 0.19125,0.0735,0.0225,1.0f };
	GLfloat MDiffusenine_SSD[4] = { 0.7038,0.27048,0.0828,1.0f };
	GLfloat MSpecularnine_SSD[4] = { 0.256777,0.137622,0.086014,1.0f };
	float MShininessnine_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantnine_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusenine_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularnine_SSD);

	MShininessnine_SSD = 0.1 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessnine_SSD);


	//////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.9f, 9.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantten_SSD[4] = { 0.24725, 0.1995,0.0745,1.0f };
	GLfloat MDiffuseten_SSD[4] = { 0.75164,0.60648,0.22648,1.0f };
	GLfloat MSpecularten_SSD[4] = { 0.628281,0.555802,0.366065 ,1.0f };
	float MShininessten_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantfour_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseten_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularten_SSD);

	MShininessten_SSD = 0.4 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessten_SSD);


	//////////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.8f, 9.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantelven_SSD[4] = { 0.19225,0.19225,0.19225,1.0f };
	GLfloat MDiffuseelven_SSD[4] = { 0.50754,0.50754,0.50754 ,1.0f };
	GLfloat MSpecularelven_SSD[4] = { 0.508273,0.508273,0.508273 ,1.0f };
	float MShininesselven_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantelven_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseelven_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularelven_SSD);

	MShininesselven_SSD = 0.4 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesselven_SSD);


	////////////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 9.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianttwelve_SSD[4] = { 0.0f,0.0f,0.0f  ,1.0f };
	GLfloat MDiffusetwelve_SSD[4] = { 0.01,0.01,0.01,1.0f };
	GLfloat MSpeculartwelve_SSD[4] = { 0.50,0.50,0.50 ,1.0f };
	float MShininesstwelve_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwelve_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwelve_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwelve_SSD);

	MShininesstwelve_SSD = 0.25 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwelve_SSD);



	////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 6.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantthirtyeen_SSD[4] = { 0.0f,0.1f,0.06f ,1.0f };
	GLfloat MDiffusethirtyeen_SSD[4] = { 0.0f, 0.50980392,0.50980392,1.0f };
	GLfloat MSpecularthirtyeen_SSD[4] = { 0.50196078,0.50196078,0.50196078 ,1.0f };
	float MShininessthirtyeen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantthirtyeen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusethirtyeen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularthirtyeen_SSD);

	MShininessthirtyeen_SSD = 0.25 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessthirtyeen_SSD);



	////////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.9f, 6.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantfourteen_SSD[4] = { 0.0f,0.0f,0.0f ,1.0f };
	GLfloat MDiffusefourteen_SSD[4] = { 0.1f,0.35f,0.1f ,1.0f };
	GLfloat MSpecularfourteen_SSD[4] = { 0.45f,0.55f,0.45f ,1.0f };
	float MShininessfourteen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantfourteen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusefourteen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularfourteen_SSD);

	MShininessfourteen_SSD = 0.25 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessfourteen_SSD);



	/////////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.8f, 6.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantfifteen_SSD[4] = { 0.0f,0.0f,0.0f ,1.0f };
	GLfloat MDiffusefifteen_SSD[4] = { 0.5f,0.0f,0.0f ,1.0f };
	GLfloat MSpecularfifteen_SSD[4] = { 0.7f,0.6f,0.6f ,1.0f };
	float MShininessfifteen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantfifteen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusefifteen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularfifteen_SSD);

	MShininessfifteen_SSD = 0.25 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessfifteen_SSD);


	///////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 6.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantsixteen_SSD[4] = { 0.0f,0.0f,0.0f ,1.0f };
	GLfloat MDiffusesixteen_SSD[4] = { 0.55f,0.55f,0.55f ,1.0f };
	GLfloat MSpecularsixteen_SSD[4] = { 0.70f,0.70f,0.70f ,1.0f };
	float MShininesssixteen_SSD;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantsixteen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusesixteen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularsixteen_SSD);

	MShininesssixteen_SSD = 0.25 * 128;


	glMaterialf(GL_FRONT, GL_SHININESS, MShininesssixteen_SSD);



	////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 4.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantseventeen_SSD[4] = { 0.0f,0.0f,0.0f,1.0f };
	GLfloat MDiffuseseventeen_SSD[4] = { 0.5f,0.5f,0.0f,1.0f };
	GLfloat MSpecularseventeen_SSD[4] = { 0.60f,0.60f,0.50f ,1.0f };
	float MShininessseventeen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantseventeen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseseventeen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularseventeen_SSD);
	MShininessseventeen_SSD = 0.25 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessseventeen_SSD);




	////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.9f, 4.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianteighteen_SSD[4] = { 0.02,0.02,0.02 ,1.0f };
	GLfloat MDiffuseeighteen_SSD[4] = { 0.01,0.01,0.01 ,1.0f };
	GLfloat MSpeculareighteen_SSD[4] = { 0.4,0.4,0.4,1.0f };
	float MShininesseighteen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianteighteen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffuseeighteen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculareighteen_SSD);

	MShininesseighteen_SSD = 0.078125 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesseighteen_SSD);



	////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.8f, 4.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbiantnineteen_SSD[4] = { 0.0f,0.05f,0.05f  ,1.0f };
	GLfloat MDiffusenineteen_SSD[4] = { 0.4f,0.5f,0.5f ,1.0f };
	GLfloat MSpecularnineteen_SSD[4] = { 0.04f,0.7f,0.7f ,1.0f };
	float MShininessnineteen_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbiantnineteen_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusenineteen_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpecularnineteen_SSD);
	MShininessnineteen_SSD = 0.078125 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininessnineteen_SSD);



	////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 4.0f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianttwenty_SSD[4] = { 0.0f,0.05f,0.0f  ,1.0f };
	GLfloat MDiffusetwenty_SSD[4] = { 0.4f,0.5f,0.4f ,1.0f };
	GLfloat MSpeculartwenty_SSD[4] = { 0.04f,0.7f,0.04f ,1.0f };
	float MShininesstwenty_SSD;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwenty_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwenty_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwenty_SSD);

	MShininesstwenty_SSD = 0.078125 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwenty_SSD);


	//////////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 1.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);

	GLfloat MAmbianttwentyone_SSD[4] = { 0.05f,0.0f,0.0f ,1.0f };
	GLfloat MDiffusetwentyone_SSD[4] = { 0.5f,0.4f,0.4f ,1.0f };
	GLfloat MSpeculartwentyone_SSD[4] = { 0.7f,0.04,0.04 ,1.0f };
	float MShininesstwentyone_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwentyone_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwentyone_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwentyone_SSD);
	MShininesstwentyone_SSD = 0.078125 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwentyone_SSD);



	//////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.8f, 1.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianttwentytwo_SSD[4] = { 0.05,0.05,0.05 ,1.0f };
	GLfloat MDiffusetwentytwo_SSD[4] = { 0.5f,0.5f,0.5f,1.0f };
	GLfloat MSpeculartwentytwo_SSD[4] = { 0.7f,0.7f,0.7f,1.0f };
	float MShininesstwentytwo_SSD;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwentytwo_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwentytwo_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwentytwo_SSD);
	MShininesstwentytwo_SSD = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwentytwo_SSD);



	/////////////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(17.7f, 1.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);
	GLfloat MAmbianttwentythree_SSD[4] = { 0.05f, 0.05f, 0.0f  ,1.0f };
	GLfloat MDiffusetwentythree_SSD[4] = { 0.5f, 0.5f, 0.4f, 1.0f };
	GLfloat MSpeculartwentythree_SSD[4] = { 0.7f, 0.7f, 0.04f, 1.0f };
	float MShininesstwentythree_SSD;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MAmbianttwentythree_SSD);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MDiffusetwentythree_SSD);

	glMaterialfv(GL_FRONT, GL_SPECULAR, MSpeculartwentythree_SSD);

	MShininesstwentythree_SSD = 0.078125 * 128;

	glMaterialf(GL_FRONT, GL_SHININESS, MShininesstwentythree_SSD);


	////////////////////////////////////////////
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(26.5f, 1.5f, 0.0f);
	gluSphere(quadric_SSD[0], 1.0f, 30, 30);

}

void uninitialized_SSD(void)
{
	//code
	if (gbFullscreen_SSD == true)
	{
		dwstyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
	}
	if (wglGetCurrentContext() == ghrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc_SSD)
	{
		wglDeleteContext(ghrc_SSD);
		ghrc_SSD = NULL;
	}
	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf(gbFile_SSD, "log file closed\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}
	if (quadric_SSD)
	{
		for (int i_SSD = 0; i_SSD <= 24; i_SSD++)
		{
			gluDeleteQuadric(quadric_SSD[0]);
			quadric_SSD[0] = NULL;

		}

	}

}






