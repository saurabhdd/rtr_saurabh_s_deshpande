
#include<windows.h>
#include<stdio.h>
#include<math.h>
#include<gl/gl.h>
#include<gl/GLU.h>
#include"Header.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

#define winwidth 800
#define winhight 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD dwStyle_SSD;
WINDOWPLACEMENT wpPrev_SSD;
bool gbFullscreen_SSD = false;
HWND ghwnd_SSD;
HDC ghdc_SSD = NULL;
HGLRC ghrc_SSD;
bool gbActiveWindow_SSD = false;
FILE *gbFile_SSD = NULL;
GLfloat angle_SSD = 0.0f;
GLuint stone_texture;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndClass_SSD;
	MSG msg_SSD;
	HWND hwnd_SSD;
	TCHAR szAppName_SSD[] = TEXT("MyApp");
	bool bDone_SSD = false;


	void Initialize(void);
	void Display(void);

	wpPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	if ((fopen_s(&gbFile_SSD, "log.txt", "w")))
	{
		MessageBox(NULL, TEXT("File not opened"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gbFile_SSD, "log file successfullly created, program started successful\n");
	}

	wndClass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndClass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass_SSD.cbClsExtra = 0;
	wndClass_SSD.cbWndExtra = 0;
	wndClass_SSD.lpfnWndProc = WndProc;
	wndClass_SSD.hInstance = hInstance;
	wndClass_SSD.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass_SSD.lpszClassName = szAppName_SSD;
	wndClass_SSD.lpszMenuName = NULL;
	wndClass_SSD.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndClass_SSD);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		winwidth,
		winhight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;


	Initialize();
	ShowWindow(hwnd_SSD, iCmdShow);
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;
			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				//here should call update function for openGL rendering

				//here should call display function for openGL rendering
				Display();
			}
		}
	}
	return (int)msg_SSD.wParam;

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);


	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;


	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullscreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wpPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullscreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullscreen_SSD = false;
	}

}



void Initialize()
{
	//function declaration
	void Resize(int, int);
	bool LoadGLTexture(GLuint*, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex_SSD;

	//code
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf_s(gbFile_SSD, "choosepixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	ghrc_SSD = wglCreateContext(ghdc_SSD);
	if (ghrc_SSD == NULL)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, ghrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//loading textures
	LoadGLTexture(&stone_texture, MAKEINTRESOURCE(STONE_BITMAP));

	glEnable(GL_TEXTURE_2D);

	//Set clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


	//warm up call resize
	Resize(winwidth, winhight);

}

bool LoadGLTexture(GLuint *texture, TCHAR resourceID[])
{
	//variable declaration
	bool bResult_SSD = false;
	HBITMAP hBitmap_SSD = NULL;
	BITMAP bmp_SSD;

	//code
	hBitmap_SSD = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	/*LR_CREATEDIBSECTION == load resource, create (DIB) device independent bitmap section*/

	if (hBitmap_SSD)
	{
		bResult_SSD = true;
		GetObject(hBitmap_SSD, sizeof(BITMAP), &bmp_SSD);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//following call will push into graphic memory with the help of graphic driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp_SSD.bmWidth, bmp_SSD.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp_SSD.bmBits);

		DeleteObject(hBitmap_SSD);
	}

	return bResult_SSD;
}

void Resize(int width, int height)
{
	if (height <= 0) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void Display(void)
{
	//function declaration
	void Update(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -5.0f);
	glTranslatef(-1.50f, 0.0f, 0.0f);
	glRotatef(angle_SSD, 0.0f, 1.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, stone_texture);

	glBegin(GL_TRIANGLES);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();

	
	Update();

	SwapBuffers(ghdc_SSD);

}

void Update(void)
{
	angle_SSD = angle_SSD + 0.1f;
	if (angle_SSD >= 360.0f)
		angle_SSD = 0.0f;
}
void Uninitialize(void)
{
	//file IO code

	if (gbFullscreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullscreen_SSD = false;
	}

	if (wglGetCurrentContext() == ghrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_SSD)
	{
		wglDeleteContext(ghrc_SSD);
		ghrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}

	glDeleteTextures(1, &stone_texture);

	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}


