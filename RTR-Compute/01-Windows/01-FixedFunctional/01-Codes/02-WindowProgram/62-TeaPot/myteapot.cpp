#include<windows.h>
#include<stdlib.h>
#include<stdio.h>
#include<GL/GL.h>
#include<GL/GLU.h>
#include"Header.h"
#include"TeaPot.h"
#include<math.h>


#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glu32.lib")


#define WINWIDTH 800
#define WINHEIGHT 600
#define PI 3.14

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gbFile_SSD = NULL;

DWORD dwstyle_SSD;
WINDOWPLACEMENT wpPrev_SSD = { sizeof(WINDOWPLACEMENT) };
GLuint teapot_SSD_texture;

bool gbFullscreen_SSD = false;
bool gbActiveWindow_SSD = false;

HWND ghwnd_SSD = NULL;
HDC ghdc_SSD = NULL;
HGLRC ghrc_SSD = NULL;
bool bLight_SSD = false;
bool bAnimation_SSD = false;
bool btexture_SSD = false;



GLUquadric* quadric_SSD = NULL;

GLfloat lAmbient_SSD[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lDiffuse_SSD[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lSpecular_SSD[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lPosition_SSD[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat mAmbient_SSD[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat mDiffuse_SSD[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat mSpecular_SSD[] = { 1.0f,1.0f,1.0f,1.0f };

GLfloat mShinnines_SSD = 50.0f;

float angle_SSD = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	void Initialize(void);
	void Display(void);
	void Update(void);

	WNDCLASSEX wndclass_SSD;
	HWND hwnd_SSD;
	MSG msg_SSD;
	TCHAR szAppName_SSD[] = TEXT("MyApp");

	if (fopen_s(&gbFile_SSD, "logApp.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("File not created"), TEXT("error"), MB_OK);
		return(0);
	}
	else
	{
		fprintf(gbFile_SSD, "log file successfully created\n");
	}
	bool bDone = false;
	int length_SSD;
	int height_SSD;

	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));


	RegisterClassEx(&wndclass_SSD);


	length_SSD = GetSystemMetrics(SM_CXSCREEN) / 2;
	height_SSD = GetSystemMetrics(SM_CYSCREEN) / 2;


	int x_SSD = length_SSD - WINWIDTH / 2;
	int y_SSD = height_SSD - WINHEIGHT / 2;


	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("WINDOW"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, x_SSD, y_SSD, WINWIDTH, WINWIDTH, NULL, NULL, hInstance, NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();

	ShowWindow(hwnd_SSD, iCmdShow);
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);
	while (bDone == false)
	{

		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);

			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
			if (bAnimation_SSD == true)
			{

				Update();

			}
		}


	}
	return((int)msg_SSD.wParam);

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case 'L':
		case 'l':

			if (bLight_SSD == false)
			{
				glEnable(GL_LIGHTING);
				bLight_SSD = true;

			}
			else
			{
				glDisable(GL_LIGHTING);
				bLight_SSD = false;

			}
			break;
		case 'T':
		case 't':

			if (btexture_SSD == false)
			{
				glEnable(GL_TEXTURE_2D);
				btexture_SSD = true;

			}
			else
			{
				glDisable(GL_TEXTURE_2D);
				btexture_SSD = false;

			}
			break;
		case 'A':
		case 'a':

			if (bAnimation_SSD == false)
			{

				bAnimation_SSD = true;

			}
			else
			{

				bAnimation_SSD = false;

			}
			break;
		default:
			break;
		}
		break;


	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CREATE:

		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{

	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };

	if (gbFullscreen_SSD == false)
	{
		dwstyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);

		if (dwstyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wpPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}

		}
		ShowCursor(false);
		gbFullscreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
		gbFullscreen_SSD = false;
	}

}

void Initialize(void)
{

	void Resize(int, int);
	bool loadTexture(GLuint*, TCHAR[]);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex_SSD;
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;


	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd);

	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf(gbFile_SSD, "choosepixelformat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd) == FALSE)
	{
		fprintf(gbFile_SSD, "Setformat()\n");
		DestroyWindow(ghwnd_SSD);
	}


	ghrc_SSD = wglCreateContext(ghdc_SSD);
	if (ghrc_SSD == NULL)
	{
		fprintf(gbFile_SSD, "wglcreatecontext() fail\n");
		DestroyWindow(ghwnd_SSD);
	}


	if (wglMakeCurrent(ghdc_SSD, ghrc_SSD) == FALSE)
	{
		fprintf(gbFile_SSD, "wglmakecurrent() fail\n");
		DestroyWindow(ghwnd_SSD);
	}

	loadTexture(&teapot_SSD_texture, MAKEINTRESOURCE(MYBITMAP));

	glLightfv(GL_LIGHT0, GL_AMBIENT, lAmbient_SSD);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lDiffuse_SSD);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lSpecular_SSD);
	glLightfv(GL_LIGHT0, GL_POSITION, lPosition_SSD);

	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT, GL_AMBIENT, mAmbient_SSD);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mDiffuse_SSD);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mSpecular_SSD);
	glMaterialf(GL_FRONT, GL_SHININESS, mShinnines_SSD);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(WINWIDTH, WINWIDTH);

}


void Resize(int width_SSD, int height_SSD)
{


	if (height_SSD == 0)
		height_SSD = 1;

	glViewport(0, 0, (GLsizei)width_SSD, GLsizei(height_SSD));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width_SSD / (GLfloat)height_SSD), 0.1f, 100.0f);



}


bool loadTexture(GLuint* texture_SSD, TCHAR ResourceID_SSD[])
{
	HBITMAP hbitmap_SSD = NULL;
	BITMAP bmp_SSD;
	bool bResult_SSD = false;

	hbitmap_SSD = (HBITMAP)LoadImage(GetModuleHandle(NULL), ResourceID_SSD, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	fprintf(gbFile_SSD, "error=%d\n", GetLastError());
	if (hbitmap_SSD)
	{
		fprintf(gbFile_SSD, "load my  stone texture image sucessfully\n");
		GetObject(hbitmap_SSD, sizeof(BITMAP), &bmp_SSD);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture_SSD);
		glBindTexture(GL_TEXTURE_2D, *texture_SSD);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp_SSD.bmWidth, bmp_SSD.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp_SSD.bmBits);
		glBindTexture(GL_TEXTURE_2D, 0);
		DeleteObject(hbitmap_SSD);
		bResult_SSD = true;
	}
	return(bResult_SSD);



}

void Display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -1.0f);
	glRotatef(angle_SSD, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, teapot_SSD_texture);

	glBegin(GL_TRIANGLES);
	for (int i_SSD = 0; i_SSD < sizeof(face_indicies) / sizeof(face_indicies[0]); i_SSD++)
	{

		for (int j_SSD = 0; j_SSD < 3; j_SSD++)
		{

			int vi_SSD = face_indicies[i_SSD][j_SSD];
			int ni_SSD = face_indicies[i_SSD][j_SSD] + 3;
			int ti_SSD = face_indicies[i_SSD][j_SSD] + 6;

			glNormal3f(normals[ni_SSD][0], normals[ni_SSD][1], normals[ni_SSD][2]);
			glTexCoord2f(textures[ti_SSD][0], textures[ti_SSD][1]);
			glVertex3f(vertices[vi_SSD][0], vertices[vi_SSD][1], vertices[vi_SSD][2]);
		}

	}
	glEnd();

	SwapBuffers(ghdc_SSD);

}
void Update(void)
{

	angle_SSD = angle_SSD + 0.1f;
	if (angle_SSD >= 360.0)
		angle_SSD = 0.0f;

}
void Uninitialize(void)
{

	if (gbFullscreen_SSD == true)
	{
		dwstyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwstyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wpPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
	}
	if (wglGetCurrentContext() == ghrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc_SSD)
	{
		wglDeleteContext(ghrc_SSD);
		ghrc_SSD = NULL;
	}
	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf(gbFile_SSD, "log file closed \n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

	if (teapot_SSD_texture)
	{
		glDeleteTextures(1, &teapot_SSD_texture);


	}


}






