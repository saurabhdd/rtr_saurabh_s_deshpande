
#include<windows.h>
#include<stdio.h>
#include<math.h>
#include<gl/gl.h>
#include<gl/GLU.h>
#include"Header.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

#define GL_PI 3.14159
#define WINWIDTH 800
#define WINHEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;
GLfloat angle_SSD = 0.0f;

bool bLight_SSD = false;

GLfloat lAmbient_SSD[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat lDiffuse_SSD[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lPosition_SSD[] = { 0.0f,0.0f,2.0f,1.0f };

GLUquadric* quadric_SSD = NULL;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("practice"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':

			if (bLight_SSD == true)
			{
				bLight_SSD = false;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight_SSD = true;
				glDisable(GL_LIGHTING);
			}
			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}



void Initialize()
{
	//function declaration
	void Resize(int, int);
	bool LoadTexture(GLuint*, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex_SSD;

	//code
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf_s(gbFile_SSD, "choosepixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (hglrc_SSD == NULL)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	glLightfv(GL_LIGHT1, GL_AMBIENT, lAmbient_SSD);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lDiffuse_SSD);
	glLightfv(GL_LIGHT1, GL_POSITION, lPosition_SSD);
	glEnable(GL_LIGHT1);


	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	//glShadeModel(GL_FLAT);
	//glFrontFace(GL_CW);

	Resize(WINWIDTH, WINHEIGHT);

}

void Resize(int width, int height)
{
	if (height <= 0) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//variable declaration
	static GLfloat q_angel_SSD = 0.0f;
	glTranslatef(0.0f, 0.0f, -12.0f);
	glRotatef(q_angel_SSD, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	glNormal3f(0.0f, 0.0f, 1.0f);   
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glNormal3f(0.0f, 0.0f, -1.0f);   
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glNormal3f(-1.0f, 0.0f, 0.0f);  
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glNormal3f(0.0f, 1.0f, 0.0f);    
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glNormal3f(0.0f, -1.0f, 0.0f);    
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();

	q_angel_SSD = q_angel_SSD + 0.1f;

	SwapBuffers(ghdc_SSD);
}


bool LoadTexture(GLuint* texture, TCHAR ResourceID[])
{
	HBITMAP hbitmap_SSD = NULL;
	BITMAP bmp_SSD; 
	bool bResult_SSD = false;
	fprintf(gbFile_SSD, "load my smiley texture image\n");
	hbitmap_SSD = (HBITMAP)LoadImage(GetModuleHandle(NULL), ResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	fprintf(gbFile_SSD, "error=%d\n", GetLastError());
	if (hbitmap_SSD)
	{
		fprintf(gbFile_SSD, "load my smiley texture image sucessfully\n");
		GetObject(hbitmap_SSD, sizeof(BITMAP), &bmp_SSD);
		//from here actual texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp_SSD.bmWidth, bmp_SSD.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp_SSD.bmBits);
		
		DeleteObject(hbitmap_SSD);
		bResult_SSD = true;
	}
	return(bResult_SSD);

}


void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

	

}






