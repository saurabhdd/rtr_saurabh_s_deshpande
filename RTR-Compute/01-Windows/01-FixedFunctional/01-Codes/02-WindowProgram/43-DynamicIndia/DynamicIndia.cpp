
#include<windows.h>
#include<stdio.h>
#include<math.h>
#include<gl/gl.h>
#include<gl/GLU.h>
#include"Header.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

#define GL_PI 3.14159
#define WINWIDTH 800
#define WINHEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;
GLfloat angle_SSD = 0.0f;
GLfloat X = 0.0f;
GLfloat Y = 0.0f;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("practice"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	PlaySound(MAKEINTRESOURCE(DULHANCHALI), hInstance, SND_RESOURCE | SND_ASYNC);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}



void Initialize()
{
	//function declaration
	void Resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex_SSD;

	//code
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf_s(gbFile_SSD, "choosepixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixel()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (hglrc_SSD == NULL)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent()  failed\n");
		DestroyWindow(ghwnd_SSD);
	}


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);
	//glShadeModel(GL_FLAT);
	//glFrontFace(GL_CW);

	Resize(WINWIDTH, WINHEIGHT);

}


void Resize(int width, int height)
{
	if (height <= 0) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{
	//function declaration
	void DrawPlane(GLfloat, GLfloat, GLfloat, GLfloat);
	void PlaneAnimation(GLfloat*, bool*);

	//variable declaration
	static GLfloat xTransIL_SSD = -3.0f;
	static GLfloat yTransN_SSD = 2.50f;
	static GLfloat xTransIR_SSD = 3.30f;
	static GLfloat yTransA_SSD = -2.40f;
	static GLfloat DWrgb_SSD = 0.0f;
	static GLfloat DORc_SSD = 0.0f;
	static GLfloat DOGc_SSD = 0.0f;
	static GLfloat Intermediate1_SSD = 0.0f;
	static GLfloat Intermediate2_SSD = 0.0f;
	static GLfloat Intermediate3_SSD = 0.0f;
	static GLfloat ACenterTop_SSD = 0.38f;
	static GLfloat ACenterMid_SSD = 0.35f;
	static GLfloat ACenterBottom_SSD = 0.32f;

	static bool ILComplete_SSD = false;
	static bool NComplete_SSD = false;
	static bool DComplete_SSD = false;
	static bool IRComplete_SSD = false;
	static bool AComplete_SSD = false;
	static bool startMidFlag_SSD = false;

	static GLfloat xCenter_SSD = -18.50f;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glShadeModel(GL_SMOOTH);
	glColor3f(1.0f, 1.0f, 1.0f);



	//I
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glScalef(0.5f, 0.8f, 0.0f);
	glTranslatef(xTransIL_SSD, 0.0f, 0.0f);
	glTranslatef(-1.7f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.50f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 0.50f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.5f, 0.0f);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);

	glEnd();

	if (xTransIL_SSD < 0.0f)
		xTransIL_SSD += 0.00018f;
	else
	{
		xTransIL_SSD = 0.0f;
		ILComplete_SSD = true;
	}


	//N**************************************************************

	if (AComplete_SSD)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.20f, 0.0f, -3.0f);
		glScalef(0.5f, 0.8f, 0.0f);
		glTranslatef(0.0f, yTransN_SSD, 0.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.50f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.2f, 0.50f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glEnd();

		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.3f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.2f, 0.5f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.3f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.6f, -0.5f, 0.0f);
		glVertex3f(0.8f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);

		glEnd();

		//glTranslatef(0.6f, 0.0f, 0.0f);

		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.60f, 0.50f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.60f, 0.0f, 0.0f);
		glVertex3f(0.8f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.8f, 0.50f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.60f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.60f, -0.5f, 0.0f);
		glVertex3f(0.8f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.8f, 0.0f, 0.0f);

		glEnd();

		if (yTransN_SSD > 0.0f)
			yTransN_SSD -= 0.00018;
		else
		{
			yTransN_SSD = 0.0f;
			NComplete_SSD = true;
		}
	}

	//D****************************************************************

	if (IRComplete_SSD)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.250f, 0.0f, -3.0f);
		glScalef(0.5f, 0.8f, 0.0f);
		glTranslatef(0.0f, 0.0f, 0.0f);
		glBegin(GL_QUADS);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.0f, 0.50f, 0.0f);
		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.2f, 0.50f, 0.0f);

		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(0.0f, DWrgb_SSD, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glEnd();
		//glTranslatef(0.18f, 0.0f, 0.0f);

		glBegin(GL_QUADS);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);
		glColor3f(DWrgb_SSD, Intermediate1_SSD, Intermediate2_SSD); //0.6, 0.4
		glVertex3f(0.0f, 0.3f, 0.0f);
		glVertex3f(0.6f, 0.3f, 0.0f);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.6f, 0.5f, 0.0f);
		glColor3f(DWrgb_SSD, Intermediate1_SSD, Intermediate2_SSD); //0.6, 0.4
		glVertex3f(0.6f, 0.3f, 0.0f);
		glVertex3f(0.8f, 0.3f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glColor3f(0.0f, DWrgb_SSD, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glColor3f(Intermediate3_SSD, DWrgb_SSD, Intermediate3_SSD); //0.3, 1.0, 0.3
		glVertex3f(0.0f, -0.3f, 0.0f);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glColor3f(0.0f, DWrgb_SSD, 0.0f);
		glVertex3f(0.6f, -0.5f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.0f, DWrgb_SSD, 0.0f);
		glVertex3f(0.6f, -0.5f, 0.0f);
		glColor3f(Intermediate3_SSD, DWrgb_SSD, Intermediate3_SSD); //0.3, 0.3
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.8f, -0.3f, 0.0f);
		glEnd();

		//glTranslatef(0.6f, 0.0f, 0.0f);
		glBegin(GL_QUADS);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.60f, 0.30f, 0.0f);
		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.6f, 0.0f, 0.0f);
		glVertex3f(0.8f, 0.0f, 0.0f);
		glColor3f(DORc_SSD, DOGc_SSD, 0.0f);
		glVertex3f(0.8f, 0.30f, 0.0f);

		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.60f, 0.0f, 0.0f);
		glColor3f(0.0f, DWrgb_SSD, 0.0f);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.8f, -0.3f, 0.0f);
		glColor3f(DWrgb_SSD, DWrgb_SSD, DWrgb_SSD);
		glVertex3f(0.8f, 0.0f, 0.0f);

		glEnd();

		if (DWrgb_SSD < 1.0f)
			DWrgb_SSD += 0.000061f;
		if (DORc_SSD < 1.0f)
			DORc_SSD += 0.000061f;
		if (DOGc_SSD < 0.5f)
			DOGc_SSD += (0.000061f / (1.0f / 0.5f));
		if (Intermediate1_SSD < 0.6f)
			Intermediate1_SSD += (0.000061f / (1.0f / 0.6f));
		if (Intermediate2_SSD < 0.4f)
			Intermediate2_SSD += (0.000061f / (1.0f / 0.4f));
		if (Intermediate3_SSD < 0.3f)
			Intermediate3_SSD += (0.000061f / (1.0f / 0.3f));

		if (DWrgb_SSD >= 1.0f)
			DComplete_SSD = true;

	}

	//I**********************************************************************

	if (NComplete_SSD)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.3f, 0.0f, -3.0f);
		glScalef(0.5f, 0.8f, 0.0f);
		glTranslatef(0.0f, yTransA_SSD, 0.0f);
		glTranslatef(1.0f, 0.0f, 0.0f);

		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.50f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.2f, 0.50f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glEnd();

		if (yTransA_SSD < 0.0f)
			yTransA_SSD += 0.00018;
		else
		{
			yTransA_SSD = 0.0f;
			IRComplete_SSD = true;
		}


	}



	//A***************************************************************

	if (ILComplete_SSD)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.35f, 0.0f, -3.0f);
		glScalef(0.5f, 0.8f, 0.0f);
		glTranslatef(xTransIR_SSD, 0.0f, 0.0f);
		glTranslatef(1.4f, 0.0f, 0.0f);

		glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.15f, 0.0f, 0.0f);
		glVertex3f(0.35f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.15f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.5f, 0.0f);
		glVertex3f(0.5f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.35f, 0.0f, 0.0f);



		glEnd();

		if (startMidFlag_SSD)
		{
			if (ACenterTop_SSD <= 0.42f)
				ACenterTop_SSD += 0.000091f;
			else
				ACenterTop_SSD = 0.42f;

			if (ACenterMid_SSD <= 0.45f)
				ACenterMid_SSD += 0.000091f;
			else
				ACenterMid_SSD = 0.45f;

			if (ACenterBottom_SSD <= 0.48f)
				ACenterBottom_SSD += 0.000091f;
			else
				ACenterBottom_SSD = 0.48f;

			glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.38f, 0.065f, 0.0f);
			glVertex3f(ACenterTop_SSD, 0.065f, 0.0f);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(ACenterMid_SSD, 0.02f, 0.0f);
			glVertex3f(0.35f, 0.02f, 0.0f);

			glVertex3f(0.35f, 0.02f, 0.0f);
			glVertex3f(ACenterMid_SSD, 0.02f, 0.0f);
			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(ACenterBottom_SSD, -0.04f, 0.0f);
			glVertex3f(0.32f, -0.04f, 0.0f);

			glEnd();


		}

		//glTranslatef(0.6f, 0.0f, 0.0f);

		glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.60f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.45, 0.0f, 0.0f);
		glVertex3f(0.65f, 0.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.8f, -0.5f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.45f, 0.0f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.5f, 0.0f);
		glVertex3f(0.5f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.65f, 0.0f, 0.0f);


		glEnd();

		if (xTransIR_SSD > 0.0f)
			xTransIR_SSD -= 0.00018f;
		else
		{
			xTransIR_SSD = 0.0f;
			AComplete_SSD = true;
		}


	}


	//DrawPlane()
	if (DComplete_SSD)
		PlaneAnimation(&xCenter_SSD, &startMidFlag_SSD);



	SwapBuffers(ghdc_SSD);   //

}

void PlaneAnimation(GLfloat* xCenter, bool *startMidFlag)
{
	//function declaration
	void DrawPlane(GLfloat, GLfloat, GLint, GLfloat, bool, bool*, bool*);

	//variable declaration
	static GLfloat xTrans_SSD = -30.5f;
	static GLfloat yTrans_SSD = 3.50f;
	static GLfloat alpha_SSD = 0.0f;
	static GLfloat yRot_SSD = 0.0f;
	static GLfloat beta_SSD = 0.0f;
	static bool planeStop_SSD = false;
	static bool smokeComplete_SSD = false;


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	//planes animation
	alpha_SSD = alpha_SSD + (GL_PI / 20000);

	if (xTrans_SSD < -7.5f)
	{
		xTrans_SSD = 9.50f * cos(alpha_SSD) * (-1.0f) - 7.5f;
		yTrans_SSD = 9.50f * sin(alpha_SSD) * (-1.0f) + 9.50f;
		yRot_SSD = ((GL_PI / 2) - alpha_SSD) * -1.0f;
	}
	else
	{
		if (xTrans_SSD < 7.5f)
			xTrans_SSD += 0.001;

		if (xTrans_SSD >= 7.5f)
		{


			beta_SSD = beta_SSD + (GL_PI / 20000);
			xTrans_SSD = 9.50f * sin(beta_SSD) * (1.0f) + 7.5f;
			yTrans_SSD = 9.50f * cos(beta_SSD) * (-1.0f) + 9.50f;
			yRot_SSD = beta_SSD;


		}

		if (xTrans_SSD >= -4.5f)
			planeStop_SSD = true;
	}




	DrawPlane(xTrans_SSD, yTrans_SSD, 0, (yRot_SSD * 180 / GL_PI), planeStop_SSD, &smokeComplete_SSD, startMidFlag);
	DrawPlane(xTrans_SSD, -yTrans_SSD, 2, (yRot_SSD * 180 / GL_PI) * -1.0f, planeStop_SSD, &smokeComplete_SSD, startMidFlag);
	DrawPlane(*xCenter, 0.0f, 1, 0.0f, planeStop_SSD, &smokeComplete_SSD, startMidFlag);
	if (*xCenter < -7.5f)
	{
		*xCenter += 0.0011f;

	}
	else if (*xCenter >= 7.5f)
	{

		*xCenter += 0.0011f;

	}
	else
		*xCenter += 0.001f;

	if (*xCenter >= -4.5f)
		planeStop_SSD = true;

	//fprintf_s(gbFile_SSD, "/n/n xTrans ***%f******/n", *xCenter);
}

void DrawPlane(GLfloat xTrans, GLfloat yTrans, GLint smokeColor, GLfloat yRot, bool planeStop, bool *smokeComplete, bool *startMidFlag)
{
	//function declaration

	//variable declaration
	GLfloat alpha_SSD = 0.0f;
	GLfloat x_SSD, x__SSD, y_SSD, y__SSD, radius_SSD;
	static GLfloat smokeLength_SSD = -15.8f;
	static GLfloat smokeR_SSD;
	static GLfloat smokeG_SSD;
	static GLfloat smokeB_SSD;

	//glClear(GL_COLOR_BUFFER_BIT);
	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);


	glScalef(0.12f, 0.12f, 0.12f);
	glTranslatef(xTrans, yTrans, 0.0f);
	glRotatef(yRot, 0.0f, 0.0f, 1.0f);




	glColor3f(0.0941f, 0.5451f, 0.7608f);

	//main shank
	glBegin(GL_QUADS);
	glVertex3f(2.5f, 0.4f, 0.0f);
	glVertex3f(2.5f, -0.4f, 0.0f);
	glVertex3f(-0.1f, -0.4f, 0.0f);
	glVertex3f(-0.1f, 0.4f, 0.0f);

	glEnd();




	//triangle
	glBegin(GL_TRIANGLES);

	glVertex3f(2.5f, 0.4f, 0.0f);
	glVertex3f(2.5f, -0.4f, 0.0f);
	glVertex3f(3.5f, 0.0f, 0.0f);
	glEnd();

	radius_SSD = 0.12f;

	//wings
	glBegin(GL_TRIANGLES);
	glVertex3f(1.8f, 0.4f, 0.0f);
	glVertex3f(0.2f, 0.4f, 0.0f);
	glVertex3f(0.2f, 1.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(1.8f, -0.4f, 0.0f);
	glVertex3f(0.2f, -0.4f, 0.0f);
	glVertex3f(0.2f, -1.7f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.2f, -1.7f, 0.0f);
	glVertex3f(0.75f, -1.7f, 0.0f);
	glVertex3f(0.75f, -1.5f, 0.0f);
	glVertex3f(0.2f, -1.5f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.2f, 1.7f, 0.0f);
	glVertex3f(0.75f, 1.7f, 0.0f);
	glVertex3f(0.75f, 1.5f, 0.0f);
	glVertex3f(0.2f, 1.5f, 0.0f);
	glEnd();

	//side curve top
	for (alpha_SSD = GL_PI / 2; alpha_SSD <= (GL_PI); alpha_SSD = alpha_SSD + ((GL_PI / 2) / 50))
	{
		x_SSD = (radius_SSD * sin(alpha_SSD) + 0.1) * -1.0f;
		y_SSD = (radius_SSD * cos(alpha_SSD) + 0.52);
		x__SSD = (radius_SSD * sin(alpha_SSD + ((GL_PI / 2) / 50)) + 0.1) * -1.0f;
		y__SSD = (radius_SSD * cos(alpha_SSD + ((GL_PI / 2) / 50)) + 0.52);

		glBegin(GL_QUADS);
		glVertex3f(x_SSD, y_SSD, 0.0f);
		glVertex3f(x_SSD, 0.0f, 0.0f);
		glVertex3f(x__SSD, 0.0f, 0.0f);
		glVertex3f(x__SSD, y__SSD, 0.0f);
		glEnd();

	}

	//side curve bottom
	for (alpha_SSD = GL_PI / 2; alpha_SSD <= (GL_PI); alpha_SSD = alpha_SSD + ((GL_PI / 2) / 50))
	{
		x_SSD = (radius_SSD * sin(alpha_SSD) + 0.1) * -1.0f;
		y_SSD = (radius_SSD * cos(alpha_SSD) + 0.52) * -1.0f;
		x__SSD = (radius_SSD * sin(alpha_SSD + ((GL_PI / 2) / 50)) + 0.1) * -1.0f;
		y__SSD = (radius_SSD * cos(alpha_SSD + ((GL_PI / 2) / 50)) + 0.52) * -1.0f;

		glBegin(GL_QUADS);
		glVertex3f(x_SSD, y_SSD, 0.0f);
		glVertex3f(x_SSD, 0.0f, 0.0f);
		glVertex3f(x__SSD, 0.0f, 0.0f);
		glVertex3f(x__SSD, y__SSD, 0.0f);
		glEnd();

	}
	//upside flange(0.2 + 0.12)
	glBegin(GL_QUADS);
	glVertex3f(-0.22f, 0.0f, 0.0f);
	glVertex3f(-0.22f, 0.72f, 0.0f);
	glVertex3f(-0.42f, 0.72f, 0.0f);
	glVertex3f(-0.42f, 0.0f, 0.0f);
	glEnd();

	//downside flage
	glBegin(GL_QUADS);
	glVertex3f(-0.22f, 0.0f, 0.0f);
	glVertex3f(-0.22f, -0.72f, 0.0f);
	glVertex3f(-0.42f, -0.72f, 0.0f);
	glVertex3f(-0.42f, 0.0f, 0.0f);
	glEnd();

	//side flange curve top
	radius_SSD = 0.2f;

	for (alpha_SSD = (GL_PI / 2); alpha_SSD <= (GL_PI); alpha_SSD = alpha_SSD + ((GL_PI / 2) / 50))
	{
		x_SSD = (radius_SSD * cos(alpha_SSD) + 0.42f) * -1.0f;
		y_SSD = (radius_SSD * sin(alpha_SSD) + 0.72f);
		x__SSD = (radius_SSD * cos(alpha_SSD + ((GL_PI / 2) / 50)) + 0.42f) * -1.0f;
		y__SSD = radius_SSD * sin(alpha_SSD + ((GL_PI / 2) / 50)) + 0.72f;

		glBegin(GL_QUADS);
		glVertex3f(x_SSD, y_SSD, 0.0f);
		glVertex3f(x_SSD, 0.72f, 0.0f);
		glVertex3f(x__SSD, 0.72f, 0.0f);
		glVertex3f(x__SSD, y__SSD, 0.0f);
		glEnd();

	}

	//side curve bottom
	radius_SSD = 0.2f;

	for (alpha_SSD = (GL_PI / 2); alpha_SSD <= (GL_PI); alpha_SSD = alpha_SSD + ((GL_PI / 2) / 50))
	{
		x_SSD = (radius_SSD * cos(alpha_SSD) + 0.42f) * -1.0f;
		y_SSD = (radius_SSD * sin(alpha_SSD) + 0.72f) * -1.0f;
		x__SSD = (radius_SSD * cos(alpha_SSD + ((GL_PI / 2) / 50)) + 0.42f) * -1.0f;
		y__SSD = (radius_SSD * sin(alpha_SSD + ((GL_PI / 2) / 50)) + 0.72f) * -1.0f;

		glBegin(GL_QUADS);
		glVertex3f(x_SSD, y_SSD, 0.0f);
		glVertex3f(x_SSD, 0.72f, 0.0f);
		glVertex3f(x__SSD, 0.72f, 0.0f);
		glVertex3f(x__SSD, y__SSD, 0.0f);
		glEnd();

	}

	//shank support
	glBegin(GL_QUADS);
	glVertex3f(-0.42f, 0.92f, 0.0f);
	glVertex3f(-0.42f, -0.92f, 0.0f);
	glVertex3f(-0.62f, -0.96f, 0.0f);
	glVertex3f(-0.62f, 0.96f, 0.0f);
	glEnd();

	//end support
	glBegin(GL_QUADS);
	glVertex3f(-0.62f, 0.55f, 0.0f);
	glVertex3f(-0.62f, -0.55f, 0.0f);
	glVertex3f(-0.82f, -0.6f, 0.0f);
	glVertex3f(-0.82f, 0.6f, 0.0f);
	glEnd();


	//IAF
	glLineWidth(2.0f);
	glColor3f(0.90f, 0.4f, 0.09f);

	glBegin(GL_LINES);
	glVertex3f(1.2f, 0.16f, 0.0f);
	glVertex3f(1.2f, -0.16f, 0.0f);
	glVertex3f(1.35f, 0.16f, 0.0f);
	glVertex3f(1.35f, -0.16f, 0.0f);
	glVertex3f(1.5f, 0.16f, 0.0f);
	glVertex3f(1.5f, -0.16f, 0.0f);
	glVertex3f(1.35f, 0.16f, 0.0f);
	glVertex3f(1.5f, 0.16f, 0.0f);
	glVertex3f(1.35f, -0.08f, 0.0f);
	glVertex3f(1.5f, -0.08f, 0.0f);
	glVertex3f(1.65f, 0.16f, 0.0f);
	glVertex3f(1.65f, -0.16f, 0.0f);
	glVertex3f(1.65f, 0.16f, 0.0f);
	glVertex3f(1.8f, 0.16f, 0.0f);
	glVertex3f(1.65f, 0.0f, 0.0f);
	glVertex3f(1.8f, 0.0f, 0.0f);

	glEnd();

	//smoke

	if (smokeLength_SSD >= -3.7f)
		*startMidFlag = true;

	if (planeStop == true)
	{
		if (smokeLength_SSD <= -0.81f)
			smokeLength_SSD += 0.000351f;
		else
			*smokeComplete = true;

		fprintf_s(gbFile_SSD, "/n ***%f**SmokeLength", smokeLength_SSD);

	}

	if (smokeColor == 0)
	{
		smokeR_SSD = 1.0f;
		smokeG_SSD = 0.50f;
		smokeB_SSD = 0.0f;
	}

	if (smokeColor == 1)
	{
		smokeR_SSD = 1.0f;
		smokeG_SSD = 1.0f;
		smokeB_SSD = 1.0f;
	}

	if (smokeColor == 2)
	{
		smokeR_SSD = 0.0f;
		smokeG_SSD = 1.0f;
		smokeB_SSD = 0.0f;
	}

	glBegin(GL_QUADS);
	glColor3f(smokeR_SSD, smokeG_SSD, smokeB_SSD);
	glVertex3f(-0.82f, 0.3f, 0.0f);
	glVertex3f(-0.82f, -0.3f, 0.0f);
	glColor3f(0.2f *smokeR_SSD, 0.1f * smokeG_SSD, 0.1f *smokeB_SSD);
	glVertex3f(smokeLength_SSD, -0.3f, 0.0f);
	glVertex3f(smokeLength_SSD, 0.3f, 0.0f);

	/*glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.82f, 0.075f, 0.0f);
	glVertex3f(-0.82f, -0.07f, 0.0f);
	glColor3f(0.1f, 0.1f, 0.1f);
	glVertex3f(smokeLength, -0.075f, 0.0f);
	glVertex3f(smokeLength, 0.075f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.82f, -0.15f, 0.0f);
	glVertex3f(-0.82f, -0.3f, 0.0f);
	glColor3f(0.0f, 0.1f, 0.0f);
	glVertex3f(smokeLength, -0.3f, 0.0f);
	glVertex3f(smokeLength, -0.15f, 0.0f);
	*/

	glEnd();


}

void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}






