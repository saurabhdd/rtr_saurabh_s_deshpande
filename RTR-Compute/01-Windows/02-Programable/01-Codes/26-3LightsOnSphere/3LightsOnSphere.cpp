#include<windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<gl/gl.h>
#include"Header.h"
#include"vmath.h"
#include"Sphere.h"
#include<math.h>

#define WINWIDTH 800
#define WINHEIGHT 600
#define GL_PI 3.1415

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Sphere.lib")

#define GL_PER_VERTEX 0
#define GL_PER_FRAGMENT 1

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;

//shader variables
GLuint vertexShaderObject_v;
GLuint fragmentShaderObject_v;
GLuint shaderProgramObject_v;
GLuint vertexShaderObject_f;
GLuint fragmentShaderObject_f;
GLuint shaderProgramObject_f;

using namespace vmath;

enum
{
	SSD_ATTRIBUTE_POSITION = 0,
	SSD_ATTRIBUTE_COLOR,
	SSD_ATTRIBUTE_NORMAL,
	SSD_ATTRIBUTE_TEXTURE,
};

GLuint vao;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;

//matrix uniforms
GLuint projectionMatrixUniform;
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;

//light uniforms
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint laUniform_f;
GLuint ldUniform_f;
GLuint lsUniform_f;

//material uniform
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;

//shininess uniform
GLuint shininessUniform;

//lightPosition Uniform
GLuint lightPositionUniform;
GLuint lightPositionUniform_f;

//key pressed uniform
GLuint lKeyPressedUniform;


//material arrays
GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat shininess = 128;

bool bAnimate;
bool bLighting;

mat4 perspectiveProjectionMatrix;

GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numSphereVertices;
GLuint numSphereElements;

GLint toggle_shader;

typedef struct __Light {

	vec3 ambientLightColor;
	vec3 diffuseLightColor;
	vec3 specularLightColor;
	vec4 lightPosition;

}Light;

Light light[3];

GLfloat xRotate = 0.0f;
GLfloat yRotate = 0.0f;
GLfloat zRotate = 0.0f;

GLfloat xRotate_r = 0.0f;
GLfloat yRotate_r = 0.0f;
GLfloat zRotate_r = 0.0f;

GLfloat radius = 400.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName_SSD,
		TEXT("practice"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			ToggleFullScreen();
			break;

		case 0x46:
		case 0x66:
			if (toggle_shader == GL_PER_VERTEX)
				toggle_shader = GL_PER_FRAGMENT;
			else
				toggle_shader = GL_PER_VERTEX;
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'A':
		case 'a':
			if (bAnimate == false)
				bAnimate = true;
			else
				bAnimate = false;
			break;

		case 'l':
		case 'L':
			if (bLighting == false)
				bLighting = true;
			else
				bLighting = false;
			break;

		case 'Q':
		case 'q':
			DestroyWindow(ghwnd_SSD);

		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}

void Initialize(void)
{

	//function declaration
	void Resize(int, int);
	void ToggleShader(void);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex_SSD;

	ghdc_SSD = GetDC(ghwnd_SSD);

	//initalizing pixelformatdescriptor
	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf_s(gbFile_SSD, "ChoosePixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (!hglrc_SSD)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}

	//OpenGL related logs
	fprintf_s(gbFile_SSD, "OpenGL vendor: %s\n", glGetString(GL_VENDOR));
	fprintf_s(gbFile_SSD, "OpenGL renderer: %s\n", glGetString(GL_RENDER));
	fprintf_s(gbFile_SSD, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf_s(gbFile_SSD, "Graphic library shading language version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf_s(gbFile_SSD, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//************shaders***********
	//**vertex Shader**

	//create shader
	vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);

	//provice source code to shader
	const GLchar* vertexShaderSource_v =
		"# version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec3 u_la[3];" \
		"uniform vec3 u_ld[3];" \
		"uniform vec3 u_ls[3];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform int u_l_key_pressed;" \
		"uniform float u_shininess;" \
		"uniform vec4 u_light_position[3];" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"fong_ads_light = vec3(0.0, 0.0, 0.0);" \
		"vec4 eye_cordinate;" \
		"vec3 light_direction[3];" \
		"vec3 reflection_vector[3];" \
		"vec3 view_vector;" \
		"vec3 transformed_normal;" \
		"vec3 ambient_light[3];" \
		"vec3 diffuse_light[3];" \
		"vec3 specular_light[3];" \
		"int i;" \
		"if(u_l_key_pressed == 1)" \
		"{" \
		"eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"view_vector = normalize(vec3(-eye_cordinate));" \
		"for(i = 0; i < 3; i++)" \
		"{" \
		"light_direction[i] = normalize(vec3(u_light_position[i] - eye_cordinate));" \
		"reflection_vector[i] = reflect(-light_direction[i], transformed_normal);" \
		"ambient_light[i] = u_la[i] * u_ka;" \
		"diffuse_light[i] = u_ld[i] * u_kd * max(dot(light_direction[i], transformed_normal), 0.0);" \
		"specular_light[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i], view_vector), 0.0), u_shininess);" \
		"fong_ads_light = fong_ads_light + ambient_light[i] + diffuse_light[i] + specular_light[i];" \
		"}" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";



	glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSource_v, NULL);

	//compile shader
	glCompileShader(vertexShaderObject_v);

	GLint vertexInfoLogLength = 0;
	GLint vertexShaderCompileStatus = 0;
	GLchar* vertexInfoLog = NULL;

	glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &vertexShaderCompileStatus);

	if (vertexShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &vertexInfoLogLength);

		if (vertexInfoLogLength > 0)
		{
			vertexInfoLog = (GLchar*)malloc(vertexInfoLogLength);

			if (vertexInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_v, vertexInfoLogLength, &written, vertexInfoLog);
				fprintf_s(gbFile_SSD, "Vertex shader compile log per vertex: %s\n", vertexInfoLog);
				free(vertexInfoLog);

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//per fragment vertex shader
	vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);

	//provice source code to shader
	const GLchar* vertexShaderSource_f =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_l_key_pressed;" \
		"uniform vec4 u_light_position[3];" \
		"out vec4 eye_cordinate;" \
		"out vec3 trasformed_normal;" \
		"out vec3 light_direction[3];" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"int i;" \
		"if(u_l_key_pressed == 1)" \
		"{" \
		"eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"trasformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"for(i = 0; i < 3; i++)" \
		"{" \
		"light_direction[i] = vec3(eye_cordinate - u_light_position[i]);" \
		"}" \
		"view_vector = vec3(-eye_cordinate);" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(vertexShaderObject_f, 1, (const GLchar**)&vertexShaderSource_f, NULL);

	//compile shader
	glCompileShader(vertexShaderObject_f);

	vertexInfoLogLength = 0;
	vertexShaderCompileStatus = 0;
	vertexInfoLog = NULL;

	glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &vertexShaderCompileStatus);

	if (vertexShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &vertexInfoLogLength);

		if (vertexInfoLogLength > 0)
		{
			vertexInfoLog = (GLchar*)malloc(vertexInfoLogLength);

			if (vertexInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_f, vertexInfoLogLength, &written, vertexInfoLog);
				fprintf_s(gbFile_SSD, "Vertex shader compile log per fragment: %s\n", vertexInfoLog);
				free(vertexInfoLog);

				DestroyWindow(ghwnd_SSD);
			}
		}
	}


	//***fragment shader***
	//create Shader
	fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

	//shader source
	const GLchar* fragmentShaderSource_v =
		"#version 430 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSource_v, NULL);
	glCompileShader(fragmentShaderObject_v);

	GLint fragmentCompileStatus = 0;
	GLint fragmentInfoLogLength = 0;
	GLchar* fragmentInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &fragmentCompileStatus);

	if (fragmentCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &fragmentInfoLogLength);

		if (fragmentInfoLogLength > 0)
		{
			fragmentInfoLog = (GLchar*)malloc(fragmentInfoLogLength);

			if (fragmentInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject_v, fragmentInfoLogLength, &written, fragmentInfoLog);
				fprintf_s(gbFile_SSD, "fragment shader compile log per vertex: %s\n", fragmentInfoLog);
				free(fragmentInfoLog);
				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//per fragment shader
	fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);

	//shader source
	const GLchar* fragmentShaderSource_f =
		"#version 430 core" \
		"\n" \
		"vec3 fong_ads_light;" \
		"in vec4 eye_cordinate;" \
		"in vec3 trasformed_normal;" \
		"in vec3 light_direction[3];" \
		"in vec3 view_vector;" \
		"uniform vec3 u_la[3];" \
		"uniform vec3 u_ld[3];" \
		"uniform vec3 u_ls[3];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;" \
		"uniform int u_l_key_pressed;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"int i;" \
		"vec3 normalised_light_direction[3];" \
		"vec3 reflection_vector[3];" \
		"vec3 ambient[3];" \
		"vec3 diffuse[3];" \
		"vec3 specular[3];" \
		"fong_ads_light = vec3(0.0, 0.0, 0.0);" \
 		"if(u_l_key_pressed == 1)" \
		"{" \
		"vec3 normalised_transformed_normal = normalize(trasformed_normal);" \
		"vec3 normalized_view_vector = normalize(view_vector);" \
		"for(i = 0; i < 3; i++)" \
		"{" \
		"normalised_light_direction[i] = normalize(light_direction[i]);" \
		"reflection_vector[i] = reflect(-normalised_light_direction[i], normalised_transformed_normal);" \
		"ambient[i] = u_la[i] * u_ka;" \
		"diffuse[i] = u_ld[i] * u_kd * max(dot(normalised_light_direction[i], normalised_transformed_normal), 0.0);" \
		"specular[i] = u_ls[i] * u_ks* pow(max(dot(reflection_vector[i], normalized_view_vector), 0.0), u_shininess);" \
		"fong_ads_light = fong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
		"}" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"fragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSource_f, NULL);
	glCompileShader(fragmentShaderObject_f);

	fragmentCompileStatus = 0;
	fragmentInfoLogLength = 0;
	fragmentInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &fragmentCompileStatus);

	if (fragmentCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &fragmentInfoLogLength);

		if (fragmentInfoLogLength > 0)
		{
			fragmentInfoLog = (GLchar*)malloc(fragmentInfoLogLength);

			if (fragmentInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject_f, fragmentInfoLogLength, &written, fragmentInfoLog);
				fprintf_s(gbFile_SSD, "fragment shader compile log per fragment: %s\n", fragmentInfoLog);
				free(fragmentInfoLog);
				DestroyWindow(ghwnd_SSD);
			}
		}
	}


	//shader program for per vertex
	//create
	shaderProgramObject_v = glCreateProgram();

	//attach vertex shader to program object
	glAttachShader(shaderProgramObject_v, vertexShaderObject_v);

	//attach fragment shader to program object
	glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

	//pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(shaderProgramObject_v, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject_v, SSD_ATTRIBUTE_NORMAL, "vNormal");

	//linking program
	glLinkProgram(shaderProgramObject_v);

	GLint programLinkStatus = 0;
	GLint programInfoLogLength = 0;
	GLchar* programInfoLog = NULL;

	glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &programLinkStatus);

	if (programLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &programInfoLogLength);

		if (programInfoLogLength > 0)
		{
			programInfoLog = (GLchar*)malloc(programInfoLogLength);

			if (programInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_v, programInfoLogLength, &written, programInfoLog);
				fprintf_s(gbFile_SSD, "Program link log : %s\n", programInfoLog);
				free(programInfoLog);
				DestroyWindow(ghwnd_SSD);

			}
		}
	}

	//shader program for per fragment
	//create
	shaderProgramObject_f = glCreateProgram();

	//attach vertex shader to program object
	glAttachShader(shaderProgramObject_f, vertexShaderObject_f);

	//attach fragment shader to program object
	glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);

	//pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(shaderProgramObject_f, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject_f, SSD_ATTRIBUTE_NORMAL, "vNormal");

	//linking program
	glLinkProgram(shaderProgramObject_f);

	programLinkStatus = 0;
	programInfoLogLength = 0;
	programInfoLog = NULL;

	glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &programLinkStatus);

	if (programLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &programInfoLogLength);

		if (programInfoLogLength > 0)
		{
			programInfoLog = (GLchar*)malloc(programInfoLogLength);

			if (programInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_f, programInfoLogLength, &written, programInfoLog);
				fprintf_s(gbFile_SSD, "Program link log : %s\n", programInfoLog);
				free(programInfoLog);
				DestroyWindow(ghwnd_SSD);

			}
		}
	}

	ToggleShader();

	/*vec3 ambientLightColor;
	vec3 diffuseLightColor;
	vec3 specularLightColor;
	vec4 lightPosition;*/

	light[0].ambientLightColor = vec3(0.0f, 0.0f, 0.0f);
	light[0].diffuseLightColor = vec3(1.0f, 0.0f, 0.0f);
	light[0].specularLightColor = vec3(1.0f, 0.0f, 0.0f);

	light[1].ambientLightColor = vec3(0.0f, 0.0f, 0.0f);
	light[1].diffuseLightColor = vec3(0.0f, 1.0f, 0.0f);
	light[1].specularLightColor = vec3(0.0f, 1.0f, 0.0f);

	light[2].ambientLightColor = vec3(0.0f, 0.0f, 0.0f);
	light[2].diffuseLightColor = vec3(0.0f, 0.0f, 1.0f);
	light[2].specularLightColor = vec3(0.0f, 0.0f, 1.0f);

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	numSphereVertices = getNumberOfSphereVertices();
	numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//set clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	bAnimate = false;
	bLighting = false;

	perspectiveProjectionMatrix = mat4::identity();

	Resize(WINWIDTH, WINHEIGHT);


}



void Resize(int width, int height)
{
	//checking if height is 0
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{
	//variable declaration
	vec3 ambientLight[3], diffuseLight[3], specularLight[3];
	vec4 LightPosition[3];

	//function declaration
	void ToggleShader(void);
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ToggleShader();

	if (toggle_shader == GL_PER_VERTEX)
	{
		glUseProgram(shaderProgramObject_v);
	}
	else
	{
		glUseProgram(shaderProgramObject_f);
	}


	light[0].lightPosition[0] = 0.0f;
	light[0].lightPosition[1] = radius * sin(xRotate_r);
	light[0].lightPosition[2] = radius * cos(xRotate_r);

	light[1].lightPosition[0] = radius * cos(yRotate_r);
	light[1].lightPosition[1] = 0.0f;
	light[1].lightPosition[2] = radius * sin(yRotate_r);

	light[2].lightPosition[0] = radius * sin(zRotate_r);
	light[2].lightPosition[1] = radius * cos(zRotate_r);
	light[2].lightPosition[2] = 0.0f;


	ambientLight[0] = light[0].ambientLightColor;
	ambientLight[1] = light[1].ambientLightColor;
	ambientLight[2] = light[2].ambientLightColor;

	diffuseLight[0] = light[0].diffuseLightColor;
	diffuseLight[1] = light[1].diffuseLightColor;
	diffuseLight[2] = light[2].diffuseLightColor;

	specularLight[0] = light[0].specularLightColor;
	specularLight[1] = light[1].specularLightColor;
	specularLight[2] = light[2].specularLightColor;

	LightPosition[0] = light[0].lightPosition;
	LightPosition[1] = light[1].lightPosition;
	LightPosition[2] = light[2].lightPosition;

	if (bLighting == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3fv(laUniform, 3, *ambientLight);
		glUniform3fv(ldUniform, 3, *diffuseLight);
		glUniform3fv(lsUniform, 3, *specularLight);
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(shininessUniform, shininess);
		glUniform4fv(lightPositionUniform, 3, *LightPosition);
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

	//OpenGL drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);

	modelMatrix = translateMatrix;

	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc_SSD);

	if (xRotate >= 360.0f)
		xRotate = 0.0f;
	else
		xRotate += 0.1f;

	if (yRotate >= 360.0f)
		yRotate = 0.0f;
	else
		yRotate += 0.1f;

	if (zRotate >= 360.0f)
		zRotate = 0.0f;
	else
		zRotate += 0.1f;

	xRotate_r = (GL_PI / 180.0f) * xRotate;
	yRotate_r = (GL_PI / 180.0f) * yRotate;
	zRotate_r = (GL_PI / 180.0f) * zRotate;
}

void ToggleShader(void)
{



	if (toggle_shader == GL_PER_VERTEX)
	{
		modelMatrixUniform = glGetUniformLocation(shaderProgramObject_v, "u_model_matrix");
		viewMatrixUniform = glGetUniformLocation(shaderProgramObject_v, "u_view_matrix");
		projectionMatrixUniform = glGetUniformLocation(shaderProgramObject_v, "u_projection_matrix");
		laUniform = glGetUniformLocation(shaderProgramObject_v, "u_la");
		ldUniform = glGetUniformLocation(shaderProgramObject_v, "u_ld");
		lsUniform = glGetUniformLocation(shaderProgramObject_v, "u_ls");
		kaUniform = glGetUniformLocation(shaderProgramObject_v, "u_ka");
		kdUniform = glGetUniformLocation(shaderProgramObject_v, "u_kd");
		ksUniform = glGetUniformLocation(shaderProgramObject_v, "u_ks");
		shininessUniform = glGetUniformLocation(shaderProgramObject_v, "u_shininess");
		lightPositionUniform = glGetUniformLocation(shaderProgramObject_v, "u_light_position");
		lKeyPressedUniform = glGetUniformLocation(shaderProgramObject_v, "u_l_key_pressed");
	}
	else
	{

		modelMatrixUniform = glGetUniformLocation(shaderProgramObject_f, "u_model_matrix");
		viewMatrixUniform = glGetUniformLocation(shaderProgramObject_f, "u_view_matrix");
		projectionMatrixUniform = glGetUniformLocation(shaderProgramObject_f, "u_projection_matrix");
		laUniform = glGetUniformLocation(shaderProgramObject_f, "u_la");
		ldUniform = glGetUniformLocation(shaderProgramObject_f, "u_ld");
		lsUniform = glGetUniformLocation(shaderProgramObject_f, "u_ls");
		kaUniform = glGetUniformLocation(shaderProgramObject_f, "u_ka");
		kdUniform = glGetUniformLocation(shaderProgramObject_f, "u_kd");
		ksUniform = glGetUniformLocation(shaderProgramObject_f, "u_ks");
		shininessUniform = glGetUniformLocation(shaderProgramObject_f, "u_shininess");
		lightPositionUniform = glGetUniformLocation(shaderProgramObject_f, "u_light_position");
		lKeyPressedUniform = glGetUniformLocation(shaderProgramObject_f, "u_l_key_pressed");
	}

}

void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}
	if (shaderProgramObject_v)
	{
		glUseProgram(shaderProgramObject_v);
		GLsizei shaderCount;

		glGetProgramiv(shaderProgramObject_v, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = NULL;
		pShaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));

		if (pShaders == NULL)
		{
			//detach vertex shader  from shader program object
			glDetachShader(shaderProgramObject_v, vertexShaderObject_v);

			//detach fragment shader from shader program object
			glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);

			//delete shader
			glDeleteShader(vertexShaderObject_v);
			vertexShaderObject_v = 0;

			glDeleteShader(fragmentShaderObject_v);
			fragmentShaderObject_v = 0;

			//delete program object
			glDeleteProgram(shaderProgramObject_v);
			shaderProgramObject_v = 0;
			glUseProgram(0);
		}

		glGetAttachedShaders(shaderProgramObject_v, shaderCount, &shaderCount, pShaders);

		for (GLsizei i = 0; i < shaderCount; i++)
		{
			glDetachShader(shaderProgramObject_v, pShaders[i]);
			glDeleteShader(pShaders[i]);
			pShaders[i] = 0;
		}

		free(pShaders);

		glDeleteProgram(shaderProgramObject_v);
		shaderProgramObject_v = 0;

		//unlink shader program
		glUseProgram(0);
	}




	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}