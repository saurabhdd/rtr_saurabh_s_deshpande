#include<windows.h>
#include<gl/glew.h>
#include<gl/gl.h>
#include<stdio.h>
#include<stdlib.h>
#include"Header.h"
#include"vmath.h"

#define WINWIDTH 800
#define WINHEIGHT 600

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC ghlrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;

//shader variables
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;

using namespace vmath;

enum
{
	SSD_ATTRIBUTE_POSITION = 0,
	SSD_ATTRIBUTE_COLOR,
	SSD_ATTRIBUTE_NORMAL,
	SSD_ATTRIBUTE_TEXTURE

};

GLuint vao;
GLuint vbo_position;
GLuint vbo_color;
GLuint mvpUniform;

vmath::mat4 perspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd_SSD;
	WNDCLASSEX wndClass_SSD;
	bool bdone = false;
	TCHAR szAppName_SSD[] = TEXT("MyWindow");
	MSG msg_SSD;

	if (fopen_s(&gpFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("log file creation unsucceful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndClass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndClass_SSD.cbClsExtra = 0;
	wndClass_SSD.cbWndExtra = 0;
	wndClass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass_SSD.hInstance = hInstance;
	wndClass_SSD.lpfnWndProc = WndProc;
	wndClass_SSD.lpszClassName = szAppName_SSD;
	wndClass_SSD.lpszMenuName = NULL;
	wndClass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndClass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName_SSD,
		TEXT("practice"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD,
		Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd_SSD = hwnd_SSD;
	
	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bdone == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
			{
				bdone = true;
			}
			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
		}
	}

	return (int)msg_SSD.wParam;


}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void Display(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(ghwnd_SSD);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(ghwnd_SSD);
		break;

	case WM_PAINT:
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };

	//code
	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}
	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}
}

void Initialize(void)
{
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatDescriptor_SSD;

	//get device context
	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));

	//initialize pfd
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	//choose the pixel format
	iPixelFormatDescriptor_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);

	if (iPixelFormatDescriptor_SSD == 0)
	{
		fprintf_s(gpFile_SSD, "ChoosePixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatDescriptor_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gpFile_SSD, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	ghlrc_SSD = wglCreateContext(ghdc_SSD);

	if (!ghlrc_SSD)
	{
		fprintf_s(gpFile_SSD, "wglCreateContext() failed \n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, ghlrc_SSD) == FALSE)
	{
		fprintf_s(gpFile_SSD, "wglMakeCurrent() failed \n");
		DestroyWindow(ghwnd_SSD);
	}

	GLenum glew_error = glewInit();

	if (glew_error != GLEW_OK)
	{
		fprintf_s(gpFile_SSD, "glewInit() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	//openGL related logs
	fprintf_s(gpFile_SSD, "OpenGL vendor: %s\n", glGetString(GL_VENDOR));
	fprintf_s(gpFile_SSD, "OpenGL renderer: %s\n", glGetString(GL_RENDERER));
	fprintf_s(gpFile_SSD, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf_s(gpFile_SSD, "Graphics library shading language(GLSL): %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	//OpenGL enabled extenision
	GLint numExtensins;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensins);

	for (int i = 0; i < numExtensins; i++)
	{
		fprintf_s(gpFile_SSD, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//** vertex Shader**
	//create shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing source for shader
	const GLchar* vertexShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	//providing source to shader
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSource, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking of the compilation of vertex shader
	GLint infoLogLength = 0;
	GLint compilationStatus = 0;
	GLchar* vertexShaderInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &compilationStatus);

	if (compilationStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		if (infoLogLength > 0)
		{
			vertexShaderInfoLog = (GLchar*)malloc(infoLogLength);

			if (vertexShaderInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, vertexShaderInfoLog);
				fprintf_s(gpFile_SSD, "%s\n", vertexShaderInfoLog);
				free(vertexShaderInfoLog);
				vertexShaderInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//**fragment shader**
	//create shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Creating source for shader
	const GLchar* fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = out_color;" \
		"}";

	//provide source to shader object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSource, NULL);

	//compile fragment shader
	glCompileShader(fragmentShaderObject);

	//compilation error checking
	GLchar* fragmentInfoLog = NULL;
	infoLogLength = 0;
	compilationStatus = 0;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &compilationStatus);

	if (compilationStatus == FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		if (infoLogLength > 0)
		{
			fragmentInfoLog = (GLchar*)malloc(infoLogLength);

			if (fragmentInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, fragmentInfoLog);
				fprintf_s(gpFile_SSD, "%s\n", fragmentInfoLog);
				free(fragmentInfoLog);
				fragmentInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}


	//shader program object
	shaderProgramObject = glCreateProgram();

	//attach shaders to program
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	//pre link binding of shader program object with vertex shader intake values 
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_COLOR, "vColor");

	//link shader program object
	glLinkProgram(shaderProgramObject);

	GLint linkStatus = 0;
	GLint linkInfoLogLength = 0;
	GLchar* linkInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &linkInfoLogLength);

		if (linkInfoLogLength > 0)
		{
			GLint written;
			linkInfoLog = (GLchar*)malloc(linkInfoLogLength);

			if (linkInfoLog != NULL)
			{
				glGetProgramInfoLog(shaderProgramObject, linkInfoLogLength, &written, linkInfoLog);
				fprintf_s(gpFile_SSD, "%s\n", linkInfoLog);
				free(linkInfoLog);
				linkInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}

		}
	}

	//get uniform location
	mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

	//vertices colors and shader attribs
	const GLfloat triangleVertices_SSD[] =
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	//colors
	const GLfloat trianleColors_SSD[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices_SSD), triangleVertices_SSD, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(trianleColors_SSD), trianleColors_SSD, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//set clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();

	Resize(WINWIDTH, WINHEIGHT);


}


void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	//OpenGL drawing
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);

	modelViewMatrix = translateMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc_SSD);

}


void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);

		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_color)
	{
		glDeleteBuffers(1, &vbo_color);
		vbo_position = vbo_color;
	}

	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
		GLsizei shaderCount;

		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = NULL;
		pShaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));

		if (pShaders == NULL)
		{
			//detach vertex shader  from shader program object
			glDetachShader(shaderProgramObject, vertexShaderObject);

			//detach fragment shader from shader program object
			glDetachShader(shaderProgramObject, fragmentShaderObject);

			//delete shader
			glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;

			glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;

			//delete program object
			glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			glUseProgram(0);
		}

		glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);

		for (GLsizei i = 0; i < shaderCount; i++)
		{
			glDetachShader(shaderProgramObject, pShaders[i]);
			glDeleteShader(pShaders[i]);
			pShaders[i] = 0;
		}

		free(pShaders);

		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		//unlink shader program
		glUseProgram(0);
	}




	if (wglGetCurrentContext() == ghlrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghlrc_SSD)
	{
		wglDeleteContext(ghlrc_SSD);
		ghlrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gpFile_SSD)
	{
		fprintf_s(gpFile_SSD, "log file closed successfully\n");
		fclose(gpFile_SSD);
		gpFile_SSD = NULL;
	}


}









































































































































































































































