#include<windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<gl/gl.h>
#include"Header.h"
#include"vmath.h"
#include"Sphere.h"

#define WINWIDTH 800
#define WINHEIGHT 600
#define GL_PI 3.1415

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;

//shader variables
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;

using namespace vmath;

enum
{
	SSD_ATTRIBUTE_POSITION = 0,
	SSD_ATTRIBUTE_COLOR,
	SSD_ATTRIBUTE_NORMAL,
	SSD_ATTRIBUTE_TEXTURE,
};

GLuint vao;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;

//matrix uniforms
GLuint projectionMatrixUniform;
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;

//light uniforms
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;

//material uniform
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;

//shininess uniform
GLuint shininessUniform;

//lightPosition Uniform
GLuint lightPositionUniform;

//key pressed uniform
GLuint lKeyPressedUniform;

//light arrays
GLfloat ambientLight[] = { 0.0f, 0.0f, 0.0f };
GLfloat diffuseLight[] = { 1.0f, 1.0f, 1.0f };
GLfloat specularLight[] = { 0.7f, 0.7f, 0.7f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 100.0f, 1.0f };


GLfloat shininess = 128;

bool bAnimate;
bool bLighting;

mat4 perspectiveProjectionMatrix;

GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numSphereVertices;
GLuint numSphereElements;

GLfloat xDimension1 = 1.8f;
GLfloat xDimension2 = 5.50f;
GLfloat yDimension1 = -3.50f;
GLfloat yDimension2 = -2.20f;
GLfloat yDimension3 = -0.80f;
GLfloat yDimension4 = 0.60f;
GLfloat yDimension5 = 2.0f;
GLfloat yDimension6 = 3.50f;

GLfloat xRotate;
GLfloat yRotate;
GLfloat zRotate;
GLfloat xRotate_r;
GLfloat yRotate_r;
GLfloat zRotate_r;

int keyPressed = 0;

GLfloat radius = 400.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);

	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName_SSD,
		TEXT("practice"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'A':
		case 'a':
			if (bAnimate == false)
				bAnimate = true;
			else
				bAnimate = false;
			break;

		case 'l':
		case 'L':
			if (bLighting == false)
				bLighting = true;
			else
				bLighting = false;
			break;

		case 'x':
		case 'X':
		   keyPressed = 1;
		   break;
		   
		 case 'y':
		 case 'Y':
		    keyPressed = 2;
			break;
			
		 case 'z':
		 case 'Z':
		 keyPressed = 3;
		 break;
		   
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}

void Initialize(void)
{

	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex_SSD;

	ghdc_SSD = GetDC(ghwnd_SSD);

	//initalizing pixelformatdescriptor
	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex_SSD = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex_SSD == 0)
	{
		fprintf_s(gbFile_SSD, "ChoosePixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex_SSD, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (!hglrc_SSD)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}

	//OpenGL related logs
	fprintf_s(gbFile_SSD, "OpenGL vendor: %s\n", glGetString(GL_VENDOR));
	fprintf_s(gbFile_SSD, "OpenGL renderer: %s\n", glGetString(GL_RENDER));
	fprintf_s(gbFile_SSD, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf_s(gbFile_SSD, "Graphic library shading language version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf_s(gbFile_SSD, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//************shaders***********
	//**vertex Shader**

	//create shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provice source code to shader
	const GLchar* vertexShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int l_key_pressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;" \
		"uniform vec4 u_light_position;" \
		"out vec3 fong_ads_lighting;" \
		"void main(void)" \
		"{" \
		"if(l_key_pressed == 1)" \
		"{" \
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(u_light_position - eye_cordinate));" \
		"vec3 refliection_vector = reflect(-light_direction, trasformed_normal);" \
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * max(dot(light_direction, trasformed_normal), 0.0);" \
		"vec3 specular = u_ls * u_ks* pow(max(dot(refliection_vector, view_vector), 0.0), u_shininess);" \
		"fong_ads_lighting = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"fong_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSource, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint vertexInfoLogLength = 0;
	GLint vertexShaderCompileStatus = 0;
	GLchar* vertexInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &vertexShaderCompileStatus);

	if (vertexShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &vertexInfoLogLength);

		if (vertexInfoLogLength > 0)
		{
			vertexInfoLog = (GLchar*)malloc(vertexInfoLogLength);

			if (vertexInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, vertexInfoLogLength, &written, vertexInfoLog);
				fprintf_s(gbFile_SSD, "Vertex shader compile log: %s\n", vertexInfoLog);
				free(vertexInfoLog);

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//***fragment shader***
	//create Shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//shader source
	const GLchar* fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec3 fong_ads_lighting;" \
		"uniform int l_key_pressed;" \
		"out vec4 fragColor;" \
		"vec4 color;" \
		"void main(void)" \
		"{" \
		"if(l_key_pressed == 1)" \
		"{" \
		"color = vec4(fong_ads_lighting, 1.0);" \
		"}" \
		"else" \
		"{" \
		"color = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"fragColor = color;" \
		"}";

	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSource, NULL);
	glCompileShader(fragmentShaderObject);

	GLint fragmentCompileStatus = 0;
	GLint fragmentInfoLogLength = 0;
	GLchar* fragmentInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &fragmentCompileStatus);

	if (fragmentCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &fragmentInfoLogLength);

		if (fragmentInfoLogLength > 0)
		{
			fragmentInfoLog = (GLchar*)malloc(fragmentInfoLogLength);

			if (fragmentInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject, fragmentInfoLogLength, &written, fragmentInfoLog);
				fprintf_s(gbFile_SSD, "fragment shader compile log: %s\n", fragmentInfoLog);
				free(fragmentInfoLog);
				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//shader program
	//create
	shaderProgramObject = glCreateProgram();

	//attach vertex shader to program object
	glAttachShader(shaderProgramObject, vertexShaderObject);

	//attach fragment shader to program object
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_NORMAL, "vNormal");

	//linking program
	glLinkProgram(shaderProgramObject);

	GLint programLinkStatus = 0;
	GLint programInfoLogLength = 0;
	GLchar* programInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &programLinkStatus);

	if (programLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &programInfoLogLength);

		if (programInfoLogLength > 0)
		{
			programInfoLog = (GLchar*)malloc(programInfoLogLength);

			if (programInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject, programInfoLogLength, &written, programInfoLog);
				fprintf_s(gbFile_SSD, "Program link log : %s\n", programInfoLog);
				free(programInfoLog);
				DestroyWindow(ghwnd_SSD);

			}
		}
	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	shininessUniform = glGetUniformLocation(shaderProgramObject, "u_shininess");
	lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "l_key_pressed");


	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	numSphereVertices = getNumberOfSphereVertices();
	numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//set clear color
	glClearColor(0.250f, 0.250f, 0.250f, 1.0f);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	bAnimate = false;
	bLighting = false;

	perspectiveProjectionMatrix = mat4::identity();

	Resize(WINWIDTH, WINHEIGHT);


}

void Resize(int width, int height)
{
	//checking if height is 0
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);

}

void Display(void)
{
	//function declaration
	void BindUniforms(vec3 materialAmbient, vec3 materialDiffuse, vec3 materialSpecular, GLfloat);

	//variable declaration
	vec3 ambientMaterial[24];
	vec3 diffuseMaterial[24];
	vec3 specularMaterial[24];
	GLfloat MShininess[24];
	GLfloat xDim; 
	GLfloat yDim;
	
	 ambientMaterial[0] = vec3(0.0215f, 0.1745f, 0.0215f);
	diffuseMaterial[0] = vec3(0.18275f, 0.17f, 0.22525f);
	specularMaterial[0] = vec3(0.332741f, 0.328634f, 0.346435f);

	ambientMaterial[1] = vec3(0.135, 0.2225, 0.1575);
	diffuseMaterial[1] = vec3(0.54, 0.89, 0.63);
	specularMaterial[1] = vec3(0.316228, 0.316228, 0.316228);

	ambientMaterial[2] = vec3(0.05375, 0.05, 0.06625);
	diffuseMaterial[2] = vec3(0.18275, 0.17, 0.22525);
	specularMaterial[2] = vec3(0.332741, 0.328634, 0.346435);

	ambientMaterial[3] = vec3(0.25, 0.20725, 0.20725);
	diffuseMaterial[3] = vec3(1.0, 0.829, 0.829);
	specularMaterial[3] = vec3(0.296648, 0.296648, 0.296648);

	ambientMaterial[4] = vec3(0.1745, 0.01175, 0.01175);
	diffuseMaterial[4] = vec3(0.61424, 0.04136, 0.04136);
	specularMaterial[4] = vec3(0.727811, 0.626959, 0.626959);

	ambientMaterial[5] = vec3(0.1f, 0.18725f, 0.1745f);
	diffuseMaterial[5] = vec3(0.396, 0.74151, 0.69102);
	specularMaterial[5] = vec3(0.297254, 0.30829, 0.306678);

	ambientMaterial[6] = vec3(0.329412, 0.223529, 0.027451);
	diffuseMaterial[6] = vec3(0.780392, 0.568627, 0.113725);
	specularMaterial[6] = vec3(0.992157, 0.941176, 0.807843);

	ambientMaterial[7] = vec3(0.2125, 0.1275, 0.054);
	diffuseMaterial[7] = vec3(0.714, 0.4284, 0.18144);
	specularMaterial[7] = vec3(0.393548, 0.271906, 0.166721);

	ambientMaterial[8] = vec3(0.25, 0.25, 0.25);
	diffuseMaterial[8] = vec3(0.4, 0.4, 0.4);
	specularMaterial[8] = vec3(0.774597, 0.774597, 0.774597);

	ambientMaterial[9] = vec3(0.19125, 0.0735, 0.0225);
	diffuseMaterial[9] = vec3(0.7038, 0.27048, 0.0828);
	specularMaterial[9] = vec3(0.256777, 0.137622, 0.086014);

	ambientMaterial[10] = vec3(0.24725, 0.1995, 0.0745);
	diffuseMaterial[10] = vec3(0.75164, 0.60648, 0.22648);
	specularMaterial[10] = vec3(0.628281, 0.555802, 0.366065);

	ambientMaterial[11] = vec3(0.19225, 0.19225, 0.19225);
	diffuseMaterial[11] = vec3(0.50754, 0.50754, 0.50754);
	specularMaterial[11] = vec3(0.508273, 0.508273, 0.508273);

	ambientMaterial[12] = vec3(0.0f, 0.0f, 0.0f);
	diffuseMaterial[12] = vec3(0.01, 0.01, 0.01);
	specularMaterial[12] = vec3(0.50, 0.50, 0.50);

	ambientMaterial[13] = vec3(0.0f, 0.1f, 0.06f);
	diffuseMaterial[13] = vec3(0.0f, 0.50980392, 0.50980392);
	specularMaterial[13] = vec3(0.50196078, 0.50196078, 0.50196078);

	ambientMaterial[14] = vec3(0.0f, 0.0f, 0.0f);
	diffuseMaterial[14] = vec3(0.1f, 0.35f, 0.1f);
	specularMaterial[14] = vec3(0.45f, 0.55f, 0.45f);

	ambientMaterial[15] = vec3(0.0f, 0.0f, 0.0f);
	diffuseMaterial[15] = vec3(0.5f, 0.0f, 0.0f);
	specularMaterial[15] = vec3(0.7f, 0.6f, 0.6f);

	ambientMaterial[16] = vec3(0.0f, 0.0f, 0.0f);
	diffuseMaterial[16] = vec3(0.55f, 0.55f, 0.55f);
	specularMaterial[16] = vec3(0.70f, 0.70f, 0.70f);

	ambientMaterial[17] = vec3(0.0f, 0.0f, 0.0f);
	diffuseMaterial[17] = vec3(0.5f, 0.5f, 0.0f);
	specularMaterial[17] = vec3(0.60f, 0.60f, 0.50f);

	ambientMaterial[18] = vec3(0.02, 0.02, 0.02);
	diffuseMaterial[18] = vec3(0.01, 0.01, 0.01);
	specularMaterial[18] = vec3(0.4, 0.4, 0.4);

	ambientMaterial[19] = vec3(0.0f, 0.05f, 0.05f);
	diffuseMaterial[19] = vec3(0.4f, 0.5f, 0.5f);
	specularMaterial[19] = vec3(0.04f, 0.7f, 0.7f);

	ambientMaterial[20] = vec3(0.0f, 0.05f, 0.0f);
	diffuseMaterial[20] = vec3(0.4f, 0.5f, 0.4f);
	specularMaterial[20] = vec3(0.04f, 0.7f, 0.04f);

	ambientMaterial[21] = vec3(0.05f, 0.0f, 0.0f);
	diffuseMaterial[21] = vec3(0.5f, 0.4f, 0.4f);
	specularMaterial[21] = vec3(0.7f, 0.04, 0.04);

	ambientMaterial[22] = vec3(0.05, 0.05, 0.05);
	diffuseMaterial[22] = vec3(0.5f, 0.5f, 0.5f);
	specularMaterial[22] = vec3(0.7f, 0.7f, 0.7f);

	ambientMaterial[23] = vec3(0.05f, 0.05f, 0.0f);
	diffuseMaterial[23] = vec3(0.5f, 0.5f, 0.4f);
	specularMaterial[23] = vec3(0.7f, 0.7f, 0.04f);
	
	
	MShininess[0] = 0.3 * 128;
	 MShininess[1] = 0.3 * 128;
	 MShininess[2] = 0.3 * 128;
	 MShininess[3] = 0.3 * 128;
	 MShininess[4] = 0.6 * 128;
	 MShininess[5] = 0.1 * 128;
	 MShininess[6] = 0.21794872 * 128;
	 MShininess[7] = 0.2 * 128;
	 MShininess[8] = 0.6 * 128;
	 MShininess[9] = 0.1 * 128;
	 MShininess[10] = 0.4 * 128;
	 MShininess[11] = 0.4 * 128;
	 MShininess[12] = 0.25 * 128;
	 MShininess[13] = 0.25 * 128;
	 MShininess[14] = 0.25 * 128;
	 MShininess[15] = 0.25 * 128;
	 MShininess[16] = 0.25 * 128;
	 MShininess[17] = 0.25 * 128;
	 MShininess[18] = 0.078125 * 128;
	 MShininess[19] = 0.078125 * 128;
	 MShininess[20] = 0.078125 * 128;
	 MShininess[21] = 0.078125 * 128;
	 MShininess[22] = 0.078125 * 128;
	 MShininess[23] = 0.078125 * 128;


	if (keyPressed == 1)
	{
		lightPosition[0] = 0.0f;
		lightPosition[1] = radius * sin(xRotate_r);
		lightPosition[2] = radius * cos(xRotate_r);
	}
	else if (keyPressed == 2)
	{
		lightPosition[0] = radius * cos(xRotate_r);
		lightPosition[1] = 0.0f;
		lightPosition[2] = radius * sin(xRotate_r);
	}
	else if (keyPressed == 3)
	{
		lightPosition[0] = radius * sin(xRotate_r);
		lightPosition[1] = radius * cos(xRotate_r);
		lightPosition[2] = 0.0f;
	}

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	

	//OpenGL drawing
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	
	mat4 translateMatrix = mat4::identity();
	
	
    for(int i =0; i < 24; i++)
	{
		if(i % 4 == 0)
			xDim = -xDimension2;
		if(i % 4 == 1)
			xDim = -xDimension1;
		if(i % 4 == 2)
			xDim = xDimension1;
		if(i % 4 == 3)
			xDim = xDimension2;
		
		if(i >= 0 && i < 4)
			yDim = yDimension6;
		if(i >= 4 && i < 8)
			yDim = yDimension5;
		if(i >= 8 && i < 12)
			yDim = yDimension4;
		if(i >= 12 && i < 16)
			yDim = yDimension3;
		if(i >= 16 && i < 20)
			yDim = yDimension2;
		if(i >= 20 && i < 24)
			yDim = yDimension1;
		
		
	    modelMatrix = mat4::identity();
	    viewMatrix = mat4::identity();
	    translateMatrix = vmath::translate(xDim, yDim, -10.0f);
	    modelMatrix = translateMatrix;
	    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	    BindUniforms(ambientMaterial[i], diffuseMaterial[i], specularMaterial[i], MShininess[i]);
	    glBindVertexArray(vao);
	    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	    glDrawElements(GL_TRIANGLES, numSphereElements, GL_UNSIGNED_SHORT, 0);
	    glBindVertexArray(0);
	
	}

	glUseProgram(0);


	if (xRotate >= 360.0f)
		xRotate = 0.0f;
	else
		xRotate += 0.1f;

	if (yRotate >= 360.0f)
		yRotate = 0.0f;
	else
		yRotate += 0.1f;

	if (zRotate >= 360.0f)
		zRotate = 0.0f;
	else
		zRotate += 0.1f;

	xRotate_r = (GL_PI / 180.0f) * xRotate;
	yRotate_r = (GL_PI / 180.0f) * yRotate;
	zRotate_r = (GL_PI / 180.0f) * zRotate;

	SwapBuffers(ghdc_SSD);
}



void BindUniforms(vec3 materialAmbient, vec3 materialDiffuse, vec3 materialSpecular, GLfloat Shininess)
{
	if (bLighting == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3fv(laUniform, 1, ambientLight);
		glUniform3fv(ldUniform, 1, diffuseLight);
		glUniform3fv(lsUniform, 1, specularLight);
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(shininessUniform, shininess);
		glUniform4fv(lightPositionUniform, 1, lightPosition);
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

}

void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}
	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
		GLsizei shaderCount;

		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = NULL;
		pShaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));

		if (pShaders == NULL)
		{
			//detach vertex shader  from shader program object
			glDetachShader(shaderProgramObject, vertexShaderObject);

			//detach fragment shader from shader program object
			glDetachShader(shaderProgramObject, fragmentShaderObject);

			//delete shader
			glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;

			glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;

			//delete program object
			glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			glUseProgram(0);
		}

		glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);

		for (GLsizei i = 0; i < shaderCount; i++)
		{
			glDetachShader(shaderProgramObject, pShaders[i]);
			glDeleteShader(pShaders[i]);
			pShaders[i] = 0;
		}

		free(pShaders);

		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		//unlink shader program
		glUseProgram(0);
	}




	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}