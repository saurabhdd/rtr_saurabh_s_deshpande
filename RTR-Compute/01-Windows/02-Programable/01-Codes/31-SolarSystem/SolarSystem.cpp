#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/gl.h>

#include"Header.h"
#include"vmath.h"
#include"Sphere.h"

#define WINWIDTH 800
#define WINHEIGHT 600
#define STACK_CAPACITY 5

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;

GLfloat angleTriangle;
GLfloat angleRectangle;

//shader variables
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;

using namespace vmath;

enum
{
	SSD_ATTRIBUTE_POSITION = 0,
	SSD_ATTRIBUTE_COLOR,
	SSD_ATTRIBUTE_NORMAL,
	SSD_ATTRIBUTE_TEXTURE,
};

GLuint vao;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;
//GLuint vbo_pyramid_color;

GLuint modelViewMatrixUniform;
GLuint projectionMatrixUniform;

//light uniform
GLuint LKeyPressedUniform;
GLuint LdUniform;
GLuint KdUniform;
GLuint lightPositionUniform;

bool gbAnimate;
bool gbLighting;


vmath::mat4 perspectiveProjectionMatrix;

GLfloat sphere_vertices[1146];
GLfloat sphere_normals[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numSphereVertices;
GLuint numSphereElements;

//matrix stack
typedef struct __Stack {

	int top;
	int capacity;
	mat4* data;

}Stack;


int day_SSD = 0;
int year_SSD = 0;
int month_SSD = 0;

Stack* stack = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);
	void Update(void);

	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("practice"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();

				//if (gbAnimate == true)
					//Update();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'D':
			day_SSD = day_SSD + 6 % 360;
			break;

		case 'd':
			day_SSD = day_SSD - 6 % 360;
			break;

		case 'Y':
			year_SSD = year_SSD + 3 % 360;
			break;

		case 'y':
			year_SSD = year_SSD - 3 % 360;
			break;

		case 'M':
			month_SSD = month_SSD + 2 % 360;
			break;

		case 'm':
			month_SSD = month_SSD - 2 % 360;
			break;

		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}

void Initialize(void)
{
	//function declaration
	void Resize(int, int);
	Stack* CreateStack(int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex;

	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));

	//initalizing pfd
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex == 0)
	{
		fprintf_s(gbFile_SSD, "ChoosePixelFormt() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (hglrc_SSD == NULL)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	GLenum glew_error = glewInit();

	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}

	fprintf_s(gbFile_SSD, "OpenGL vendor: %s\n", glGetString(GL_VENDOR));
	fprintf_s(gbFile_SSD, "OpenGL renderer: %s\n", glGetString(GL_RENDER));
	fprintf_s(gbFile_SSD, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf_s(gbFile_SSD, "OpenGL shading language version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExtensions;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf_s(gbFile_SSD, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//**vertex shader**
	//create shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//normal matrix is upper left 3*3 matrix of transpose of inverse of model view matrix

	//source code for vertex shader
	const GLchar* vertexShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"color = vColor;" \
		"}";

	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSource, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//compilation error checking
	GLint shaderCompileStatus = 0;
	GLint shaderInfoLogLength = 0;
	GLchar* shaderInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);

		if (shaderInfoLogLength > 0)
		{
			shaderInfoLog = (GLchar*)malloc(shaderInfoLogLength);

			if (shaderInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(vertexShaderObject, shaderInfoLogLength, &written, shaderInfoLog);
				fprintf_s(gbFile_SSD, "vertex shader compilation log:%s\n", shaderInfoLog);
				free(shaderInfoLog);
				shaderInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//**fragment Shader**
	//create shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = color;" \
		"}";

	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSource, NULL);

	//compile fragment shader
	glCompileShader(fragmentShaderObject);

	shaderCompileStatus = 0;
	shaderInfoLogLength = 0;
	shaderInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);

		if (shaderInfoLogLength > 0)
		{
			shaderInfoLog = (GLchar*)malloc(shaderInfoLogLength);

			if (shaderInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject, shaderInfoLogLength, &written, shaderInfoLog);
				fprintf_s(gbFile_SSD, "fragment shader compilation log:%s\n", shaderInfoLog);
				free(shaderInfoLog);
				shaderInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	shaderProgramObject = glCreateProgram();

	//attching shaders with program object
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	//pre link binding of attributes with program object
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_COLOR, "vColor");

	//linking the program
	glLinkProgram(shaderProgramObject);

	GLint linkStatus = 0;
	GLint linkInfoLogLength = 0;
	GLchar* linkInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &linkInfoLogLength);

		if (linkInfoLogLength > 0)
		{
			GLint written;
			linkInfoLog = (GLchar*)malloc(linkInfoLogLength);

			if (linkInfoLog != NULL)
			{
				glGetProgramInfoLog(shaderProgramObject, linkInfoLogLength, &written, linkInfoLog);
				fprintf_s(gbFile_SSD, "%s\n", linkInfoLog);
				free(linkInfoLog);
				linkInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}

		}
	}

	//binding of uniform with program

	modelViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	numSphereVertices = getNumberOfSphereVertices();
	numSphereElements = getNumberOfSphereElements();


	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	

	glGenBuffers(1, &vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create stack
	stack = CreateStack(STACK_CAPACITY);

	//set clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//perspective projection
	perspectiveProjectionMatrix = mat4::identity();

	Resize(WINWIDTH, WINHEIGHT);


}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void Display(void)
{
	//variable declaration


	//function declaration
	void PushMatrix(mat4);
	mat4 PopMatrix(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	mat4 modelViewMatrix = mat4::identity();
	mat4 translateMatrix = mat4::identity();
	mat4 rotateMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 modelViewMprojectionMatrix = mat4::identity();
	mat4 translateMatrix1 = mat4::identity();
	mat4 rotateMatrix1 = mat4::identity();

	//first translation
	translateMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewMatrix = translateMatrix;
	PushMatrix(modelViewMatrix); //1

	//drawing sun
	////////////////////////////////////////////////
	rotateMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	scaleMatrix = vmath::scale(1.75f, 1.75f, 1.75f);
    modelViewMatrix = modelViewMatrix * scaleMatrix * rotateMatrix;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	modelViewMprojectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMprojectionMatrix);

	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glVertexAttrib3f(SSD_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);
	glDrawElements(GL_TRIANGLES, numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	modelViewMatrix = PopMatrix();

	//earth and moon system
	////////////////////////////////////////////////////
	PushMatrix(modelViewMatrix); //1
	rotateMatrix = vmath::rotate((GLfloat)year_SSD, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(3.5f, 0.0f, 0.0f);

	modelViewMatrix = modelViewMatrix * rotateMatrix * translateMatrix;

	PushMatrix(modelViewMatrix); //2

	//moon
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	rotateMatrix = vmath::rotate((GLfloat)month_SSD, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(1.4f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.4f, 0.4f, 0.4f);
	modelViewMatrix = modelViewMatrix  * rotateMatrix * translateMatrix * rotateMatrix1 * scaleMatrix;
	modelViewMprojectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMprojectionMatrix);

	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glVertexAttrib3f(SSD_ATTRIBUTE_COLOR, 0.9f, 1.0f, 1.05f);
	glDrawElements(GL_TRIANGLES, numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	modelViewMatrix = PopMatrix(); //1

	//earth
	rotateMatrix = vmath::rotate((GLfloat)day_SSD, 0.0f, 1.0f, 0.0f);
	scaleMatrix = vmath::scale(0.8f, 0.8f, 0.8f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	modelViewMatrix = modelViewMatrix * rotateMatrix1 * rotateMatrix * scaleMatrix;

	modelViewMprojectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMprojectionMatrix);

	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glVertexAttrib3f(SSD_ATTRIBUTE_COLOR, 0.1f, 0.05f, 0.95f);
	glDrawElements(GL_TRIANGLES, numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	PopMatrix(); //0

	glUseProgram(0);

	SwapBuffers(ghdc_SSD);

}


Stack* CreateStack(int capacity)
{
	Stack* stackTemp = NULL;
	stackTemp = (Stack*)malloc(sizeof(Stack));

	if (stackTemp == NULL)
	{
		DestroyWindow(ghwnd_SSD);
	}

	stackTemp->capacity = capacity;
	stackTemp->top = -1;

	stackTemp->data = (mat4*)malloc(sizeof(mat4) * capacity);
	if (stackTemp->data == NULL)
		DestroyWindow(ghwnd_SSD);

	return stackTemp;


}

void PushMatrix(mat4 data)
{
	bool isFull(void);
	bool isEmpty(void);

	if (isFull())
	{
		stack->capacity *= 2;
		stack->data = (mat4*)realloc(stack->data, stack->capacity);
		if (stack->data == NULL)
			DestroyWindow(ghwnd_SSD);
	}

	if (stack == NULL)
		DestroyWindow(ghwnd_SSD);

	stack->data[++stack->top] = data;


}

mat4 PopMatrix(void)
{
	bool isFull(void);
	bool isEmpty(void);


	if (isEmpty())
		return NULL;

	return stack->data[stack->top--];


}

void DeleteStack(void)
{
	if (stack)
	{
		if (stack->data)
		{
			free(stack->data);
			stack->data = NULL;
		}

		free(stack);
		stack = NULL;
	}
}

bool isEmpty(void)
{
	if (stack != NULL)
	{
		if (stack->top == -1)
			return true;
	}

	return false;
}

bool isFull(void)
{
	if (stack != NULL)
	{
		if (stack->top == stack->capacity - 1)
			return true;
	}

	return false;
}

void Uninitialize(void)
{
	//file IO code
	void DeleteStack(void);

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}



	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
		GLsizei shaderCount;

		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = NULL;
		pShaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));

		if (pShaders == NULL)
		{
			//detach vertex shader  from shader program object
			glDetachShader(shaderProgramObject, vertexShaderObject);

			//detach fragment shader from shader program object
			glDetachShader(shaderProgramObject, fragmentShaderObject);

			//delete shader
			glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;

			glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;

			//delete program object
			glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			glUseProgram(0);
		}

		glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);

		for (GLsizei i = 0; i < shaderCount; i++)
		{
			glDetachShader(shaderProgramObject, pShaders[i]);
			glDeleteShader(pShaders[i]);
			pShaders[i] = 0;
		}

		free(pShaders);

		DeleteStack();

		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		//unlink shader program
		glUseProgram(0);
	}




	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}