#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GL/gl.h>

#include"Header.h"
#include"vmath.h"

#define WINWIDTH 800
#define WINHEIGHT 600

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gbFile_SSD = NULL;
HWND ghwnd_SSD;
bool gbActiveWindow_SSD = false;
bool gbFullScreen_SSD = false;

HGLRC hglrc_SSD;
HDC ghdc_SSD;
DWORD dwStyle_SSD;
WINDOWPLACEMENT wPrev_SSD;
int WINDOWMAXWIDTH_SSD, WINDOWMAXHEIGHT_SSD, X_SSD, Y_SSD;

GLfloat angleTriangle;
GLfloat angleRectangle;

//shader variables
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;

using namespace vmath;

enum
{
	SSD_ATTRIBUTE_POSITION = 0,
	SSD_ATTRIBUTE_COLOR,
	SSD_ATTRIBUTE_NORMAL,
	SSD_ATTRIBUTE_TEXTURE,
};

GLuint vao_pyramid;
GLuint vbo_pyramid_position;
GLuint vbo_normal;
//GLuint vbo_pyramid_color;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

//key pressed uniform
GLuint LKeyPressedUniform;

//light uniforms
GLuint La1Uniform;
GLuint Ld1Uniform;
GLuint Ls1Uniform;
GLuint La2Uniform;
GLuint Ld2Uniform;
GLuint Ls2Uniform;

//material uniform
GLuint KaUniform;
GLuint KdUniform;
GLuint KsUniform;

//light Position Uniform
GLuint lightPositionUniform1;
GLuint lightPositionUniform2;

//shininess uniform
GLuint shininessUniform;

bool gbAnimate;
bool gbLighting;

typedef GLfloat vector3[3];
typedef GLfloat vector4[4];


struct Light {
	vec3 lightAmbientColor;
	vec3 lightDiffuseColor;
	vec3 lightSpecularColor;
	vec4 lightPosition;
};

struct Light light[2];


GLfloat shininess = 128;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };



vmath::mat4 perspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function declaration
	void Display(void);
	void Initialize(void);
	void Update(void);


	//variable declaration
	HWND hwnd_SSD;
	bool bDone_SSD = false;
	TCHAR szAppName_SSD[] = TEXT("MyWIndow");
	WNDCLASSEX wndclass_SSD;
	MSG msg_SSD;

	//Creation of the log file
	if (fopen_s(&gbFile_SSD, "logApp.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log file creation unsuccessful, exiting"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wPrev_SSD = { sizeof(WINDOWPLACEMENT) };

	wndclass_SSD.cbClsExtra = 0;
	wndclass_SSD.cbSize = sizeof(WNDCLASSEX);
	wndclass_SSD.cbWndExtra = 0;
	wndclass_SSD.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_SSD.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_SSD.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass_SSD.hInstance = hInstance;
	wndclass_SSD.lpfnWndProc = WndProc;
	wndclass_SSD.lpszClassName = szAppName_SSD;
	wndclass_SSD.lpszMenuName = NULL;
	wndclass_SSD.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass_SSD);

	WINDOWMAXWIDTH_SSD = GetSystemMetrics(SM_CXMAXIMIZED);
	WINDOWMAXHEIGHT_SSD = GetSystemMetrics(SM_CYMAXIMIZED);

	X_SSD = (WINDOWMAXWIDTH_SSD / 2) - (WINWIDTH / 2);
	Y_SSD = (WINDOWMAXHEIGHT_SSD / 2) - (WINHEIGHT / 2);

	hwnd_SSD = CreateWindowEx(WS_EX_APPWINDOW, szAppName_SSD, TEXT("practice"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X_SSD, Y_SSD,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_SSD = hwnd_SSD;

	Initialize();
	SetForegroundWindow(hwnd_SSD);
	SetFocus(hwnd_SSD);

	while (bDone_SSD == false)
	{
		if (PeekMessage(&msg_SSD, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_SSD.message == WM_QUIT)
				bDone_SSD = true;

			else
			{
				TranslateMessage(&msg_SSD);
				DispatchMessage(&msg_SSD);
			}
		}
		else
		{
			if (gbActiveWindow_SSD == true)
			{
				Display();

				if(gbAnimate == true)
				Update();
			}
		}
	}

	return (int)msg_SSD.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Resize(int, int);
	void ChangeDisplay(void);
	void Display(void);

	switch (iMsg)
	{


	case WM_SETFOCUS:
		gbActiveWindow_SSD = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow_SSD = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'A':
		case 'a':
			if (gbAnimate == false)
				gbAnimate = true;
			else
				gbAnimate = false;
			break;

		case 'l':
		case 'L':
			if (gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_PAINT:
		//Display();
		break;



	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//local variable declaration
	MONITORINFO mi_SSD = { sizeof(MONITORINFO) };
	//code

	if (gbFullScreen_SSD == false)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE); //getWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle_SSD & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd_SSD, &wPrev_SSD) && GetMonitorInfo(MonitorFromWindow(ghwnd_SSD, MONITORINFOF_PRIMARY), &mi_SSD))
			{
				SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd_SSD, HWND_TOP, mi_SSD.rcMonitor.left, mi_SSD.rcMonitor.top, (mi_SSD.rcMonitor.right - mi_SSD.rcMonitor.left), (mi_SSD.rcMonitor.bottom - mi_SSD.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);  //SetWindowPosition(ghwnd, HWND_TOP, mi.rcmonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED); //WM_NCCALCSIZE

			}
		}

		ShowCursor(FALSE);
		gbFullScreen_SSD = true;
	}

	else
	{
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

}

void Initialize(void)
{
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd_SSD;
	int iPixelFormatIndex;

	ghdc_SSD = GetDC(ghwnd_SSD);

	ZeroMemory(&pfd_SSD, sizeof(PIXELFORMATDESCRIPTOR));

	//initalizing pfd
	pfd_SSD.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd_SSD.nVersion = 1;
	pfd_SSD.iPixelType = PFD_TYPE_RGBA;
	pfd_SSD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd_SSD.cColorBits = 32;
	pfd_SSD.cRedBits = 8;
	pfd_SSD.cGreenBits = 8;
	pfd_SSD.cBlueBits = 8;
	pfd_SSD.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc_SSD, &pfd_SSD);
	if (iPixelFormatIndex == 0)
	{
		fprintf_s(gbFile_SSD, "ChoosePixelFormt() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (SetPixelFormat(ghdc_SSD, iPixelFormatIndex, &pfd_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "SetPixelFormat() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	hglrc_SSD = wglCreateContext(ghdc_SSD);
	if (hglrc_SSD == NULL)
	{
		fprintf_s(gbFile_SSD, "wglCreateContext() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	if (wglMakeCurrent(ghdc_SSD, hglrc_SSD) == FALSE)
	{
		fprintf_s(gbFile_SSD, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd_SSD);
	}

	GLenum glew_error = glewInit();

	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}

	fprintf_s(gbFile_SSD, "OpenGL vendor: %s\n", glGetString(GL_VENDOR));
	fprintf_s(gbFile_SSD, "OpenGL renderer: %s\n", glGetString(GL_RENDER));
	fprintf_s(gbFile_SSD, "OpenGL version: %s\n", glGetString(GL_VERSION));
	fprintf_s(gbFile_SSD, "OpenGL shading language version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExtensions;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf_s(gbFile_SSD, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//**vertex shader**
	//create shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//normal matrix is upper left 3*3 matrix of transpose of inverse of model view matrix

	//source code for vertex shader
	const GLchar* vertexShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int l_key_pressed;" \
		"uniform vec3 u_la_1;" \
		"uniform vec3 u_ld_1;" \
		"uniform vec3 u_ls_1;" \
		"uniform vec3 u_la_2;" \
		"uniform vec3 u_ld_2;" \
		"uniform vec3 u_ls_2;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec4 u_light_position_1;" \
		"uniform vec4 u_light_position_2;" \
		"uniform float u_shininess;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"int i;" \
		"vec3 light_direction_1;" \
		"vec3 reflection_vector_1;" \
		"vec3 light_direction_2;" \
		"vec3 reflection_vector_2;" \
		"vec3 ambient_light_1;" \
		"vec3 diffuse_light_1;" \
		"vec3 specular_light_1;" \
		"vec3 ambient_light_2;" \
		"vec3 diffuse_light_2;" \
		"vec3 specular_light_2;" \
		"if(l_key_pressed == 1)" \
		"{" \
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" \
		"light_direction_1 = normalize(vec3(u_light_position_1 - eye_cordinate));" \
		"light_direction_2 = normalize(vec3(u_light_position_2 - eye_cordinate));" \
		"reflection_vector_1 = reflect(-light_direction_1, trasformed_normal);" \
		"reflection_vector_2 = reflect(-light_direction_2, trasformed_normal);" \
		"ambient_light_1 = u_la_1 * u_ka;" \
		"diffuse_light_1 = u_ld_1 * u_kd * max(dot(light_direction_1, trasformed_normal), 0.0);" \
		"specular_light_1 = u_ls_1 * u_ks * pow(max(dot(reflection_vector_1, view_vector), 0.0), u_shininess);" \
		"ambient_light_2 = u_la_2 * u_ka;" \
		"diffuse_light_2 = u_ld_2 * u_kd * max(dot(light_direction_2, trasformed_normal), 0.0);" \
		"specular_light_2 = u_ls_2 * u_ks * pow(max(dot(reflection_vector_2, view_vector), 0.0), u_shininess);" \
		"fong_ads_light =  ambient_light_1 + diffuse_light_1 + specular_light_1;" \
		"fong_ads_light = fong_ads_light + ambient_light_2 + diffuse_light_2 + specular_light_2;" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix *  u_model_matrix * vPosition;" \
		"}";

	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSource, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//compilation error checking
	GLint shaderCompileStatus = 0;
	GLint shaderInfoLogLength = 0;
	GLchar* shaderInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);

		if (shaderInfoLogLength > 0)
		{
			shaderInfoLog = (GLchar*)malloc(shaderInfoLogLength);

			if (shaderInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(vertexShaderObject, shaderInfoLogLength, &written, shaderInfoLog);
				fprintf_s(gbFile_SSD, "vertex shader compilation log:%s\n", shaderInfoLog);
				free(shaderInfoLog);
				shaderInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	//**fragment Shader**
	//create shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \	
		"fragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSource, NULL);

	//compile fragment shader
	glCompileShader(fragmentShaderObject);

	shaderCompileStatus = 0;
	shaderInfoLogLength = 0;
	shaderInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);

		if (shaderInfoLogLength > 0)
		{
			shaderInfoLog = (GLchar*)malloc(shaderInfoLogLength);

			if (shaderInfoLog != NULL)
			{
				GLint written;
				glGetShaderInfoLog(fragmentShaderObject, shaderInfoLogLength, &written, shaderInfoLog);
				fprintf_s(gbFile_SSD, "fragment shader compilation log:%s\n", shaderInfoLog);
				free(shaderInfoLog);
				shaderInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}
		}
	}

	shaderProgramObject = glCreateProgram();

	//attching shaders with program object
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	//pre link binding of attributes with program object
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_NORMAL, "vNormal");
	//glBindAttribLocation(shaderProgramObject, SSD_ATTRIBUTE_COLOR, "vColor");

	//linking the program
	glLinkProgram(shaderProgramObject);

	GLint linkStatus = 0;
	GLint linkInfoLogLength = 0;
	GLchar* linkInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &linkInfoLogLength);

		if (linkInfoLogLength > 0)
		{
			GLint written;
			linkInfoLog = (GLchar*)malloc(linkInfoLogLength);

			if (linkInfoLog != NULL)
			{
				glGetProgramInfoLog(shaderProgramObject, linkInfoLogLength, &written, linkInfoLog);
				fprintf_s(gbFile_SSD, "%s\n", linkInfoLog);
				free(linkInfoLog);
				linkInfoLog = NULL;

				DestroyWindow(ghwnd_SSD);
			}

		}
	}

	//binding of uniform with program
	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	La1Uniform = glGetUniformLocation(shaderProgramObject, "u_la_1");
	Ld1Uniform = glGetUniformLocation(shaderProgramObject, "u_ld_1");
	Ls1Uniform = glGetUniformLocation(shaderProgramObject, "u_ls_1");
	La2Uniform = glGetUniformLocation(shaderProgramObject, "u_la_2");
	Ld2Uniform = glGetUniformLocation(shaderProgramObject, "u_ld_2");
	Ls2Uniform = glGetUniformLocation(shaderProgramObject, "u_ls_2");
	KaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	KdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	KsUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	LKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "l_key_pressed");
	lightPositionUniform1 = glGetUniformLocation(shaderProgramObject, "u_light_position_1");
	lightPositionUniform2 = glGetUniformLocation(shaderProgramObject, "u_light_position_2");
	shininessUniform = glGetUniformLocation(shaderProgramObject, "u_shininess");


	light[0].lightAmbientColor = vec3(0.0f, 0.0f, 0.0f);
	light[0].lightDiffuseColor = vec3(1.0f, 0.0f, 0.0f);
	light[0].lightSpecularColor = vec3(1.0f, 0.0f, 0.0f);
	light[0].lightPosition = vec4(-2.0f, 0.0f, 0.0f, 1.0f);

	light[1].lightAmbientColor = vec3(0.0f, 0.0f, 0.0f);
	light[1].lightDiffuseColor = vec3(0.0f, 0.0f, 1.0f);
	light[1].lightSpecularColor = vec3(0.0f, 0.0f, 1.0f);
	light[1].lightPosition = vec4(2.0f, 0.0f, 0.0f, 1.0f);

	const GLfloat pyramidVertices[] =
	{
		// front
	0.0f, 1.0f, 0.0f,
	   -1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,


	// right
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,


	// back
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, -1.0f,
	   -1.0f, -1.0f, -1.0f,


	   // left
	   0.0f, 1.0f, 0.0f,
		  -1.0f, -1.0f, -1.0f,
		  -1.0f, -1.0f, 1.0f
	};

	
	const GLfloat pyramid_normals[] =
	{
		//Front Face	
	0.0f, 0.447214f, 0.894427f, //top
	0.0f, 0.447214f, 0.894427f, //left
	0.0f, 0.447214f, 0.894427f, //right

	//Right Face	
	0.894427f, 0.447214f, 0.0f, //top	
	0.894427f, 0.447214f, 0.0f, //left
	0.894427f, 0.447214f, 0.0f, //right

	//Back Face
	0.0f, 0.447214f, -0.894427f, //top
	0.0f, 0.447214f, -0.894427f, //left
	0.0f, 0.447214f, -0.894427f, //right

	//Left Face
	-0.894427f, 0.447214f, 0.0f, //top
	-0.894427f, 0.447214f, 0.0f, //left
	-0.894427f, 0.447214f, 0.0f, //right

	};
	

	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);

	glGenBuffers(1, &vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSD_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSD_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	
	gbAnimate = false;
	gbLighting = false;

	//set clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//perspective projection
	perspectiveProjectionMatrix = mat4::identity();

	Resize(WINWIDTH, WINHEIGHT);


}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void Display(void)
{
	//variable declaration
	/*struct Light {
	vec3 lightAmbientColor;
	vec3 lightDiffuseColor;
	vec3 lightSpecularColor;
	vec4 lightPosition;
};*/

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	//lighting calculations
	vec3 ambientLight[2], diffuseLight[2], specularLight[2];
	vec4 lightPosition[2];

	ambientLight[0] = light[0].lightAmbientColor;
	ambientLight[1] = light[1].lightAmbientColor;

	diffuseLight[0] = light[0].lightDiffuseColor;
	diffuseLight[1] = light[1].lightDiffuseColor;

	specularLight[0] = light[0].lightSpecularColor;
	specularLight[1] = light[1].lightSpecularColor;

	lightPosition[0] = light[0].lightPosition;
	lightPosition[1] = light[1].lightPosition;

	if (gbLighting == true)
	{
		glUniform1i(LKeyPressedUniform, 1);
		glUniform3fv(La1Uniform, 1, light[0].lightAmbientColor);
		glUniform3fv(Ld1Uniform, 1, light[0].lightDiffuseColor);
		glUniform3fv(Ls1Uniform, 1, light[0].lightSpecularColor);
		glUniform3fv(La2Uniform, 1, light[1].lightAmbientColor);
		glUniform3fv(Ld2Uniform, 1, light[1].lightDiffuseColor);
		glUniform3fv(Ls2Uniform, 1, light[1].lightSpecularColor);
		glUniform3fv(KaUniform, 1, materialAmbient);
		glUniform3fv(KdUniform, 1, materialDiffuse);
		glUniform3fv(KsUniform, 1, materialSpecular);
		glUniform4fv(lightPositionUniform1, 1, light[0].lightPosition);
		glUniform4fv(lightPositionUniform2, 1, light[1].lightPosition);
		glUniform1f(shininessUniform, shininess);

	}
	else
	{
		glUniform1i(LKeyPressedUniform, 0);
	}

	//OpenGL drawing

	//triangle
	mat4 modelMatrix = mat4::identity();
	mat4 translateMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	translateMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	rotationMatrix = vmath::rotate(angleTriangle, 0.0f, 1.0f, 0.0f);

	modelMatrix = translateMatrix * rotationMatrix;


	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(vao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	

	glUseProgram(0);

	SwapBuffers(ghdc_SSD);

}

void Update(void)
{
	if (angleTriangle <= 360.0f)
		angleTriangle += 0.3f;
	else
		angleTriangle = 0.0f;

	if (angleRectangle <= 360.0f)
		angleRectangle += 0.3f;
	else
		angleRectangle = 0.0f;

}

void Uninitialize(void)
{
	//file IO code

	if (gbFullScreen_SSD == true)
	{
		dwStyle_SSD = GetWindowLong(ghwnd_SSD, GWL_STYLE);
		SetWindowLong(ghwnd_SSD, GWL_STYLE, (dwStyle_SSD | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd_SSD, &wPrev_SSD);
		SetWindowPos(ghwnd_SSD, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
		ShowCursor(TRUE);
		gbFullScreen_SSD = false;
	}

	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}

	if (vbo_pyramid_position)
	{
		glDeleteBuffers(1, &vbo_pyramid_position);
		vbo_pyramid_position = 0;
	}

	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
		GLsizei shaderCount;

		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShaders = NULL;
		pShaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));

		if (pShaders == NULL)
		{
			//detach vertex shader  from shader program object
			glDetachShader(shaderProgramObject, vertexShaderObject);

			//detach fragment shader from shader program object
			glDetachShader(shaderProgramObject, fragmentShaderObject);

			//delete shader
			glDeleteShader(vertexShaderObject);
			vertexShaderObject = 0;

			glDeleteShader(fragmentShaderObject);
			fragmentShaderObject = 0;

			//delete program object
			glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			glUseProgram(0);
		}

		glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);

		for (GLsizei i = 0; i < shaderCount; i++)
		{
			glDetachShader(shaderProgramObject, pShaders[i]);
			glDeleteShader(pShaders[i]);
			pShaders[i] = 0;
		}

		free(pShaders);

		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;

		//unlink shader program
		glUseProgram(0);
	}




	if (wglGetCurrentContext() == hglrc_SSD)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (hglrc_SSD)
	{
		wglDeleteContext(hglrc_SSD);
		hglrc_SSD = NULL;
	}

	if (ghdc_SSD)
	{
		ReleaseDC(ghwnd_SSD, ghdc_SSD);
		ghdc_SSD = NULL;
	}


	if (gbFile_SSD)
	{
		fprintf_s(gbFile_SSD, "log file closed successfully\n");
		fclose(gbFile_SSD);
		gbFile_SSD = NULL;
	}

}