//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_position;
var vbo_normal;



//uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionmatrixUniform;
var laUniform;
var ldUniform;
var lsUniform;
var kaUniform;
var kdUniform;
var ksUniform;
var shininessUniform;
var lightPositionUniform;
var lKeyPressedUniform;

var yRotate_pyramid;


    var materialAmbient = [ 0.0, 0.0, 0.0 ];
    var materialDiffuse = [ 1.0, 1.0, 1.0];
    var materialSpecular = [ 1.0, 1.0, 1.0 ];
	
	var ambientLight =  [0.0, 0.0, 0.0,0.0, 0.0, 0.0 ];;
	var diffuseLight = [1.0, 0.0, 0.0,0.0, 0.0, 1.0];
	var specularLight = [1.0, 0.0, 0.0, 0.0, 0.0, 1.0];
	var lightPosition = [-2.0, 0.0, 0.0, 1.0, 2.0, 0.0, 0.0, 1.0];
	var shininess = 128.0;

var perspectiveProjectionMatrix = null; 

var gbLighting = false;

var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
		case 108:
		case 76:
			if(gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
		break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec3 u_la[2];" +
		"uniform mediump vec3 u_ld[2];" +
		"uniform mediump vec3 u_ls[2];" +
		"uniform mediump vec3 u_kd;" +
		"uniform mediump vec3 u_ka;" +
		"uniform mediump vec3 u_ks;" +
		"uniform mediump vec4 u_light_position[2];" +
		"uniform mediump float u_shininess;" +
		"out vec3 fong_ads_light;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"vec3 light_direction[2];" +
		"vec3 reflection_vector[2];" +
		"vec3 ambient_light[2];" +
		"vec3 diffuse_light[2];" +
		"vec3 specular_light[2];" +
		"if(l_key_pressed == 1)" +
		"{" +
		"fong_ads_light = vec3(0.0, 0.0, 0.0);" +
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" +
		"for(i = 0; i < 2; i++)" +
		"{" +
		"light_direction[i] = normalize(vec3(u_light_position[i] - eye_cordinate));" +
		"reflection_vector[i] = reflect(-light_direction[i], trasformed_normal);" +
		"ambient_light[i] = u_la[i] * u_ka;" +
		"diffuse_light[i] = u_ld[i] * u_kd * max(dot(light_direction[i], trasformed_normal), 0.0);" +
		"specular_light[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i], view_vector), 0.0), u_shininess);" +
		"fong_ads_light =  fong_ads_light + ambient_light[i] + diffuse_light[i] + specular_light[i];" +
		"}" +
		"}" +
		"else" +
		"{" +
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix *  u_model_matrix * vPosition;" +
		"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec3 fong_ads_light;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"fragColor = vec4(fong_ads_light, 1.0);" +
		"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
	
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "l_key_pressed");
	shininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	
	
	var pyramidVertices = new Float32Array([
	 0.0, 1.0, 0.0,
	 -1.0, -1.0, 1.0,
	1.0, -1.0, 1.0,


	// right
	0.0, 1.0, 0.0,
	1.0, -1.0, 1.0,
	1.0, -1.0, -1.0,


	// back
	0.0, 1.0, 0.0,
	1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,


	   // left
	 0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,
	-1.0, -1.0, 1.0
	]);
	
	var pyramidNormals = new Float32Array([
	0.0, 0.447214, 0.894427, //top
	0.0, 0.447214, 0.894427, //left
	0.0, 0.447214, 0.894427, //right

	//Right Face	
	0.894427, 0.447214, 0.0, //top	
	0.894427, 0.447214, 0.0, //left
	0.894427, 0.447214, 0.0, //right

	//Back Face
	0.0, 0.447214, -0.894427, //top
	0.0, 0.447214, -0.894427, //left
	0.0, 0.447214, -0.894427, //right

	//Left Face
	-0.894427, 0.447214, 0.0, //top
	-0.894427, 0.447214, 0.0, //left
	-0.894427, 0.447214, 0.0 //right
	]);
	
	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_normal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normal);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidNormals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	yRotate_pyramid = 0.0;
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	
	if(gbLighting == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, ambientLight);
		gl.uniform3fv(ldUniform, diffuseLight);
		gl.uniform3fv(lsUniform, specularLight);
		gl.uniform3fv(kaUniform, materialAmbient);
		gl.uniform3fv(kdUniform, materialDiffuse);
		gl.uniform3fv(ksUniform, materialSpecular);
		gl.uniform1f(shininessUniform, shininess);
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
		gl.uniform1i(lKeyPressedUniform, 0);
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var translateMatrix = mat4.create();
	mat4.rotateY(rotateMatrix, rotateMatrix, degree_to_radians(yRotate_pyramid));
	mat4.translate(translateMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	mat4.multiply(modelMatrix, translateMatrix, rotateMatrix);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionmatrixUniform, false, perspectiveProjectionMatrix);
	
   gl.bindVertexArray(vao_pyramid);
	
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
    
	gl.useProgram(null);
	
	yRotate_pyramid += 1.5;
	
	requestAnimationFrame(display, canvas);
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}


function uninitialize()
{
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}
	
	if(vbo_normal)
	{
		gl.deleteBuffer(vbo_normal);
		vbo_normal = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}