//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;


//uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionmatrixUniform;
var laUniform;
var ldUniform;
var lsUniform;
var kaUniform;
var kdUniform;
var ksUniform;
var shininessUniform;
var lightPositionUniform;
var lKeyPressedUniform;

var xRotate = 0.0;
var yRotate = 0.0;
var zRotate = 0.0;


    var materialAmbient = [ 0.0, 0.0, 0.0 ];
    var materialDiffuse = [ 1.0, 1.0, 1.0];
    var materialSpecular = [ 1.0, 1.0, 1.0 ];
	
	var radius = 100.0;
	
	var shininess = 128.0;

var perspectiveProjectionMatrix = null; 

var gbLighting = false;

var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
		case 108:
		case 76:
			if(gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
		break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec4 u_light_position[3];" +
		"out vec4 eye_cordinate;" +
		"out vec3 trasformed_normal;" +
		"out vec3 light_direction[3];" +
		"out vec3 view_vector;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"if(l_key_pressed == 1)" +
		"{" +
		"eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"trasformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"for(i = 0; i < 3; i++)" +
		"{" +
		"light_direction[i] = vec3(u_light_position[i] - eye_cordinate);" +
		"}" +
		"view_vector = vec3(-eye_cordinate);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	 "#version 300 es" +
		"\n" +
		"precision highp float;" +
		"vec3 fong_ads_lighting;" +
		"in vec4 eye_cordinate;" +
		"in vec3 trasformed_normal;" +
		"in vec3 light_direction[3];" +
		"in vec3 view_vector;" +
		"uniform vec3 u_la[3];" +
		"uniform vec3 u_ld[3];" +
		"uniform vec3 u_ls[3];" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shininess;" +
		"uniform mediump int l_key_pressed;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"vec3 normalised_light_direction[3];" +
		"vec3 refliection_vector[3];" +
		"vec3 ambient[3];" +
		"vec3 diffuse[3];" +
		"vec3 specular[3];" +
		"if(l_key_pressed == 1)" +
		"{" +
		"vec3 normalised_transformed_normal = normalize(trasformed_normal);" +
		"vec3 normalized_view_vector = normalize(view_vector);" +
		"for(i = 0; i < 3; i++)" +
		"{" + 
		"normalised_light_direction[i] = normalize(light_direction[i]);" +
		"refliection_vector[i] = reflect(-normalised_light_direction[i], normalised_transformed_normal);" +
		"ambient[i] = u_la[i] * u_ka;" +
		"diffuse[i] = u_ld[i] * u_kd * max(dot(normalised_light_direction[i], normalised_transformed_normal), 0.0);" +
		"specular[i] = u_ls[i] * u_ks* pow(max(dot(refliection_vector[i], normalized_view_vector), 0.0), u_shininess);" +
		"fong_ads_lighting = fong_ads_lighting + ambient[i] + diffuse[i] + specular[i];" +
		"}" +
		"}" +
		"else" +
		"{" +
		"fong_ads_lighting = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"fragColor = vec4(fong_ads_lighting, 1.0);" +
		"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
	
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "l_key_pressed");
	shininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	
	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 50, 50);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	//variable declaration
	var ambientLight = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
	var diffuseLight = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0];
	var specularLight = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0];
	
	var lightPosition = [0.0, radius * Math.sin(degree_to_radians(xRotate)), radius * Math.cos(degree_to_radians(xRotate)), 1.0, 
	radius * Math.sin(degree_to_radians(zRotate)), radius * Math.cos(degree_to_radians(zRotate)),0.0, 1.0,
	radius * Math.cos(degree_to_radians(yRotate)), 0.0, radius * Math.sin(degree_to_radians(yRotate)), 1.0]; 
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	
	if(gbLighting == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, ambientLight);
		gl.uniform3fv(ldUniform, diffuseLight);
		gl.uniform3fv(lsUniform, specularLight);
		gl.uniform3fv(kaUniform, materialAmbient);
		gl.uniform3fv(kdUniform, materialDiffuse);
		gl.uniform3fv(ksUniform, materialSpecular);
		gl.uniform1f(shininessUniform, shininess);
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
		gl.uniform1i(lKeyPressedUniform, 0);
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionmatrixUniform, false, perspectiveProjectionMatrix);
	
    sphere.draw();
    
	gl.useProgram(null);
	
	update();
	
	
	requestAnimationFrame(display, canvas);
}


function update()
{
	if(xRotate < 360.0)
		xRotate += 3.5;
	else
		xRotate = 0.0;
	
	if(yRotate < 360.0)
		yRotate += 3.5;
	else
		yRotate = 0.0;
	
	if(zRotate < 360.0)
		zRotate += 3.5;
	else
		zRotate = 0.0;
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	
	if(sphere)
		sphere.deallocate();
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}