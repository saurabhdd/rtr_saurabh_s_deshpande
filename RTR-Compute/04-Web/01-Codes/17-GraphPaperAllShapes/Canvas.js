//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vao_y;
var vbo_position;
var vbo_position_middle_color;
var vao_middle;
var vbo_position_y;
var vbo_position_middle;
var vao_circle;
var vao_rectangle;
var vao_triangle;
var vbo_position_circle;
var vbo_position_rectangle;
var vbo_position_triangle;
var vbo_color_circle;
var vbo_color_rectangle;
var vbo_color_triangle;
var mvpUniform;

var xRotate_cube;
var yRotate_pyramid;

var perspectiveProjectionMatrix = null; 


var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_color = vColor;" +
	"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +
	"fragColor = out_color;" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_COLOR, "vColor");
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	
	var graphXVertices = new Float32Array(240);
	var i = 0.0;
	var counter = 0;
	
	for(i = i + (1.0 / 21.0); i <= 1.0; i = i + (1.0 / 20.0))
	{
		graphXVertices[counter++]  = i;
		
		if(counter % 2 != 0)
			graphXVertices[counter++] = -1.0;
		else
			graphXVertices[counter++] = 1.0;
		graphXVertices[counter++] = 0.0;
		
		
		
		graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0;
		 else
			 graphXVertices[counter++] = 1.0;
		 graphXVertices[counter++] = 0.0;
		 
	}
	
	i = 0;
    for (i = i - (1.0 / 21.0); i >= -1.0; i = i - (1.0 / 20.0))
	 {
		 graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0;
		 else
			 graphXVertices[counter++] = 1.0;
		 graphXVertices[counter++] = 0.0;


		
		 graphXVertices[counter++] = i;

		 if (counter % 2 != 0)
			 graphXVertices[counter++] = -1.0;
		 else
			 graphXVertices[counter++] = 1.0;
		 graphXVertices[counter++] = 0.0;

		 
	 }
	
	
	i = 0.0;
	counter = 0;
	var graphYVertices = new Float32Array(240);
	
	for (i = i + (1.0 / 21.0); i <= 1.0; i = i + (1.0 / 20.0))
	 {
		 
		 if(counter % 2 != 0)
			 graphYVertices[counter++] = -1.0;
		 else
			 graphYVertices[counter++] = 1.0;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0;




		 if(counter % 2 != 0)
			 graphYVertices[counter++] = -1.0;
		 else
			 graphYVertices[counter++] = 1.0;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0;

	 }
	 
	 i = 0.0;
	 for (i = i - (1.0 / 21.0); i >= -1.0; i = i - (1.0 / 20.0))
	 {
		 if(counter % 2 != 0)
			 graphYVertices[counter++] = -1.0;
		 else
			 graphYVertices[counter++] = 1.0;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0;




		 if(counter % 2 != 0)
			 graphYVertices[counter++] = -1.0;
		 else
			 graphYVertices[counter++] = 1.0;

		 graphYVertices[counter++] = i;

		 graphYVertices[counter++] = 0.0;

	 }
	
	
	
	var middleLines = new Float32Array([
									0.0, 1.0, 0.0,
									0.0, -1.0, 0.0,
									1.0, 0.0, 0.0,
									-1.0, 0.0, 0.0
									]);
									
									
    var middleColors = new Float32Array([
									0.0, 1.0, 0.0, 
									0.0, 1.0, 0.0, 
									0.0, 1.0, 0.0,
									0.0, 1.0, 0.0
									]);
	
	
	
	//circle vertices
	var circleVertices = new Float32Array(300);
	var radius = 0.5;
	counter = 0;
	for ( i = 0.0; i <= 2.0 * Math.PI; i = i + (2.0 * Math.PI / 100))
	 {
		 circleVertices[counter++] = radius * Math.cos(i);
		 circleVertices[counter++] = radius * Math.sin(i);
		 circleVertices[counter++] = 0.0;
	 }
	
	
	//circle colors
	var circleColors = new Float32Array(300);
	counter = 0;
	for (let j = 0; j < 100; j++)
	 {
		 circleColors[counter++] = 0.0;
		 circleColors[counter++] = 1.0;
		 circleColors[counter++] = 0.0;
	 }
	
	
	//rectangle vertices
	const RECTANGLE_SIDE = radius / 1.414;
	
	var rectangleVertices = new Float32Array([
	parseFloat(RECTANGLE_SIDE), parseFloat(RECTANGLE_SIDE), 0.0,
	parseFloat(-RECTANGLE_SIDE), parseFloat(RECTANGLE_SIDE), 0.0,
	parseFloat(-RECTANGLE_SIDE), parseFloat(-RECTANGLE_SIDE), 0.0,
	parseFloat(RECTANGLE_SIDE), parseFloat(-RECTANGLE_SIDE), 0.0
	]);
	
	
	//rectangle colors
	var rectangleColors = new Float32Array([
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0, 
	0.0, 1.0, 0.0
	]);
	
	
	var triangleVertices = new Float32Array([
	parseFloat(-RECTANGLE_SIDE), parseFloat(-RECTANGLE_SIDE), 0.0,
	parseFloat(RECTANGLE_SIDE), parseFloat(-RECTANGLE_SIDE), 0.0, 
	0.0, parseFloat(RECTANGLE_SIDE), 0.0]);
	
	
	
	var triangleCOlors = new Float32Array([
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0, 
	0.0, 1.0, 0.0
	]);
	
	
	
	
	
	vao_middle = gl.createVertexArray();
	gl.bindVertexArray(vao_middle);
	
	vbo_position_middle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_middle);
	gl.bufferData(gl.ARRAY_BUFFER, middleLines, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	
	
	vbo_position_middle_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_middle_color);
	gl.bufferData(gl.ARRAY_BUFFER, middleColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);																			   
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	
	
	//vao_y
	vao_y = gl.createVertexArray();
	gl.bindVertexArray(vao_y);
	
	vbo_position_y = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_y);
	gl.bufferData(gl.ARRAY_BUFFER, graphYVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.vertexAttrib3f(WebGLMacros.SSD_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
	
	gl.bindVertexArray(null);
	
	
	
	
	vao = gl.createVertexArray();
	gl.bindVertexArray(vao);
	
	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, graphXVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.vertexAttrib3f(WebGLMacros.SSD_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
	
	gl.bindVertexArray(null);
	
	
	//circle vao
	vao_circle = gl.createVertexArray();
	gl.bindVertexArray(vao_circle);
	
	vbo_position_circle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_circle);
	gl.bufferData(gl.ARRAY_BUFFER, circleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_color_circle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_circle);
	gl.bufferData(gl.ARRAY_BUFFER, circleColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);
	
	
	
	//rectangle vao
	vao_rectangle = gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);
	
	vbo_position_rectangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_rectangle);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_color_rectangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_rectangle);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);
	
	
	//vao triangle
	vao_triangle = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);
	
	vbo_position_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_triangle);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_color_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_triangle);
	gl.bufferData(gl.ARRAY_BUFFER, triangleCOlors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);
	
	
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	

	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao);
	gl.drawArrays(gl.LINES, 0, 80);
	gl.bindVertexArray(null);
	
	gl.bindVertexArray(vao_y);
	gl.drawArrays(gl.LINES, 0, 80);
	gl.bindVertexArray(null);
	
	gl.bindVertexArray(vao_middle);
	gl.drawArrays(gl.LINES, 0, 4);
	gl.bindVertexArray(null);
	
	
	gl.bindVertexArray(vao_circle);
	gl.drawArrays(gl.LINE_LOOP, 0, 100);
	gl.bindVertexArray(null);

	gl.bindVertexArray(vao_rectangle);
	gl.drawArrays(gl.LINE_LOOP, 0, 4);
	gl.bindVertexArray(null);

	gl.bindVertexArray(vao_triangle);
	gl.drawArrays(gl.LINE_LOOP, 0, 3);
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);

    
	
	requestAnimationFrame(display, canvas);
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao_y)
	{
		gl.deleteVertexArray(vao_y);
		vao_y = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	if(vbo_position_middle_color)
	{
		gl.deleteBuffer(vbo_position_middle_color);
		vbo_position_middle_color = null;
	}
	
	if(vao_middle)
	{
		gl.deleteVertexArray(vao_middle);
		vao_middle = null;
	}
	
	
	if(vbo_position_middle)
	{
		gl.deleteBuffer(vbo_position_middle);
		vbo_position_middle = null;
	}
	
	if(vbo_position_y)
	{
		gl.deleteBuffer(vbo_position_y);
		vbo_position_y = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}