//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_position_cube;
var vbo_texture_cube;
var vao_pyramid;
var vbo_position_pyramid;
var vbo_texture_pyramid;
var mvpUniform;
var textureSamplerUniform;

var pyramid_texture;
var kundali_texture;

var xRotate_cube;
var yRotate_pyramid;

var perspectiveProjectionMatrix = null; 




var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec2 vTexCoords;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec2 out_TexCoords;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_TexCoords = vTexCoords;" +
	"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec2 out_TexCoords;" +
	"uniform highp sampler2D u_texture_sampler;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +
	"fragColor = texture(u_texture_sampler, out_TexCoords);" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_TEXTURE, "vTexCoords");
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_texture_sampler");
	
	
	//triangle vertices
	
    var cubeVertices = new Float32Array([
											-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
		1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, 1.0, -1.0,
		-1.0, -1.0, 1.0,
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, -1.0, 1.0
											]);
											
	

    var cubeTexCoords = new Float32Array([
	0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0
		]);

    var pyramidVertices = new Float32Array([
											0.0, 1.0, 0.0,
											-1.0, -1.0, 1.0,
											1.0, -1.0, 1.0,
											0.0, 1.0, 0.0,
											1.0, -1.0, 1.0,
											1.0, -1.0, -1.0,
											0.0, 1.0, 0.0,
											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,
											0.0, 1.0, 0.0,
											-1.0, -1.0, -1.0,
											-1.0, -1.0, 1.0
											]);
	
	
	var pyramidTexCoords = new Float32Array([
										0.5, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		0.5, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.5, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.5, 1.0,
		0.0, 0.0,
		1.0, 0.0
										]);
	
	
	
	//vao_cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	vbo_position_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_cube);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_texture_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexCoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_TEXTURE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	
	//triangle vao
	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	
	vbo_position_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	
	
	vbo_texture_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexCoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);																			   
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_TEXTURE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
		
	xRotate_cube = 0.0;
	yRotate_pyramid = 0.0;
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	
	pyramid_texture = gl.createTexture();
	kundali_texture  = gl.createTexture();
	
	pyramid_texture.image = new Image();
	kundali_texture.image = new Image();
	
	kundali_texture.image.src = "Vijay_Kundali.png";
	pyramid_texture.image.src = "stone.png";
	
	//lambda, closure or block
	//function pointer samor nav na lihita directly body
	pyramid_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pyramid_texture.image);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	
	kundali_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, kundali_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, kundali_texture.image);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	

	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	//drawing pyramid
	mat4.rotateY(rotateMatrix, rotateMatrix, degree_to_radians(yRotate_pyramid));
	mat4.translate(translateMatrix, translateMatrix, [-2.0, 0.0, -6.0]);
	mat4.multiply(modelViewMatrix, translateMatrix, rotateMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	//texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
	gl.uniform1i(textureSamplerUniform, 0);
	
	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	
	
	//drawing cube
	mat4.identity(modelViewMatrix);
	mat4.identity(translateMatrix);
	mat4.identity(rotateMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.rotateX(rotateMatrix, rotateMatrix, degree_to_radians(xRotate_cube));
	mat4.translate(modelViewMatrix, modelViewMatrix, [2.0, 0.0, -6.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	//texture
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, kundali_texture);
	gl.uniform1i(textureSamplerUniform, 0);
	
	gl.bindVertexArray(vao_cube);
	
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);

    if(yRotate_pyramid < 360.0)
		yRotate_pyramid += 2.0;
	else
		yRotate_pyramid = 0.0;
	
	
	if(xRotate_cube < 360.0)
		xRotate_cube += 2.0;
	else
		xRotate_cube = 0.0;
	
	requestAnimationFrame(display, canvas);
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}
	
	if(vbo_position_cube)
	{
		gl.deleteBuffer(vbo_position_cube);
		vbo_position_cube = null;
	}
	
	if(vbo_texture_cube)
	{
		gl.deleteBuffer(vbo_texture_cube);
		vbo_texture_cube = null;
	}
	
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}
	
	
	if(vbo_texture_pyramid)
	{
		gl.deleteBuffer(vbo_texture_pyramid);
		vbo_texture_pyramid = null;
	}
	
	if(vbo_position_pyramid)
	{
		gl.deleteBuffer(vbo_position_pyramid);
		vbo_position_pyramid = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}