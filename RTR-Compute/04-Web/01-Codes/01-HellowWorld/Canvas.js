function main()
{
	//get canvas from DOM
	var canvas = document.getElementById("SSD");
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of information
	console.log("canvas width = " + canvas.width + "\n canvas height = " + canvas.height);
	
	//get drawing context from the canvas
	var context = canvas.getContext("2d");
	
	if(!context)
	{
		console.log("Obtaining context failed\n");
	}else
		console.log("Obtaining context succeeded\n");
	
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	//center the text
	
	context.textAlign = "center";
	
	//vertical center
	context.textBaseline = "middle";
	
	context.font = "48px sans-serif";
	
	//declare the string to be displayed
	var str = "Hello World !!!";
	
	//color the text
	context.fillStyle = "green";
	
	//disply the text
	context.fillText(str, canvas.width / 2, canvas.height / 2);
	
	
	
}