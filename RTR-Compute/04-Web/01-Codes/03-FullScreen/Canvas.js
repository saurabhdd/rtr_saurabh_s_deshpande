//global variables
var canvas = null;
var context = null;


function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of information
	console.log("canvas width = " + canvas.width + "\n canvas height = " + canvas.height);
	
	//get drawing context from the canvas
    context = canvas.getContext("2d");
	
	if(!context)
	{
		console.log("Obtaining context failed\n");
	}else
		console.log("Obtaining context succeeded\n");
	
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	//center the text
	drawText("Hello World!!!");
	 
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			drawText("Hello World!!!");  //as there is no repaint event in js based browser in windowing 
			break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}

function drawText(text)
{
	context.textAlign = "center";
	context.textBaseline = "middle";
	
	context.font = "48px sans-serif";
	
	context.fillStyle = "green";
	
	//disply the text
	context.fillText(text, canvas.width / 2, canvas.height / 2);
}

function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}



	
