//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4,
};

const toggleShaderMacro = 
{
	PER_VERTEX : 1,
	PER_FRAGMENT: 2
};

var vertexShaderObject_f;
var fragmentShaderObject_f;
var shaderProgramObject_f;
var vertexShaderObject_f_v;
var fragmentShaderObject_f_v;
var shaderProgramObject_f_v;


var sphere = null;

var toggleShader = 0;


//uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionmatrixUniform;
var laUniform;
var ldUniform;
var lsUniform;
var kaUniform;
var kdUniform;
var ksUniform;
var shininessUniform;
var lightPositionUniform;
var lKeyPressedUniform;



    var materialAmbient = [ 0.0, 0.0, 0.0 ];
    var materialDiffuse = [ 1.0, 1.0, 1.0];
    var materialSpecular = [ 1.0, 1.0, 1.0 ];
	
	var lightPosition = [ 100.0, 100.0, 100.0, 1.0 ];
	var ambientLight = [ 0.0, 0.0, 0.0 ];
	var diffuseLight = [ 1.0, 1.0, 1.0];
	var specularLight = [ 0.7, 0.7, 0.7];
	var shininess = 128.0;

var perspectiveProjectionMatrix = null; 

var gbLighting = false;

var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
		case 108:
		case 76:
			if(gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
		break;
		
		case 84:
		case 116:
			if(toggleShader == 1)
				toggleShader = 2;
			else if(toggleShader == 2)
				toggleShader = 1;
			else
				toggleShader = 1;
			
		break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode_f = 
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec4 u_light_position;" +
		"out vec4 eye_cordinate;" +
		"out vec3 trasformed_normal;" +
		"out vec3 light_direction;" +
		"out vec3 view_vector;" +
		"void main(void)" +
		"{" +
		"if(l_key_pressed == 1)" +
		"{" +
		"eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"trasformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"light_direction = vec3(u_light_position - eye_cordinate);" +
		"view_vector = vec3(-eye_cordinate);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";
	
	
	vertexShaderObject_f = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_f, vertexShaderSourceCode_f);
	gl.compileShader(vertexShaderObject_f);
	
	if(gl.getShaderParameter(vertexShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre_f = 
	 "#version 300 es" +
		"\n" +
		"precision highp float;" +
		"vec3 fong_ads_lighting;" +
		"in vec4 eye_cordinate;" +
		"in vec3 trasformed_normal;" +
		"in vec3 light_direction;" +
		"in vec3 view_vector;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shininess;" +
		"uniform mediump int l_key_pressed;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"if(l_key_pressed == 1)" +
		"{" +
		"vec3 normalised_transformed_normal = normalize(trasformed_normal);" +
		"vec3 normalised_light_direction = normalize(light_direction);" +
		"vec3 normalized_view_vector = normalize(view_vector);" +
		"vec3 refliection_vector = reflect(-normalised_light_direction, normalised_transformed_normal);" +
		"vec3 ambient = u_la * u_ka;" +
		"vec3 diffuse = u_ld * u_kd * max(dot(normalised_light_direction, normalised_transformed_normal), 0.0);" +
		"vec3 specular = u_ls * u_ks* pow(max(dot(refliection_vector, normalized_view_vector), 0.0), u_shininess);" +
		"fong_ads_lighting = ambient + diffuse + specular;" +
		"}" +
		"else" +
		"{" +
		"fong_ads_lighting = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"fragColor = vec4(fong_ads_lighting, 1.0);" +
		"}";
	
	fragmentShaderObject_f = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_f, fragmentShaderSoucre_f);
	gl.compileShader(fragmentShaderObject_f);
	
	if(gl.getShaderParameter(fragmentShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject_f = gl.createProgram();
	gl.attachShader(shaderProgramObject_f, vertexShaderObject_f);
	gl.attachShader(shaderProgramObject_f, fragmentShaderObject_f);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
	
	
	//link program
	gl.linkProgram(shaderProgramObject_f);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject_f, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	
	
	//per vertex program
	var vertexShaderSourceCode_v = 
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec3 u_la;" +
		"uniform mediump vec3 u_ld;" +
		"uniform mediump vec3 u_ls;" +
		"uniform mediump vec3 u_kd;" +
		"uniform mediump vec3 u_ka;" +
		"uniform mediump vec3 u_ks;" +
		"uniform mediump vec4 u_light_position;" +
		"uniform mediump float u_shininess;" +
		"out vec3 fong_ads_light;" +
		"void main(void)" +
		"{" +
		"vec3 light_direction;" +
		"vec3 reflection_vector;" +
		"vec3 ambient_light;" +
		"vec3 diffuse_light;" +
		"vec3 specular_light;" +
		"if(l_key_pressed == 1)" +
		"{" +
		"vec4 eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"vec3 trasformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		"vec3 view_vector = normalize(vec3(-eye_cordinate));" +
		"light_direction = normalize(vec3(u_light_position - eye_cordinate));" +
		"reflection_vector = reflect(-light_direction, trasformed_normal);" +
		"ambient_light = u_la * u_ka;" +
		"diffuse_light = u_ld * u_kd * max(dot(light_direction, trasformed_normal), 0.0);" +
		"specular_light = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0), u_shininess);" +
		"fong_ads_light =  ambient_light + diffuse_light + specular_light;" +
		"}" +
		"else" +
		"{" +
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix *  u_model_matrix * vPosition;" +
		"}";
	
	
	vertexShaderObject_v = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_v, vertexShaderSourceCode_v);
	gl.compileShader(vertexShaderObject_v);
	
	if(gl.getShaderParameter(vertexShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre_v = 
	"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec3 fong_ads_light;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"fragColor = vec4(fong_ads_light, 1.0);" +
		"}";
	
	fragmentShaderObject_v = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_v, fragmentShaderSoucre_v);
	gl.compileShader(fragmentShaderObject_v);
	
	if(gl.getShaderParameter(fragmentShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject_v = gl.createProgram();
	gl.attachShader(shaderProgramObject_v, vertexShaderObject_v);
	gl.attachShader(shaderProgramObject_v, fragmentShaderObject_v);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
	
	
	//link program
	gl.linkProgram(shaderProgramObject_v);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject_v, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	toggleShaderFunc();
	
	
	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function toggleShaderFunc()
{
	
	if(toggleShader == toggleShaderMacro.PER_FRAGMENT)
	{
		modelMatrixUniform = gl.getUniformLocation(shaderProgramObject_f, "u_model_matrix");
	    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject_f, "u_view_matrix");
	    projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject_f, "u_projection_matrix");
	    laUniform = gl.getUniformLocation(shaderProgramObject_f, "u_la");
	    ldUniform = gl.getUniformLocation(shaderProgramObject_f, "u_ld");
	    lsUniform = gl.getUniformLocation(shaderProgramObject_f, "u_ls");
	    kaUniform = gl.getUniformLocation(shaderProgramObject_f, "u_ka");
	    kdUniform = gl.getUniformLocation(shaderProgramObject_f, "u_kd");
	    ksUniform = gl.getUniformLocation(shaderProgramObject_f, "u_ks");
	    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject_f, "l_key_pressed");
	    shininessUniform = gl.getUniformLocation(shaderProgramObject_f, "u_shininess");
	    lightPositionUniform = gl.getUniformLocation(shaderProgramObject_f, "u_light_position");
	}
	else if(toggleShader == toggleShaderMacro.PER_VERTEX){
		
		 modelMatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_model_matrix");
	    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_view_matrix");
	    projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_projection_matrix");
	    laUniform = gl.getUniformLocation(shaderProgramObject_v, "u_la");
	    ldUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ld");
	    lsUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ls");
	    kaUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ka");
	    kdUniform = gl.getUniformLocation(shaderProgramObject_v, "u_kd");
	    ksUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ks");
	    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject_v, "l_key_pressed");
	    shininessUniform = gl.getUniformLocation(shaderProgramObject_v, "u_shininess");
	    lightPositionUniform = gl.getUniformLocation(shaderProgramObject_v, "u_light_position");
		
	}else
	{
		modelMatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_model_matrix");
	    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_view_matrix");
	    projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject_v, "u_projection_matrix");
	    laUniform = gl.getUniformLocation(shaderProgramObject_v, "u_la");
	    ldUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ld");
	    lsUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ls");
	    kaUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ka");
	    kdUniform = gl.getUniformLocation(shaderProgramObject_v, "u_kd");
	    ksUniform = gl.getUniformLocation(shaderProgramObject_v, "u_ks");
	    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject_v, "l_key_pressed");
	    shininessUniform = gl.getUniformLocation(shaderProgramObject_v, "u_shininess");
	    lightPositionUniform = gl.getUniformLocation(shaderProgramObject_v, "u_light_position");
	}
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	toggleShaderFunc();
	
	if(toggleShader == toggleShaderMacro.PER_FRAGMENT)
	    gl.useProgram(shaderProgramObject_f);
    else if(toggleShader == toggleShaderMacro.PER_VERTEX)
	    gl.useProgram(shaderProgramObject_v);
	else
		gl.useProgram(shaderProgramObject_v);
	
	
	if(gbLighting == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, ambientLight);
		gl.uniform3fv(ldUniform, diffuseLight);
		gl.uniform3fv(lsUniform, specularLight);
		gl.uniform3fv(kaUniform, materialAmbient);
		gl.uniform3fv(kdUniform, materialDiffuse);
		gl.uniform3fv(ksUniform, materialSpecular);
		gl.uniform1f(shininessUniform, shininess);
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
		gl.uniform1i(lKeyPressedUniform, 0);
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionmatrixUniform, false, perspectiveProjectionMatrix);
	
    sphere.draw();
    
	gl.useProgram(null);
	
	
	
	requestAnimationFrame(display, canvas);
}


function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	
	if(sphere)
		sphere.deallocate();
	
	if(shaderProgramObject_f)
	{
		if(fragmentShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, fragmentShaderObject_f);
			gl.deleteShader(fragmentShaderObject_f);
			fragmentShaderObject_f = null;
		}
		
		if(vertexShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, vertexShaderObject_f);
			gl.deleteShader(vertexShaderObject_f);
			vertexShaderObject_f = null;
		}
		
		gl.deleteProgram(shaderProgramObject_f);
		shaderProgramObject_f = null;
	}
}