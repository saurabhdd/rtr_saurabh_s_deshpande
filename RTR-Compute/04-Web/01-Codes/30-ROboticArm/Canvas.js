//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

var default_stack_size = 5;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;
var mvpUniform;
var stackTop = -1;

var shoulder = 0;
var elbow = 0;

var container = new Float32Array(default_stack_size * 16);

var perspectiveProjectionMatrix = null; 

var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
			case 83:
			case 115:
			shoulder = (shoulder + 3) % 360;
			break;
			
			case 69:
			elbow = (elbow + 3) % 360;
			break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +
	"fragColor = vec4(0.50, 0.350, 0.05, 1.0);" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	
	
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	let modelViewMatrix = mat4.create();
	let translateMatrix = mat4.create();
	let rotateMatrix = mat4.create();
	let tempMatrix = mat4.create();
	let modelViewProjectionMatrix = mat4.create();
	let scaleMatrix = mat4.create(); 
	let translateMatrix1 = mat4.create();
	let modelViewMatrix1 = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [0.0, 0.0, -15.0]);
    mat4.multiply(modelViewMatrix, modelViewMatrix, translateMatrix);
	
	pushMatrix(modelViewMatrix);
	
	//shoulder rotation
	mat4.translate(translateMatrix, translateMatrix, [1.0, 0.0, 0.0]);
	mat4.rotateZ(rotateMatrix, rotateMatrix, degree_to_radians(shoulder));
	mat4.multiply(modelViewMatrix1, rotateMatrix, translateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, modelViewMatrix1);
	
	pushMatrix(modelViewMatrix);
	
	mat4.scale(scaleMatrix, scaleMatrix, [2.0, 0.5, 1.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
    sphere.draw();
	
	//mat4.identity(modelViewMatrix);
	modelViewMatrix = popMatrix();
	mat4.identity(rotateMatrix);
	mat4.identity(translateMatrix);
	mat4.identity(translateMatrix1);
	mat4.identity(scaleMatrix);
	
	mat4.translate(translateMatrix, translateMatrix, [4.0, 0.0, 0.0]);
	mat4.rotateZ(rotateMatrix, rotateMatrix, degree_to_radians(elbow));
	mat4.translate(translateMatrix1, translateMatrix1, [3.9, 0.0, 0.0]);
	mat4.multiply(tempMatrix, rotateMatrix, translateMatrix1);
	mat4.multiply(modelViewMatrix1, translateMatrix, tempMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, modelViewMatrix1);
	
	pushMatrix(modelViewMatrix);
	
	mat4.scale(scaleMatrix, scaleMatrix, [2.0, 0.5, 1.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
    sphere.draw();
	popMatrix();
	popMatrix();
	
	gl.useProgram(null);
	
	
	
	requestAnimationFrame(display, canvas);
}


function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}


function pushMatrix(matrix)
{
	
	container[++stackTop] = matrix[0];
	container[++stackTop] = matrix[1];
	container[++stackTop] = matrix[2];
	container[++stackTop] = matrix[3];
	
	container[++stackTop] = matrix[4];
	container[++stackTop] = matrix[5];
	container[++stackTop] = matrix[6];
	container[++stackTop] = matrix[7];
	
	container[++stackTop] = matrix[8];
	container[++stackTop] = matrix[9];
	container[++stackTop] = matrix[10];
	container[++stackTop] = matrix[11];
	
	container[++stackTop] = matrix[12];
	container[++stackTop] = matrix[13];
	container[++stackTop] = matrix[14];
	container[++stackTop] = matrix[15];
}

function popMatrix()
{
	let tempContainer = new Float32Array(16);
	tempContainer[15] = container[stackTop--];
	tempContainer[14] = container[stackTop--];
	tempContainer[13] = container[stackTop--];
	tempContainer[12] = container[stackTop--];
	
	tempContainer[11] = container[stackTop--];
	tempContainer[10] = container[stackTop--];
	tempContainer[9] = container[stackTop--];
	tempContainer[8] = container[stackTop--];
	
	tempContainer[7] = container[stackTop--];
	tempContainer[6] = container[stackTop--];
	tempContainer[5] = container[stackTop--];
	tempContainer[4] = container[stackTop--];
	
	tempContainer[3] = container[stackTop--];
	tempContainer[2] = container[stackTop--];
	tempContainer[1] = container[stackTop--];
	tempContainer[0] = container[stackTop--];
	
	return tempContainer;
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	
	if(sphere)
		sphere.deallocate();
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}