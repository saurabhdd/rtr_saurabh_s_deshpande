//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_position_cube;
var vbo_texture_cube;
var vbo_color_cube;

var mvpUniform;
var textureSamplerUniform;
var pressDigitUniform;

var smiley_texture;
var kundali_texture;

var xRotate_cube;
var yRotate_pyramid;

var pressDigit = 0;

var perspectiveProjectionMatrix = null; 




var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
		case 50:
			pressDigit = 2;
			break;
			
		case 51:
		    pressDigit = 3;
			break;
			
	    case 52:
			pressDigit = 4;
			break;
			
		case 53:
			pressDigit = 5;
			break;
			
	    case 48:
				pressDigit = 0;
				break;
		
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec2 vTexCoords;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec2 out_TexCoords;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_TexCoords = vTexCoords;" +
	"out_color = vColor;" +
	"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec2 out_TexCoords;" +
	"in vec4 out_color;" +
	"uniform highp sampler2D u_texture_sampler;" +
	"uniform int u_pressDigit;" +
	"out vec4 fragColor;" +
	"vec4 color;" +
	"void main(void)" +
	"{" +
	"if(u_pressDigit == 1)" +
	"{" +
	"color = texture(u_texture_sampler, out_TexCoords);" +
	"}" +
	"else" +
	"{" +
	"color = out_color;" +
	"}" +
	"fragColor = color;" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_TEXTURE, "vTexCoords");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_COLOR, "vColor");
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_texture_sampler");
	pressDigitUniform = gl.getUniformLocation(shaderProgramObject, "u_pressDigit");
	
	//triangle vertices
	
    var smileyVertices = new Float32Array([
											1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0	]);
											
	



	
	
	//vao_cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	vbo_position_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_cube);
	gl.bufferData(gl.ARRAY_BUFFER, smileyVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);


	vbo_texture_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	//gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	//gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
	//gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_TEXTURE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_color_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	//gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	//gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	//gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
		
	
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	
	smiley_texture = gl.createTexture();
	
	smiley_texture.image = new Image();
	
	smiley_texture.image.src = "smiley_texture.png";
	
	//lambda, closure or block
	//function pointer samor nav na lihita directly body
	smiley_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, smiley_texture.image);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	

	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	

	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	//variable declaratio
		
		
		
	let smiley1 = new Float32Array([
	    1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0]);
		
		
	let smiley2 = new Float32Array([
	    0.50, 0.50,
		0.0, 0.50,
		0.0, 0.0,
		0.50, 0.0]);
			
		
	let  smiley3 = new Float32Array([
	
		2.0, 2.0,
		0.0, 2.0,
		0.0, 0.0,
		2.0, 0.0
	]);

	let  smiley4 = new Float32Array([
	
		0.5, 0.5,
		0.5, 0.5,
		0.5, 0.5,
		0.5, 0.5
	]);
	
		

	var  quadColor = new Float32Array([
	
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0
	]);
	
	let texCoords;
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var translateMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	
	//drawing cube
	mat4.identity(modelViewMatrix);
	mat4.identity(translateMatrix);
	mat4.identity(rotateMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	
	gl.bindVertexArray(vao_cube);
	
	
	if(pressDigit == 0)
	{
		gl.uniform1i(pressDigitUniform, 0);
		gl.bindTexture(gl.TEXTURE_2D, null);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_cube);
		gl.bufferData(gl.ARRAY_BUFFER, quadColor, gl.DYNAMIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_COLOR);
		
	}else{
		
		gl.uniform1i(pressDigitUniform, 1);
		gl.activeTexture(gl.TEXTURE0);
	    gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
	    gl.uniform1i(textureSamplerUniform, 0);
		
		if (pressDigit == 2)
			texCoords = smiley2;
		if (pressDigit == 3)
			texCoords = smiley3;
		if (pressDigit == 1)
			texCoords = smiley1;
		if (pressDigit == 4)
			texCoords = smiley4;
		
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	    gl.bufferData(gl.ARRAY_BUFFER, texCoords, gl.DYNAMIC_DRAW);
	    gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
	    gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_TEXTURE);
	}
	
	
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);

	
	requestAnimationFrame(display, canvas);
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}
	
	if(vbo_position_cube)
	{
		gl.deleteBuffer(vbo_position_cube);
		vbo_position_cube = null;
	}
	
	if(vbo_texture_cube)
	{
		gl.deleteBuffer(vbo_texture_cube);
		vbo_texture_cube = null;
	}
	
	
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}