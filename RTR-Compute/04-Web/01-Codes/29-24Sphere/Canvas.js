//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;


//uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionmatrixUniform;
var laUniform;
var ldUniform;
var lsUniform;
var kaUniform;
var kdUniform;
var ksUniform;
var shininessUniform;
var lightPositionUniform;
var lKeyPressedUniform;

var xRotate = 0.0;
var yRotate = 0.0;
var zRotate = 0.0;

var keyPressed;

var ambientMaterial = new Array(24);
var diffuseMaterial = new Array(24);
var specularMaterial = new Array(24);
var MShininess = new Array(24);

var ambientLight = [ 0.0, 0.0, 0.0 ];
var diffuseLight = [ 1.0, 1.0, 1.0 ];
var specularLight = [ 0.7, 0.7, 0.7 ];
var lightPosition = [0.0, 0.0, 100.0, 1.0];
	
var radius = 400.0;
	

var xDimension1 = 1.8;
var xDimension2 = 5.50;
var yDimension1 = -3.50;
var yDimension2 = -2.20;
var yDimension3 = -0.80;
var yDimension4 = 0.60;
var yDimension5 = 2.0;
var yDimension6 = 3.50;

var xRotate;
var yRotate;
var zRotate;


var perspectiveProjectionMatrix = null; 

var gbLighting = true;

var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
			
		case 108:
		case 76:
			if(gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
		break;
		
		case 65:
		case 120:
		  keyPressed = 1;
		 break;
		 
		case 66:
		case 121:
			keyPressed = 2;
			break;
			
	    case 67:
		case 121:
			keyPressed = 3;
		break;
		
		
		
		
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int l_key_pressed;" +
		"uniform mediump vec4 u_light_position[3];" +
		"out vec4 eye_cordinate;" +
		"out vec3 trasformed_normal;" +
		"out vec3 light_direction[3];" +
		"out vec3 view_vector;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"if(l_key_pressed == 1)" +
		"{" +
		"eye_cordinate = u_view_matrix * u_model_matrix * vPosition;" +
		"trasformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"for(i = 0; i < 3; i++)" +
		"{" +
		"light_direction[i] = vec3(u_light_position[i] - eye_cordinate);" +
		"}" +
		"view_vector = vec3(-eye_cordinate);" +
		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	 "#version 300 es" +
		"\n" +
		"precision highp float;" +
		"vec3 fong_ads_lighting;" +
		"in vec4 eye_cordinate;" +
		"in vec3 trasformed_normal;" +
		"in vec3 light_direction[3];" +
		"in vec3 view_vector;" +
		"uniform vec3 u_la[3];" +
		"uniform vec3 u_ld[3];" +
		"uniform vec3 u_ls[3];" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shininess;" +
		"uniform mediump int l_key_pressed;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"int i;" +
		"vec3 normalised_light_direction[3];" +
		"vec3 refliection_vector[3];" +
		"vec3 ambient[3];" +
		"vec3 diffuse[3];" +
		"vec3 specular[3];" +
		"if(l_key_pressed == 1)" +
		"{" +
		"vec3 normalised_transformed_normal = normalize(trasformed_normal);" +
		"vec3 normalized_view_vector = normalize(view_vector);" +
		"for(i = 0; i < 3; i++)" +
		"{" + 
		"normalised_light_direction[i] = normalize(light_direction[i]);" +
		"refliection_vector[i] = reflect(-normalised_light_direction[i], normalised_transformed_normal);" +
		"ambient[i] = u_la[i] * u_ka;" +
		"diffuse[i] = u_ld[i] * u_kd * max(dot(normalised_light_direction[i], normalised_transformed_normal), 0.0);" +
		"specular[i] = u_ls[i] * u_ks* pow(max(dot(refliection_vector[i], normalized_view_vector), 0.0), u_shininess);" +
		"fong_ads_lighting = fong_ads_lighting + ambient[i] + diffuse[i] + specular[i];" +
		"}" +
		"}" +
		"else" +
		"{" +
		"fong_ads_lighting = vec3(1.0, 1.0, 1.0);" +
		"}" +
		"fragColor = vec4(fong_ads_lighting, 1.0);" +
		"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_NORMAL, "vNormal");
	
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	
	//get uniform location
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionmatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
	lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "l_key_pressed");
	shininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	xRotate = 0.0;
	yRotate = 0.0;
	zRotate = 0.0;
	keyPressed = 0;
	
	sphere = new Mesh();
	makeSphere(sphere, 0.50, 50, 50);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.250, 0.250, 0.250, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	//variable declaration
	var xDim;
	var yDim;
	
	
	
	ambientMaterial[0] = [0.0215, 0.1745, 0.0215];
	diffuseMaterial[0] = [0.18275, 0.17, 0.22525];
	specularMaterial[0] = [0.332741, 0.328634, 0.346435];

	ambientMaterial[1] = [0.135, 0.2225, 0.1575];
	diffuseMaterial[1] = [0.54, 0.89, 0.63];
	specularMaterial[1] = [0.316228, 0.316228, 0.316228];

	ambientMaterial[2] = [0.05375, 0.05, 0.06625];
	diffuseMaterial[2] = [0.18275, 0.17, 0.22525];
	specularMaterial[2] = [0.332741, 0.328634, 0.346435];

	ambientMaterial[3] = [0.25, 0.20725, 0.20725];
	diffuseMaterial[3] = [1.0, 0.829, 0.829];
	specularMaterial[3] = [0.296648, 0.296648, 0.296648];

	ambientMaterial[4] = [0.1745, 0.01175, 0.01175];
	diffuseMaterial[4] = [0.61424, 0.04136, 0.04136];
	specularMaterial[4] = [0.727811, 0.626959, 0.626959];

	ambientMaterial[5] = [0.1, 0.18725, 0.1745];
	diffuseMaterial[5] = [0.396, 0.74151, 0.69102];
	specularMaterial[5] = [0.297254, 0.30829, 0.306678];

	ambientMaterial[6] = [0.329412, 0.223529, 0.027451];
	diffuseMaterial[6] = [0.780392, 0.568627, 0.113725];
	specularMaterial[6] = [0.992157, 0.941176, 0.807843];

	ambientMaterial[7] = [0.2125, 0.1275, 0.054];
	diffuseMaterial[7] = [0.714, 0.4284, 0.18144];
	specularMaterial[7] = [0.393548, 0.271906, 0.166721];

	ambientMaterial[8] = [0.25, 0.25, 0.25];
	diffuseMaterial[8] = [0.4, 0.4, 0.4];
	specularMaterial[8] = [0.774597, 0.774597, 0.774597];

	ambientMaterial[9] = [0.19125, 0.0735, 0.0225];
	diffuseMaterial[9] = [0.7038, 0.27048, 0.0828];
	specularMaterial[9] = [0.256777, 0.137622, 0.086014];

	ambientMaterial[10] = [0.24725, 0.1995, 0.0745];
	diffuseMaterial[10] = [0.75164, 0.60648, 0.22648];
	specularMaterial[10] = [0.628281, 0.555802, 0.366065];

	ambientMaterial[11] = [0.19225, 0.19225, 0.19225];
	diffuseMaterial[11] = [0.50754, 0.50754, 0.50754];
	specularMaterial[11] = [0.508273, 0.508273, 0.508273];

	ambientMaterial[12] = [0.0, 0.0, 0.0];
	diffuseMaterial[12] = [0.01, 0.01, 0.01];
	specularMaterial[12] = [0.50, 0.50, 0.50];

	ambientMaterial[13] = [0.0, 0.1, 0.06];
	diffuseMaterial[13] = [0.0, 0.50980392, 0.50980392];
	specularMaterial[13] = [0.50196078, 0.50196078, 0.50196078];

	ambientMaterial[14] = [0.0, 0.0, 0.0];
	diffuseMaterial[14] = [0.1, 0.35, 0.1];
	specularMaterial[14] = [0.45, 0.55, 0.45];

	ambientMaterial[15] = [0.0, 0.0, 0.0];
	diffuseMaterial[15] = [0.5, 0.0, 0.0];
	specularMaterial[15] = [0.7, 0.6, 0.6];

	ambientMaterial[16] = [0.0, 0.0, 0.0];
	diffuseMaterial[16] = [0.55, 0.55, 0.55];
	specularMaterial[16] = [0.70, 0.70, 0.70];

	ambientMaterial[17] = [0.0, 0.0, 0.0];
	diffuseMaterial[17] = [0.5, 0.5, 0.0];
	specularMaterial[17] = [0.60, 0.60, 0.50];

	ambientMaterial[18] = [0.02, 0.02, 0.02];
	diffuseMaterial[18] = [0.01, 0.01, 0.01];
	specularMaterial[18] = [0.4, 0.4, 0.4];

	ambientMaterial[19] = [0.0, 0.05, 0.05];
	diffuseMaterial[19] = [0.4, 0.5, 0.5];
	specularMaterial[19] = [0.04, 0.7, 0.7];

	ambientMaterial[20] = [0.0, 0.05, 0.0];
	diffuseMaterial[20] = [0.4, 0.5, 0.4];
	specularMaterial[20] = [0.04, 0.7, 0.04];

	ambientMaterial[21] = [0.05, 0.0, 0.0];
	diffuseMaterial[21] = [0.5, 0.4, 0.4];
	specularMaterial[21] = [0.7, 0.04, 0.04];

	ambientMaterial[22] = [0.05, 0.05, 0.05];
	diffuseMaterial[22] = [0.5, 0.5, 0.5];
	specularMaterial[22] = [0.7, 0.7, 0.7];

	ambientMaterial[23] = [0.05, 0.05, 0.0];
	diffuseMaterial[23] = [0.5, 0.5, 0.4];
	specularMaterial[23] = [0.7, 0.7, 0.04];
	
	
	
	MShininess[0] = 0.3 * 128;
	 MShininess[1] = 0.3 * 128;
	 MShininess[2] = 0.3 * 128;
	 MShininess[3] = 0.3 * 128;
	 MShininess[4] = 0.6 * 128;
	 MShininess[5] = 0.1 * 128;
	 MShininess[6] = 0.21794872 * 128;
	 MShininess[7] = 0.2 * 128;
	 MShininess[8] = 0.6 * 128;
	 MShininess[9] = 0.1 * 128;
	 MShininess[10] = 0.4 * 128;
	 MShininess[11] = 0.4 * 128;
	 MShininess[12] = 0.25 * 128;
	 MShininess[13] = 0.25 * 128;
	 MShininess[14] = 0.25 * 128;
	 MShininess[15] = 0.25 * 128;
	 MShininess[16] = 0.25 * 128;
	 MShininess[17] = 0.25 * 128;
	 MShininess[18] = 0.078125 * 128;
	 MShininess[19] = 0.078125 * 128;
	 MShininess[20] = 0.078125 * 128;
	 MShininess[21] = 0.078125 * 128;
	 MShininess[22] = 0.078125 * 128;
	 MShininess[23] = 0.078125 * 128;
	
	
	if (keyPressed == 1)
	{
		lightPosition[0] = 0.0;
		lightPosition[1] = radius * Math.sin(degree_to_radians(xRotate));
		lightPosition[2] = radius * Math.cos(degree_to_radians(xRotate));
		lightPosition[3] = 1.0;
	}
	else if (keyPressed == 2)
	{
		lightPosition[0] = radius * Math.cos(degree_to_radians(xRotate));
		lightPosition[1] = 0.0;
		lightPosition[2] = radius * Math.sin(degree_to_radians(xRotate));
		lightPosition[3] = 1.0;
	}
	else if (keyPressed == 3)
	{
		lightPosition[0] = radius * Math.sin(degree_to_radians(xRotate));
		lightPosition[1] = radius * Math.cos(degree_to_radians(xRotate));
		lightPosition[2] = 0.0;
		lightPosition[3] = 1.0;
	}
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	
	
	
	for(let i =0; i < 24; i++)
	{
		if(i % 4 == 0)
			xDim = -xDimension2;
		if(i % 4 == 1)
			xDim = -xDimension1;
		if(i % 4 == 2)
			xDim = xDimension1;
		if(i % 4 == 3)
			xDim = xDimension2;
		
		if(i >= 0 && i < 4)
			yDim = yDimension6;
		if(i >= 4 && i < 8)
			yDim = yDimension5;
		if(i >= 8 && i < 12)
			yDim = yDimension4;
		if(i >= 12 && i < 16)
			yDim = yDimension3;
		if(i >= 16 && i < 20)
			yDim = yDimension2;
		if(i >= 20 && i < 24)
			yDim = yDimension1;
		
		
		mat4.identity(modelMatrix);
		mat4.identity(viewMatrix);
	   mat4.translate(modelMatrix, modelMatrix, [xDim, yDim, -8.0]);
	   gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	   gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	   gl.uniformMatrix4fv(projectionmatrixUniform, false, perspectiveProjectionMatrix);
       bindUniforms(ambientMaterial[i], diffuseMaterial[i], specularMaterial[i], MShininess[i]);
	   sphere.draw();
	}

	gl.useProgram(null);
	
	update();
	
	
	requestAnimationFrame(display, canvas);
}


function update()
{
	if(xRotate < 360.0)
		xRotate += 20.5;
	else
		xRotate = 0.0;
	
	if(yRotate < 360.0)
		yRotate += 3.5;
	else
		yRotate = 0.0;
	
	if(zRotate < 360.0)
		zRotate += 3.5;
	else
		zRotate = 0.0;
}

function bindUniforms( materialAmbientf, materialDiffusef, materialSpecularf, Shininessf)
{
	if (gbLighting == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		gl.uniform3fv(laUniform, ambientLight);
		gl.uniform3fv(ldUniform, diffuseLight);
		gl.uniform3fv(lsUniform, specularLight);
		gl.uniform3fv(kaUniform, materialAmbientf);
		gl.uniform3fv(kdUniform, materialDiffusef);
		gl.uniform3fv(ksUniform, materialSpecularf);
		gl.uniform1f(shininessUniform, Shininessf);
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
		gl.uniform1i(lKeyPressedUniform, 0);
	}

}


function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	
	if(sphere)
		sphere.deallocate();
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}