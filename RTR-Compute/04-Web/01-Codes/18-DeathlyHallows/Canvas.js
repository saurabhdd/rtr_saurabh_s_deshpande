//global variables
var canvas = null;
var gl;
var canvas_original_width; 	
var canvas_original_height;
var bFullScreen = false;

const WebGLMacros = 
{
	SSD_ATTRIBUTE_POSITION : 1,
	SSD_ATTRIBUTE_COLOR : 2,
	SSD_ATTRIBUTE_NORMAL : 3,
	SSD_ATTRIBUTE_TEXTURE: 4
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var vao_circle;
var vao_triangle;
var vbo_position_circle;
var vbo_position_triangle;
var vao_line;

//rotation variables
var yRotate_circle = 0.0;
var yRotate_triangle = 0.0;

//translate variables
var xTrans_circle = -4.0;
var yTrans_circle = -4.0;
var xTrans_triangle = 4.0;
var yTrans_triangle = -4.0;
var yTrans_line = 3.0;

//displacement
var finalCircleDisplacement = 0.0;

//booleans
var startLine = false;
var startTriangle = false;

var mvpUniform;


var perspectiveProjectionMatrix = null; 


var requestAnimationFrame = window.requestAnimationFrame || 
							window.webkitRequestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.oRequestAnimationFrame ||
							window.msRequestAnimationFrame;
							

var cancelAnimationFrame = window.cancelAnimationFrame ||
							window.webkitCancelRequestAnimationFrame ||
							window.webkitCancelAnimationFrame ||
							window.mozCancelRequestAnimationFrame || 
							window.mozCancelAnimationFrame ||
							window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
							window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
							
							
							

function main()
{
	//get canvas from DOM
	canvas = document.getElementById("SSD");
	
	if(!canvas)
	{
		console.log("Obtaining canvas failed\n");
	}else
		console.log("Obtaining canvas succeeded\n");
	
	//retrieve width and height of canvvas for sake of informatio
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
   
	
	window.addEventListener("keydown", keyDown, false); //window is inbuild variable(window os DOM object)
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	initialize();
	
	resize(); //till nowwe just had resize warm up call but we have watm up redraw call
	display();
	
	
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
			
		case 27:
			uninitialize();
			window.close();
			break;
	}
}
	
function mouseDown(event)
{
	//alert("mouse is clicked");
}



function toggleFullScreen()
{
	var fullscreen_element = document.fullscreenElement || 
							document.webkitFullscreenElement ||
							document.mozFullScreenElement || 
							document.msFullscreenElement ||
							null;
							
    
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function initialize()
{
	 gl = canvas.getContext("webgl2");
	
	if(!gl)
	{
		console.log("Obtaining webgl context failed\n");
		return;
	}else
		console.log("Obtaining webgl context succeeded\n");
	
	
	gl.viewportWindth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_color = vColor;" +
	"}";
	
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//fragment shadar
	var fragmentShaderSoucre = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +
	"fragColor = out_color;" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSoucre);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//shader Program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//pre linking binding of attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.SSD_ATTRIBUTE_COLOR, "vColor");
	
	//link program
	gl.linkProgram(shaderProgramObject);
	
	//link status
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	//get uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	
	var triangleVertices = new Float32Array([
								-0.5, -0.5, 0.0,
								0.5, -0.5, 0.0,
								0.0, 0.5, 0.0]);
								
	var triangleSide = 1.0;
	
	
	
	//circle vertices
	var circleVertices = new Float32Array(300);
	var theta = Math.atan(triangleSide / 2);
	var radius = Math.sin(theta) / (1 + Math.sin(theta));
	
	var counter = 0;
	for (var i = 0.0; i <= 2.0 * Math.PI; i = i + (2.0 * Math.PI / 100))
	 {
		 circleVertices[counter++] = radius * Math.cos(i);
		 circleVertices[counter++] = radius * Math.sin(i);
		 circleVertices[counter++] = 0.0;
	 }
	

	finalCircleDisplacement = radius - 0.5;
	
	
	//line vertices
	var lineVertices = new Float32Array([
									0.0, 0.5, 0.0,
									0.0, -0.5, 0.0]);
	
	
	
	
	
	//circle vao
	vao_circle = gl.createVertexArray();
	gl.bindVertexArray(vao_circle);
	
	vbo_position_circle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_circle);
	gl.bufferData(gl.ARRAY_BUFFER, circleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.vertexAttrib3f(WebGLMacros.SSD_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);

    gl.bindVertexArray(null);
	
	
	
	//vao triangle
	vao_triangle = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);
	
	vbo_position_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_triangle);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.vertexAttrib3f(WebGLMacros.SSD_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);

    gl.bindVertexArray(null);
	
	
	//line vao
	vao_line = gl.createVertexArray();
	gl.bindVertexArray(vao_line);
	
	vbo_position_line = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_line);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.SSD_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.SSD_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.vertexAttrib3f(WebGLMacros.SSD_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);

    gl.bindVertexArray(null);
	
	
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	perspectiveProjectionMatrix = mat4.create();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	
}

function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	

	gl.viewport(0, 0, canvas.width, canvas.height);
	
	//orthographic projection code
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	
}

	
function display()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translateMatrix = mat4.create();
	
	mat4.translate(translateMatrix, translateMatrix, [xTrans_circle, yTrans_circle, -3.50]);
	mat4.rotateY(rotateMatrix, rotateMatrix, degree_to_radians(yRotate_circle));
	mat4.multiply(modelViewMatrix, translateMatrix, rotateMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);


	gl.bindVertexArray(vao_circle);
	gl.drawArrays(gl.LINE_LOOP, 0, 100);
	gl.bindVertexArray(null);
	
	
	if(startLine == true)
	{
		mat4.identity(translateMatrix);
		mat4.identity(rotateMatrix);
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		
		mat4.translate(translateMatrix, translateMatrix, [0.0, yTrans_line, -3.50]);
	    mat4.multiply(modelViewMatrix, translateMatrix, rotateMatrix);
	    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

		
		gl.bindVertexArray(vao_line);
	    gl.drawArrays(gl.LINES, 0, 2);
	    gl.bindVertexArray(null);
	}
	
	
	if(startTriangle == true)
	{
		mat4.identity(translateMatrix);
		mat4.identity(rotateMatrix);
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		
		mat4.translate(translateMatrix, translateMatrix, [xTrans_triangle, yTrans_triangle, -3.50]);
	    mat4.rotateY(rotateMatrix, rotateMatrix, degree_to_radians(yRotate_triangle));
		mat4.multiply(modelViewMatrix, translateMatrix, rotateMatrix);
	    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

		
		gl.bindVertexArray(vao_triangle);
	    gl.drawArrays(gl.LINE_LOOP, 0, 3);
	    gl.bindVertexArray(null);
	}

	
	
	gl.useProgram(null);
    
    update();
	
	requestAnimationFrame(display, canvas);
}

function degree_to_radians(degrees)
{
	var pi = Math.PI;
	return degrees * (pi / 180.0);
}

function update()
{
	if (xTrans_circle < 0.0 && yTrans_circle < finalCircleDisplacement)
	{
		xTrans_circle += 0.01;
		yTrans_circle += 0.01;

		if (yRotate_circle < 360.0)
			yRotate_circle += 4.51;
		else
			yRotate_circle = 0.0;
	}
	else
	{
		startTriangle = true;
		yRotate_circle = 0.0;
		xTrans_circle = 0.0;
		yTrans_circle = finalCircleDisplacement;
	}



	if (startTriangle == true)
	{
		if (xTrans_triangle > 0.0 && yTrans_triangle < 0.0)
		{
			xTrans_triangle -= 0.01;
			yTrans_triangle += 0.01;

			if (yRotate_triangle < 360.0)
				yRotate_triangle += 4.51;
			else
				yRotate_triangle = 0.0;
		}
		else
		{
			startLine = true;
			yRotate_triangle = 0.0;
			yTrans_triangle = 0.0;
			xTrans_triangle = 0.0;
		}
	}

	if (startLine == true)
	{
		if (yTrans_line > 0.0)
			yTrans_line -= 0.01;
	}
}


function uninitialize()
{
	if(vao_y)
	{
		gl.deleteVertexArray(vao_y);
		vao_y = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	if(vbo_position_middle_color)
	{
		gl.deleteBuffer(vbo_position_middle_color);
		vbo_position_middle_color = null;
	}
	
	if(vao_middle)
	{
		gl.deleteVertexArray(vao_middle);
		vao_middle = null;
	}
	
	
	if(vbo_position_middle)
	{
		gl.deleteBuffer(vbo_position_middle);
		vbo_position_middle = null;
	}
	
	if(vbo_position_y)
	{
		gl.deleteBuffer(vbo_position_y);
		vbo_position_y = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}