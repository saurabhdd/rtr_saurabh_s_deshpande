#include <stdio.h>
int main(void)
{
  void PrintBinaryFormOfNumber(unsigned int);

   unsigned int a_SSD;
   unsigned int b_SSD;
   unsigned int result_SSD;

   printf("\n\n");
   printf("Enter An Integer = ");
   scanf("%u", &a_SSD);
   printf("\n\n");
   printf("Enter Another Integer = ");
   scanf("%u", &b_SSD);
   printf("\n\n\n\n");
   result_SSD = (a_SSD & b_SSD);
   printf("Bitwise AND Of \nA = %d and B = %d  is  %d \n\n", a_SSD, b_SSD, result_SSD);


   PrintBinaryFormOfNumber(a_SSD);
   PrintBinaryFormOfNumber(a_SSD);
   PrintBinaryFormOfNumber(result_SSD);

  return(0);

}


void PrintBinaryFormOfNumber(unsigned int decimal_number_SSD)
{

   unsigned int quotient_SSD, remainder_SSD;
   unsigned int num_SSD;
   unsigned int binary_array_SSD[8];
   int i_SSD;

 for (i_SSD = 0; i_SSD < 8; i_SSD++)
     binary_array_SSD[i_SSD] = 0;

   printf("The Binary of  %d Is\t=\t", decimal_number_SSD);
   num_SSD = decimal_number_SSD;
    i_SSD = 7;
   while (num_SSD != 0)
  {
   quotient_SSD = num_SSD / 2;
   remainder_SSD = num_SSD % 2;
   binary_array_SSD[i_SSD] = remainder_SSD;
   num_SSD = quotient_SSD;
   i_SSD--;
  }

   for (i_SSD = 0; i_SSD < 8; i_SSD++)
   printf("%u", binary_array_SSD[i_SSD]);
   printf("\n\n");

}