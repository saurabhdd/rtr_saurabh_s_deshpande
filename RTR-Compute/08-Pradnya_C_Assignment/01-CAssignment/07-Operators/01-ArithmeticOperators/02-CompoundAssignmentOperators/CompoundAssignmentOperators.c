#include <stdio.h>

int main(void)
{
	//variable declarations
	int SSD_a;
	int SSD_b;
	int SSD_x;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &SSD_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &SSD_b);

	printf("\n\n");

	//Since, In All The Following 5 Cases, The Operand on The Left 'a' Is Getting Repeated Immeiately On The Right (e.g : a = a + b or a = a -b),
	//We Are Using Compound Assignment Operators +=, -=, *=, /= and %=

	//Since, 'a' Will Be Assigned The Value Of (a + b) At The Expression (a += b), We Must Save The Original Value Of 'a' To Another Variable (x)
	SSD_x = SSD_a;
	SSD_a += SSD_b; // SSD_a = SSD_a + SSD_b
	printf("Addition Of A = %d And B = %d Gives %d.\n", SSD_x, SSD_b, SSD_a);

	//Value Of 'a' Altered In The Above Expression Is Used Here...
	//Since, 'a' Will Be Assigned The Value Of (a - b) At The Expression (a -= b), We Must Save The Original Value Of 'a' To Another Variable (x)
	SSD_x = SSD_a;
	SSD_a -= SSD_b; // a = a - b
	printf("Subtraction Of A = %d And B = %d Gives %d.\n", SSD_x, SSD_b, SSD_a);

	//Value Of 'a' Altered In The Above Expression Is Used Here...
	//Since, 'a' Will Be Assigned The Value Of (a * b) At The Expression (a *= b), We Must Save The Original Value Of 'a' To Another Variable (x)
	SSD_x = SSD_a;
	SSD_a *= SSD_b; // a = a * b
	printf("Multiplication Of A = %d And B = %d Gives %d.\n", SSD_x, SSD_b, SSD_a);

	//Value Of 'a' Altered In The Above Expression Is Used Here...
	//Since, 'a' Will Be Assigned The Value Of (a / b) At The Expression (a /= b), We Must Save The Original Value Of 'a' To Another Variable (x)
	SSD_x = SSD_a;
	SSD_a /= SSD_b; // a = a / b 
	printf("Division Of A = %d And B = %d Gives Quotient %d.\n", SSD_x, SSD_b, SSD_a);

	//Value Of 'a' Altered In The Above Expression Is Used Here...
	//Since, 'a' Will Be Assigned The Value Of (a % b) At The Expression (a %= b), We Must Save The Original Value Of 'a' To Another Variable (x)
	SSD_x = SSD_a;
	SSD_a %= SSD_b; // a = a % b
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", SSD_x, SSD_b, SSD_a);

	printf("\n\n");

	return(0);
}
