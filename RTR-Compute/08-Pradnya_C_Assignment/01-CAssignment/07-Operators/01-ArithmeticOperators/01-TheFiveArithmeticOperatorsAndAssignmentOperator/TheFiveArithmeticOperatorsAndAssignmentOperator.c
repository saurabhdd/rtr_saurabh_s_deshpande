#include <stdio.h>

int main(void)
{
	//variable declarations
	int SSD_a;
	int SSD_b;
	int SSD_result;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &SSD_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &SSD_b);

	printf("\n\n");

	// *** The Following Are The 5 Arithmetic Operators +, -, *, / and % ***
	// *** Also, The Resultants Of The Arithmetic Operations In All The Below Five Cases Have Been Assigned To The Variable 'result' Using the Assignment Operator (=) ***
	SSD_result = SSD_a + SSD_b;
	printf("Addition Of SSD_A = %d And SSD_B = %d Gives %d.\n", SSD_a, SSD_b, SSD_result);

	SSD_result = SSD_a - SSD_b;
	printf("Subtraction Of SSD_A = %d And SSD_B = %d Gives %d.\n", SSD_a, SSD_b, SSD_result);

	SSD_result = SSD_a * SSD_b;
	printf("Multiplication Of SSD_A = %d And SSD_B = %d Gives %d.\n", SSD_a, SSD_b, SSD_result);

	SSD_result = SSD_a / SSD_b;
	printf("Division Of SSD_A = %d And SSD_B = %d Gives Quotient %d.\n", SSD_a, SSD_b, SSD_result);

	SSD_result = SSD_a % SSD_b;
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", SSD_a, SSD_b, SSD_result);

	printf("\n\n");

	return(0);
}
