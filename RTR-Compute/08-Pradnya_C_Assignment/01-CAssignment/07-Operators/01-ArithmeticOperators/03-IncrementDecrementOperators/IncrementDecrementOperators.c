#include <stdio.h>

int main(void)
{
	//variable declarations
	int SSD_a = 5;
	int SSD_b = 10;

	//code
	printf("\n\n");
	printf("A = %d\n", SSD_a);
	printf("A = %d\n", SSD_a++);
	printf("A = %d\n", SSD_a);
	printf("A = %d\n\n", ++SSD_a);

	printf("B = %d\n", SSD_b);
	printf("B = %d\n", SSD_b--);
	printf("B = %d\n", SSD_b);
	printf("B = %d\n\n", --SSD_b);

	return(0);
}
