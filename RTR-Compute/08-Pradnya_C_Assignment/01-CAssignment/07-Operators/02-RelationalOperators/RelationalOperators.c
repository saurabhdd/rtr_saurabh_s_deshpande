#include <stdio.h>
int main(void)
{
   
   int SSD_a;
   int SSD_b;
   int SSD_result;
   //code
   printf("\n\n");
   printf("Enter One Integer : ");
   scanf("%d", &SSD_a);
   printf("\n\n");
   printf("Enter Another Integer : ");
   scanf("%d", &SSD_b);
   printf("\n\n");
   printf("If Answer = 0, It Is 'FALSE'.\n");
   printf("If Answer = 1, It Is 'TRUE'.\n\n");

   SSD_result = (SSD_a < SSD_b);
   printf("(SSD_a < SSD_b) SSD_A = %d Is Less Than SSD_B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);

   SSD_result = (SSD_a > SSD_b);
   printf("(SSD_a > SSD_b) A = %d Is Greater Than B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);

   SSD_result = (SSD_a <= SSD_b);
   printf("(SSD_a <= SSD_b) A = %d Is Less Than Or Equal To B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);

   SSD_result = (SSD_a >= SSD_b);
   printf("(SSD_a >= SSD_b) A = %d Is Greater Than Or Equal To B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);

   SSD_result = (SSD_a == SSD_b);
   printf("(SSD_a == SSD_b) A = %d Is Equal To B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);

   SSD_result = (SSD_a != SSD_b);
   printf("(SSD_a != SSD_b) A = %d Is NOT Equal To B = %d \t Answer = %d\n", SSD_a, SSD_b, SSD_result);
  
return(0);
}