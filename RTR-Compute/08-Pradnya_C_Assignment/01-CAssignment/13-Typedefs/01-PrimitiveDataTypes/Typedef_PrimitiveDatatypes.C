#include<stdio.h>
typedef int MY_INT;

int main(void)
{
	MY_INT Add(MY_INT, MY_INT);
	typedef int MY_INT_SSD;
	typedef float float_SSD;
	typedef char char_SSD;
	typedef double double_SSD;
	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINSTANCE;

	MY_INT my_int = 23, i;
	MY_INT iArray_SSD[] = {34, 4, 45, 6, 4, 23, 7, 2, 65, 23};

	float_SSD f_SSD = 45.5f;
	const float_SSD f_SSD_pi = 3.14f;

	char_SSD ch_SSD = '*';
	char_SSD chArray_01SSD[] = "Hello";
	char_SSD chArray_02SSD[][10] = { "Saurabh", "Deshpande", "Student" };

	double_SSD d_SSD = 8.34342;

	UINT uint_SSD = 3434;
	HANDLE handle_SSD = 23;
	HWND hwnd_SSD = 332;
	HINSTANCE hInstance_SSD = 23423;

	printf("\n\n");
	printf("Saurabh\n\n");

	printf("Type int variable my_int = %d\n", my_int);

	printf("\n\n");

	for (i = 0; i < (sizeof(iArray_SSD) / sizeof(int)); i++)
	{
		printf("Displaying the array %d :", iArray_SSD[i]);
	}
	 
	printf("\n\n");

	printf("the float variable f_SSD = %f\n", f_SSD);

	printf("the float const variable f_SSD_pi = %f\n", f_SSD_pi);

	printf("\n\n");

	printf("display my double variable d = %lf\n", d_SSD);
	printf("\n\n");

	printf("display my char variable c = %c\n", ch_SSD);
	printf("\n\n");

	printf("Display char array = %s\n", chArray_01SSD);
	printf("\n\n");

	for (i = 0; i < (sizeof(chArray_02SSD) / sizeof(chArray_02SSD[0])); i++)
	{
		printf("%s\n", chArray_02SSD[i]);
	}

	printf("\n\n");

	printf("Display uint = %u\n\n", uint_SSD);
	printf("Display handle = %u\n\n", handle_SSD);
	printf("Display hwnd = %u\n\n", hwnd_SSD);
	printf("Display hinstance = %u\n\n", hInstance_SSD);
	printf("\n\n");


    MY_INT x_SSD = 34;
	MY_INT y_SSD = 45;
	MY_INT ret_SSD;

	ret_SSD = Add(x_SSD, y_SSD);
	printf("ret = %d\n\n", ret_SSD);
	return(0);

}
MY_INT Add(MY_INT a_SSD, MY_INT b_SSD)
{
	MY_INT c_SSD;
	c_SSD = a_SSD + b_SSD;
	return(c_SSD);
}

