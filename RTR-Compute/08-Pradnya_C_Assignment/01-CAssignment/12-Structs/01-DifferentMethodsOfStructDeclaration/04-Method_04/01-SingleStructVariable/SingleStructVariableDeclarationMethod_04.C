#include<stdio.h>
struct MYDATA
{
	int i;
	float f;
	double d;
};

int main(void)
{
	struct MYDATA data;
	int i_SSD, f_SSD, d_SSD, struct_myData_size;

	data.i = 34;
	data.f = 45.3f;
	data.d = 2.4355;

	printf("\n\n");
	printf("Saurabh\n\n");

	printf("Displaying the data members of struct");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);

	i_SSD = sizeof(data.i);
	f_SSD = sizeof(data.f);
	d_SSD = sizeof(data.d);

	printf("\n\n");

	printf("Displaying the sizes of the members: \n\n");
	printf("i  = %d bytes\n ", i_SSD);
	printf("f  = %d bytes\n ", f_SSD);
	printf("d  = %d bytes\n ", d_SSD);

	struct_myData_size = sizeof(struct MYDATA);

	printf("\n\n");
	printf("size of struct myData: %d bytes \n\n", struct_myData_size);

	return(0);
}



