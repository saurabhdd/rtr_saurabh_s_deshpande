#include<stdio.h>

struct MYPOINT
{
	int x;
	int y;
};

struct MYPOINT A_SSD, B_SSD, C_SSD, D_SSD, E_SSD;

int main(void)
{
	printf("\nSaurabh\n\n");

	A_SSD.x = 4;
	A_SSD.y = 45;

	B_SSD.x = 34;
	B_SSD.y = 64;

	C_SSD.x = 12;
	C_SSD.y = 75;

	D_SSD.x = 34;
	D_SSD.y = 23;

	E_SSD.x = 84;
	E_SSD.y = 27;

	printf("CO-ordinates of (x, y) for A : (%d, %d) \n\n", A_SSD.x, A_SSD.y);
	printf("CO-ordinates of (x, y) for B : (%d, %d) \n\n", B_SSD.x, B_SSD.y);
	printf("CO-ordinates of (x, y) for C : (%d, %d) \n\n", C_SSD.x, C_SSD.y);
	printf("CO-ordinates of (x, y) for D : (%d, %d) \n\n", D_SSD.x, D_SSD.y);
	printf("CO-ordinates of (x, y) for E : (%d, %d) \n\n", E_SSD.x, E_SSD.y);

	return(0);
}

