#include <stdio.h>
int main(void)
{
	


	int a_SSD = 43;
	printf(" Value Of 'SSD_a' = %d\n", a_SSD);
	printf(" Octal Value Of 'a' = %o\n", a_SSD);
	printf(" Hexadecimal Value Of 'a' (In Lower Case) = %x\n", a_SSD);
	printf("Hexadecimal Value Of 'a' (In Upper Case) = %X\n\n", a_SSD);

	char ch_SSD = 'D';
	printf("Character ch_SSD = %c\n", ch_SSD);
	char str_SSD[] = "Saurabh Deshpande 053 AstroMediComp's Real Time Rendering Batch 3.0 (2020-2021)";
	printf("String str_SSD = %s\n\n", str_SSD);

	long num_SSD = 3423434L;
	printf("Long Integer = %ld\n\n", num_SSD);

	unsigned int b_SSD = 75;
	printf("Unsigned Integer 'SSD_b' = %u\n\n", b_SSD);

	float f_num_SSD = 3322.4563f;
	printf(" %%f 'SSD_f_num' = %f\n", f_num_SSD);
	printf(" %%4.2f 'SSD_f_num' = %4.2f\n", f_num_SSD);
	printf(" %%6.5f 'SSD_f_num' = %6.5f\n\n", f_num_SSD);

	double d_pi_SSD = 3.14159265358979323846;
	printf("Double Precision Floating Point Number Without Exponential = %g\n", d_pi_SSD);

	printf("Double Precision Floating Point Number With Exponential  = %e\n", d_pi_SSD);
	printf("Double Precision Floating Point Number With Exponential  = %E\n\n", d_pi_SSD);
	printf("Double Hexadecimal Value Of 'SSD_d_pi' (Hexadecimal Letters In Lower Case) = %a\n", d_pi_SSD);
	printf("Double Hexadecimal Value Of 'SSD_d_pi' (Hexadecimal Letters In Upper Case) = %A\n\n", d_pi_SSD);
    
   
	return(0);
}
