#include <stdio.h>
#include <stdlib.h>
#define MAX_STRING_LENGTH 512
int main(void)
{
	char* ReplaceVowelsWithHashSymbol(char *);

	char string_SSD[MAX_STRING_LENGTH];
	char *replaced_string_SSD = NULL;


	printf("\n\nSaurabh\n\n");
	printf("Please enter a string : ");

	gets_s(string_SSD, MAX_STRING_LENGTH);

	replaced_string_SSD = ReplaceVowelsWithHashSymbol(string_SSD);

	if (replaced_string_SSD == NULL)
	{
		printf("replace function has failed\n\n");
	}

	printf("\n\n");
	printf("Now the replaced string is : \n\n");

	printf("%s\n\n", replaced_string_SSD);
	if (replaced_string_SSD)
	{
		free(replaced_string_SSD);
		replaced_string_SSD = NULL;
	}

	return(0);

}
char* ReplaceVowelsWithHashSymbol(char *s)
{
	void MyStrcpy(char *, char *);
	int MyStrlen(char *);

	char *new_string_SSD = NULL;
	int i;

	new_string_SSD = (char *)malloc(MyStrlen(s) * sizeof(char));
	if (new_string_SSD == NULL)
	{
		printf("COULD NOT ALLOCATE MEMORY FOR NEW STRING !!!\n\n");
		return(NULL);
	}

	MyStrcpy(new_string_SSD, s);

	for (i = 0; i < MyStrlen(new_string_SSD); i++)
	{
		switch (new_string_SSD[i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			new_string_SSD[i] = '#';
			break;
		default:
			break;
		}
	}
	return(new_string_SSD);

}

void MyStrcpy(char *str_destination, char *str_source)
{
	int MyStrlen(char *);
	int iStringLength = 0;
	int j;

	iStringLength = MyStrlen(str_source);
	for (j = 0; j < iStringLength; j++)
		*(str_destination + j) = *(str_source + j);
	*(str_destination + j) = '\0';
}

int MyStrlen(char *str)
{
	int j;
	int string_length = 0;
	for (j = 0; j < MAX_STRING_LENGTH; j++) {
		if (*(str + j) == '\0'
			)
			break
			;
		else
			string_length++;
	}
	return(string_length);
}

