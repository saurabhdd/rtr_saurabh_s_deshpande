#include <stdio.h>
int main(void)
{
	void SwapNumbers(int, int);
	int a;
	int b;

	printf("\n\nSaurabh\n\n");

	printf("Enter value for a : ");
	scanf("%d", &a);
	printf("\n\n");

	printf("Enter value for b : ");
	scanf("%d", &b);
	printf("\n\n");


	SwapNumbers(a, b); 
	printf("\n\n");

	printf("Value Of a after swapping = %d\n\n", a);
	printf("Value Of b after swapping = %d\n\n", b);

	return(0);
}
void SwapNumbers(int x, int y)
{
	int temp;

	printf("\n\n");
	printf("Value Of x which is a before swap = %d\n\n", x);
	printf("Value Of y which is b before swap = %d\n\n", y);

	temp = x;

	x = y;
	y = temp;
	printf("\n\n");

	printf("Values after swap\n");
	printf("Value Of x = %d\n\n", x);
	printf("Value Of y = %d\n\n", y);
}