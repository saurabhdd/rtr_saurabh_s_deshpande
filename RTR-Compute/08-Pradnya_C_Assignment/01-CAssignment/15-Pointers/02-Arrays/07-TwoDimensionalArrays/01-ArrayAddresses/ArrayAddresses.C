#include <stdio.h>
#include <stdlib.h>
#define ROWS_SSD 4
#define COLUMNS_SSD 3
int main(void)
{
	int iArray_SSD[ROWS_SSD][COLUMNS_SSD];
	int i, j;

	for (i = 0; i < ROWS_SSD; i++)
	{
		for (j = 0; j < COLUMNS_SSD; j++)
			iArray_SSD[i][j] = (i + 1) * (j + 1);
	}
	printf("\n\n");
	printf("Saurabh\n\n");

	printf("Adress and elements of 2D array : \n\n");

	for (i = 0; i < ROWS_SSD; i++)
	{
		for (j = 0; j < COLUMNS_SSD; j++)
		{
			printf("iArray_SSD[%d][%d] = %d ------ address : %p\n", i, j, iArray_SSD[i][j], &iArray_SSD[i][j]);
		}
		printf("\n\n");
	}
	return(0);
}