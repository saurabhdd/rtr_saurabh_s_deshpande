#include<stdio.h>
#define NUM_OF_ROWS_SSD 4
#define NUM_OFCOL_SSD 5

int main(int argc, char *argv)
{
	int *arr_SSD[NUM_OF_ROWS_SSD];
	int i = 0; int j = 0;

	printf("\n\nSaurabh\n\n");

	for (i = 0; i < NUM_OF_ROWS_SSD; i++)
	{
		arr_SSD[i] = (int*)malloc(sizeof(int) * NUM_OFCOL_SSD);
		if (!arr_SSD[i])
		{
			printf("memory not allocated for %d row\n", i);
			exit(0);
		}
	}

		//assigning values to created arry
		for (i = 0; i < NUM_OF_ROWS_SSD; i++)
		{
			for (j = 0; j < NUM_OFCOL_SSD; j++)
			{
				*(arr_SSD[i] + j) = (i + 1)*(j + 1);
			}
		}

		//Displaying the values inserted

		for (i = 0; i < NUM_OF_ROWS_SSD; i++)
		{
			for (j = 0; j < NUM_OFCOL_SSD; j++)
			{
				printf("element arr_SSD[%d][%d] is %d\n", i, j, *(arr_SSD[i] + j));
			}
		}

		//Releasing the memory
		for (i =( NUM_OF_ROWS_SSD - 1); i >= 0; i--)
		{
			
				free(arr_SSD[i]);
				arr_SSD[i] = NULL;
			
		}


	return 0;
}