#include <stdio.h>
struct sizes
{
	char myname_SSD[100];
	int myage_SSD;
	float mysal_SSD;
	char mysex_SSD;
	char mymarital_status_SSD;
};
int main(void)
{
	//code
	printf("Saurabh\n\n");

	printf("Sizes of data types and pointers: \n\n");
	printf("Size Of (Int)   : %d \t  Size Of Pointer to Int(int*)       : % d \t Size Of Pointer To Pointer To int(int**)       : %d\n", sizeof(int), sizeof(int*), sizeof(int**));
	printf("Size Of (Float) : %d \t  Size Of Pointer To float(float*)   : % d \t ize Of Pointer To Pointer To float(float**)   : %d\n", sizeof(float), sizeof(float*), sizeof(float**));
	printf("Size Of (Double): %d \t  Size Of Pointer To Double(double*) : % d \t Size Of Pointer To Pointer To double(double**) : %d\n", sizeof(double), sizeof(double*), sizeof(double**));
	printf("Size Of (Char)  : %d \t  Size Of Pointer To Char(char*)     : % d \t Size Of Pointer To Pointer To char(char**)     : %d\n", sizeof(char), sizeof(char*), sizeof(char**));
	printf("Size Of (Struct sizes) : %d \t \t Size Of Pointer To Struct sizes(struct sizes*) : %d \t Size Of Pointer To Pointer To struct sizes(struct sizes**) : % d\n\n", sizeof(struct sizes), sizeof(struct sizes*), sizeof(struct sizes**));
	return(0);

}