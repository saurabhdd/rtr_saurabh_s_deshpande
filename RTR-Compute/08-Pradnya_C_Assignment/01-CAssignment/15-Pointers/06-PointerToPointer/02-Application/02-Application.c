#include <stdio.h>
int main(void)
{
	int number_SSD;
	int* _ptr_SSD = NULL;
	int** _pptr_SSD = NULL; 

		number_SSD = 20;
	printf("Saurabh\n");
	printf("\n\n");
	printf(" before _ptr_SSD = &number_SSD \n\n");
	printf("value of number_SSD = %d\n", number_SSD);
	printf("address of number_SSD = %p\n\n", &number_SSD);
	printf("value at address of number_SSD = %d\n", *(&number_SSD));

	_ptr_SSD = &number_SSD;
	printf("\n\n");
	printf(" after _ptr_SSD = &number_SSD \n");
	printf("value of number_SSD = %d\n", number_SSD);
	printf("address of number_SSD = %p\n\n", _ptr_SSD);
	printf("value at address of number_SSD = %d\n", *_ptr_SSD);


		_pptr_SSD = &_ptr_SSD;
	printf("\n\n");
	printf(" after _pptr_SSD = &_ptr_SSD\n");
	printf("value of number_SSD = %d\n", number_SSD);
	printf("address of number_SSD (_ptr_SSD) = %p\n", _ptr_SSD);
	printf("address of _ptr_SSD (_pptr_SSD) = %p\n", _pptr_SSD);
	printf("value at address of _ptr_SSD (*_pptr_SSD) = %p\n\n", *_pptr_SSD);
	printf("value at address of number_SSD (*_ptr_SSD) (*_pptr_SSD) = %d\n", **_pptr_SSD);


	return(0);
}