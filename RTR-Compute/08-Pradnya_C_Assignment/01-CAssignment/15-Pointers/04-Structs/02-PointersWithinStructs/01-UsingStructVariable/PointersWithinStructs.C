#include <stdio.h>

struct MyData_SSD
{
	int *ptr_i;
	int i;
	float *ptr_f;
	float f;
	double *ptr_d;
	double d;
};
int main(void)
{

	struct MyData_SSD data_SSD;

	//code
	data_SSD.i = 9;
	data_SSD.ptr_i = &data_SSD.i;
	data_SSD.f = 11.45f;
	data_SSD.ptr_f = &data_SSD.f;
	data_SSD.d = 30.121995;
	data_SSD.ptr_d = &data_SSD.d;

	printf("Displaying the values\n");

	printf("\n\nSaurabh\n\n");

	printf("i = %d\n", *(data_SSD.ptr_i));
	printf("Adress Of 'i' = %p\n", data_SSD.ptr_i);
	printf("\n\n");
	printf("f = %f\n", *(data_SSD.ptr_f));
	printf("Adress Of 'f' = %p\n", data_SSD.ptr_f);
	printf("\n\n");
	printf("d = %lf\n", *(data_SSD.ptr_d));
	printf("Adress Of 'd' = %p\n", data_SSD.ptr_d);

	return(0);
}

