#include <stdio.h>

struct MYPOINT
{
	int x;
	int y;
};

struct RECTANGLE
{
	struct MYPOINT point_01_SSD; 
	struct MYPOINT point_02_SSD;

};

int main(void)
{
	struct RECTANGLE rect_SSD = { {3, 5},{6,9}};


	int length_SSD, breadth_SSD, area_SSD;

	printf("\n\n");
	printf("Saurabh\n\n");

	length_SSD = rect_SSD.point_02_SSD.y - rect_SSD.point_01_SSD.y;

	if (length_SSD < 0)
		length_SSD = length_SSD * -1;

	breadth_SSD = rect_SSD.point_02_SSD.x - rect_SSD.point_01_SSD.x;

	if (breadth_SSD < 0)
		breadth_SSD = breadth_SSD * -1;

	area_SSD = length_SSD * breadth_SSD;

	printf("\n\n");
	printf("Length = %d\n\n", length_SSD);
	printf("Breadth = %d\n\n", breadth_SSD);
	printf("Area = %d\n\n", area_SSD);


	return(0);
}