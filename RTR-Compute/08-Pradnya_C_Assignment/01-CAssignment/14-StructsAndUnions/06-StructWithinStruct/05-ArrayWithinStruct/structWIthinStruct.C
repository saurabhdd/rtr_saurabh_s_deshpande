#include <stdio.h>
struct MYNUMBER
{
	int NUM_SSD;
	int NUM_TABLE[10];
};
struct NUMTABLES
{
	struct MYNUMBER Z_SSD;
	struct MYNUMBER Y_SSD;
	struct MYNUMBER X_SSD;
};
int main(void)
{
	struct NUMTABLES tables_SSD;
	int i;

	printf("\n\nSaurabh\n\n");

	tables_SSD.Z_SSD.NUM_SSD = 2;
    for (i = 0; i < 10; i++)
	tables_SSD.Z_SSD.NUM_TABLE[i] = tables_SSD.Z_SSD.NUM_SSD * (i + 1);
    printf("\n\n");
    printf("Table Of %d : \n\n", tables_SSD.Z_SSD.NUM_SSD);
    for (i = 0; i < 10; i++)
	printf("%d * %d = %d\n", tables_SSD.Z_SSD.NUM_SSD, (i + 1), tables_SSD.Z_SSD.NUM_TABLE[i]);


	tables_SSD.Y_SSD.NUM_SSD = 3;
    for (i = 0; i < 10; i++)
	tables_SSD.Y_SSD.NUM_TABLE[i] = tables_SSD.Y_SSD.NUM_SSD * (i + 1);
    printf("\n\n");
    printf("Table Of %d : \n\n", tables_SSD.Y_SSD.NUM_SSD);
	for (i = 0; i < 10; i++)
	printf("%d * %d = %d\n", tables_SSD.Y_SSD.NUM_SSD, (i + 1), tables_SSD.Y_SSD.NUM_TABLE[i]);



	tables_SSD.X_SSD.NUM_SSD = 4;

	for (i = 0; i < 10; i++)
		tables_SSD.X_SSD.NUM_TABLE[i] = tables_SSD.X_SSD.NUM_SSD * (i + 1);

	printf("\n\n");

	printf("Table Of %d : \n\n", tables_SSD.X_SSD.NUM_SSD);

	for (i = 0; i < 10; i++)
		printf("%d * %d = %d\n", tables_SSD.X_SSD.NUM_SSD, (i + 1), tables_SSD.X_SSD.NUM_TABLE[i]);

	

	return(0);
}