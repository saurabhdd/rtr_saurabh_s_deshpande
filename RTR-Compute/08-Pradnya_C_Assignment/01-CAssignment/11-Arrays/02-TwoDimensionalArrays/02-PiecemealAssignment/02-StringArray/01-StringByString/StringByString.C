#include <stdio.h>
#define MAX_STRING_LENGTH_SSD 512
int main(void)
{
	void MyStrcpy(char[], char[]);

	char strArray_SSD[5][10]; 
	int char_size_SSD;
	int strArray_size_SSD;
	int strArray_num_elements_SSD, strArray_num_rows_SSD, strArray_num_columns_SSD, i;
	printf("\n\n");

	printf("Saurabh\n\n");
	char_size_SSD = sizeof(char);
	strArray_size_SSD = sizeof(strArray_SSD); 
    printf("Complete size of char 2D array is = %d\n\n", strArray_size_SSD);

	strArray_num_rows_SSD = strArray_size_SSD / sizeof(strArray_SSD[0]);
	printf("Number of rows in 2D array are = %d\n\n", strArray_num_rows_SSD);

	strArray_num_columns_SSD = sizeof(strArray_SSD[0]) / char_size_SSD;
	printf("Number of columns in array are  = %d\n\n", strArray_num_columns_SSD);

	strArray_num_elements_SSD = strArray_num_rows_SSD * strArray_num_columns_SSD;
	printf("Maximum number of elements in array are = %d\n\n", strArray_num_elements_SSD);
		
	MyStrcpy(strArray_SSD[0], "My");
	MyStrcpy(strArray_SSD[1], "Name");
	MyStrcpy(strArray_SSD[2], "is");
	MyStrcpy(strArray_SSD[3], "Saurabh");
	MyStrcpy(strArray_SSD[4], "Deshpande");

	printf("\n\n");

	printf("The number of strings in char array are : \n\n");
	for (i = 0; i < strArray_num_rows_SSD; i++)
		printf("%s ", strArray_SSD[i]);

	printf("\n\n");
	return(0);
}
void MyStrcpy(char str_destination_SSD[], char str_source_SSD[])
{
	int MyStrlen(char[]);
	int j, iStringLength_SSD = 0;

	iStringLength_SSD = MyStrlen(str_source_SSD);

	for (j = 0; j < iStringLength_SSD; j++)
		str_destination_SSD[j] = str_source_SSD[j];

	str_destination_SSD[j] = '\0';
}
int MyStrlen(char str[])
{
	int j;
	int string_length_SSD = 0;
		for (j = 0; j < MAX_STRING_LENGTH_SSD; j++)
		{
			if (str[j] == '\0')
				break;
			else
				string_length_SSD++;
		}
	return(string_length_SSD);
}