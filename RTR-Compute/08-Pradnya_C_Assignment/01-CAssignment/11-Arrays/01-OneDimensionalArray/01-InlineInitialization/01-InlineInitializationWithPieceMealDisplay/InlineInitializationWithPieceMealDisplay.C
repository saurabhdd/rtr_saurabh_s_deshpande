#include <stdio.h>
int main(void)
{
	int iArray_SSD[] = { 4, 43, 4, 34, 3, 21, 56, 3, 27, 67 };
	int int_size_SSD;
	int iArray_size_SSD;
	int iArray_num_elements_SSD;
	float fArray_SSD[] = { 2.3f, 3.3f, 4.6f, 5.8f, 1.4f, 5.9f, 2.4f, 8.7f };
	int float_size_SSD;
	int fArray_size_SSD;
	int fArray_num_elements_SSD;
	char cArray_SSD[] = { 'S', 'A', 'U', 'R', 'A', 'B', 'H', 'D', 'E', 'S', 'H', 'P','A', 'N', 'D', 'E'};
	int char_size_SSD;
	int cArray_size_SSD;
	int cArray_num_elements_SSD;
	printf("Saurabh\n");

	printf("\n");
	printf("Inline initialization and piece-meal display of elements of array : \n\n");
	printf("1st Element = %d\n", iArray_SSD[0]);
	printf("2nd Element = %d\n", iArray_SSD[1]);
	printf("3rd Element = %d\n", iArray_SSD[2]);
	printf("4th Element = %d\n", iArray_SSD[3]);
	printf("5th Element = %d\n", iArray_SSD[4]);
	printf("6th Element = %d\n", iArray_SSD[5]);
	printf("7th Element = %d\n", iArray_SSD[6]);
	printf("8th Element = %d\n", iArray_SSD[7]);
	printf("9th Element = %d\n", iArray_SSD[8]);
	printf("10th Element = %d\n\n", iArray_SSD[9]);
	int_size_SSD = sizeof(int);
	iArray_size_SSD = sizeof(iArray_SSD);
	iArray_num_elements_SSD = iArray_size_SSD / int_size_SSD;
	printf("Size Of Data type 'int' = %d bytes\n", int_size_SSD);
	printf("Number of elemenys in our array= %d Elements\n", iArray_num_elements_SSD);
	printf("Size Of Array %d Elements * %d Bytes = %d Bytes\n", iArray_num_elements_SSD, int_size_SSD, iArray_size_SSD);
	printf("\n\n");
	printf("Inline initialization and piece-meal display of elements of array: \n\n");
	printf("1st Element = %f\n", fArray_SSD[0]);
	printf("2nd Element = %f\n", fArray_SSD[1]);
	printf("3rd Element = %f\n", fArray_SSD[2]);
	printf("4th Element = %f\n", fArray_SSD[3]);
	printf("5th Element = %f\n", fArray_SSD[4]);
	printf("6th Element = %f\n", fArray_SSD[5]);
	printf("7th Element = %f\n", fArray_SSD[6]);
	printf("8th Element = %f\n", fArray_SSD[7]);
	printf("9th Element = %f\n", fArray_SSD[8]);

	float_size_SSD = sizeof(float);
	fArray_size_SSD = sizeof(fArray_SSD);

	fArray_num_elements_SSD = fArray_size_SSD / float_size_SSD;
	printf("Size Of Data type 'float' = %d bytes\n",float_size_SSD);
	printf("Number Of Elements In 'float' Array 'fArray[]' = %d Elements\n",fArray_num_elements_SSD);
	printf("Size Of Array %d Elements * %d Bytes = %d Bytes\n\n",fArray_num_elements_SSD, float_size_SSD, fArray_size_SSD);

	printf("\n\n");
	printf("Inline initialization and piece-meal display of elements of array: \n\n");
	printf("1st Element = %c\n", cArray_SSD[0]);
	printf("2nd Element = %c\n", cArray_SSD[1]);
	printf("3rd Element = %c\n", cArray_SSD[2]);
	printf("4th Element = %c\n", cArray_SSD[3]);
	printf("5th Element = %c\n", cArray_SSD[4]);
	printf("6th Element = %c\n", cArray_SSD[5]);
	printf("7th Element = %c\n", cArray_SSD[6]);
	printf("8th Element = %c\n", cArray_SSD[7]);
	printf("9th Element = %c\n", cArray_SSD[8]);
	printf("10th Element = %c\n", cArray_SSD[9]);
	printf("11th Element = %c\n", cArray_SSD[10]);
	printf("12th Element = %c\n", cArray_SSD[11]);
	printf("13th Element = %c\n", cArray_SSD[12]);
	printf("14th Element = %c\n", cArray_SSD[13]);
	printf("15th Element = %c\n", cArray_SSD[14]);
	printf("16th Element = %c\n", cArray_SSD[15]);

	char_size_SSD = sizeof(char);
	cArray_size_SSD = sizeof(cArray_SSD);
	cArray_num_elements_SSD = cArray_size_SSD / char_size_SSD;
	printf("Size Of Data type 'char' = %d bytes\n", char_size_SSD);
	printf("Number of elements in our char array = %d Elements\n",cArray_num_elements_SSD);
	printf("Size Of Array %d Elements * %d Bytes = %d Bytes\n\n", cArray_num_elements_SSD, char_size_SSD, cArray_size_SSD);

	return(0);
}
