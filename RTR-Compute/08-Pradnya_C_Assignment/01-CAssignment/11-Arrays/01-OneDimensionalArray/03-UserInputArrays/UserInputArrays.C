#include <stdio.h>

#define INT_ARRAY_NUM_ELEMENTS_SSD 13
#define FLOAT_ARRAY_NUM_ELEMENTS_SSD 4
#define CHAR_ARRAY_NUM_ELEMENTS_SSD 6

int main(void)
{
	int iArray_SSD[INT_ARRAY_NUM_ELEMENTS_SSD];
	float fArray_SSD[FLOAT_ARRAY_NUM_ELEMENTS_SSD];
	char cArray_SSD[CHAR_ARRAY_NUM_ELEMENTS_SSD];
	int i_SSD, num_SSD;
	
	printf("\n\n");
	printf("Entering elements in int array using loop : \n");
	for (i_SSD = 0; i_SSD < INT_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
		scanf("%d", &iArray_SSD[i_SSD]);
	printf("\n\n");
	printf("Entering elements in float aray using loop : \n");
	for (i_SSD = 0; i_SSD < FLOAT_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
		scanf("%f", &fArray_SSD[i_SSD]);
	printf("\n\n");
	printf("Entering elements for character array using loop: \n");
	for (i_SSD = 0; i_SSD < CHAR_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
	{
		cArray_SSD[i_SSD] = getch();
		printf("%c\n", cArray_SSD[i_SSD]);
	}
	
	printf("\n\n");
	printf("Displaying interger array: \n\n");
	for (i_SSD = 0; i_SSD < INT_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
		printf("%d\n", iArray_SSD[i_SSD]);
	printf("\n\n");
	printf("Displaying float array : \n\n");
	for (i_SSD = 0; i_SSD < FLOAT_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
		printf("%f\n", fArray_SSD[i_SSD]);
	printf("\n\n");

	printf("Displaying character array : \n\n");
	for (i_SSD = 0; i_SSD < CHAR_ARRAY_NUM_ELEMENTS_SSD; i_SSD++)
		printf("%c\n", cArray_SSD[i_SSD]);
	return(0);
}