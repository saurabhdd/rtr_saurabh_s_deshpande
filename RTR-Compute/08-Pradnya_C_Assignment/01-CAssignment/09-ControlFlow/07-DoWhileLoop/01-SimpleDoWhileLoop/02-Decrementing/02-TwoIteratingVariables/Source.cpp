#include <stdio.h>
int main(void)
{
	int i_SSD, j_SSD;

	printf("\n\n");
	printf("Printing digits from 1 to 100 ----\n");
	i_SSD = 10;
	j_SSD = 100;
	do
	{
		printf("\t %d \t %d\n", i_SSD, j_SSD);

		i_SSD--;
		j_SSD = j_SSD - 10;
	} while (i_SSD >= 1, j_SSD >= 10);

	printf("\n\n");
	return(0);

}

