#include <stdio.h>
int main(void)
{
	int i_SSD;

	printf("\n\n");
	printf("Printing digits from 10 to 1 : \n");
	i_SSD = 10;
	do
	{
		printf("\t%d\n", i_SSD);
		i_SSD--;
	} while (i_SSD >= 1);
	printf("\n\n");
	return(0);
}