#include <stdio.h>
int main(void)
{

int i_SSD, j_SSD;

printf("One Loop Prints Odd Numbers Between 1 and 10. \n");
printf("Inner Loop Prints Even Numbers Between 1 and 10.\n");

for (i_SSD = 1; i_SSD <= 10; i_SSD++)
{
if (i_SSD % 2 != 0) 
{
printf("i = %d\n", i_SSD);

for (j_SSD = 1; j_SSD <= 10; j_SSD++)
{
if (j_SSD % 2 == 0) 
{
printf("\tj = %d\n",j_SSD);
}
else 
{
continue;
}
}

}
else 
{
continue;
}
}

return(0);
}