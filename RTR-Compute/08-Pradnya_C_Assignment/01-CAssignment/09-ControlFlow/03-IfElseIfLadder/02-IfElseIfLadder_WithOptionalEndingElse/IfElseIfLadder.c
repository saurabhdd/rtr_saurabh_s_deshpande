#include <stdio.h>
int main(void)
{

   int num_SSD;
   
   printf("Enter Value For 'num' : ");
   scanf("%d", &num_SSD);

   if (num_SSD < 0)
   printf("Num = %d Is Less Than 0 \n", num_SSD); 

   else if ((num_SSD > 0) && (num_SSD <= 100))
   printf("Num = %d Is Between 0 And 100 \n", num_SSD);

   else if ((num_SSD > 100) && (num_SSD <= 200))
   printf("Num = %d Is Between 100 And 200 \n", num_SSD);

   else if ((num_SSD > 200) && (num_SSD <= 300))
   printf("Num = %d Is Between 200 And 300 \n", num_SSD);

   else if ((num_SSD > 300) && (num_SSD <= 400))
   printf("Num = %d Is Between 300 And 400 \n", num_SSD);

   else if ((num_SSD > 400) && (num_SSD <= 500))
   printf("Num = %d Is Between 400 And 500 \n", num_SSD);

   else if (num_SSD > 500)
   printf("Num = %d Is Greater Than 500 \n", num_SSD);
   
   printf("Invalid Value Entered \n");

return(0);

}